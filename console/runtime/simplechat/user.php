<?php

return [
    'user0' => [
        'email' => 'maymie.keeling@hotmail.com',
        'created_at' => '2019-09-25 12:11:43',
    ],
    'user1' => [
        'email' => 'rath.shaniya@gmail.com',
        'created_at' => '2019-09-25 12:11:43',
    ],
    'user2' => [
        'email' => 'qeichmann@friesen.biz',
        'created_at' => '2019-09-25 12:11:43',
    ],
    'user3' => [
        'email' => 'nrussel@gerlach.biz',
        'created_at' => '2019-09-25 12:11:43',
    ],
    'user4' => [
        'email' => 'ochamplin@gmail.com',
        'created_at' => '2019-09-25 12:11:43',
    ],
    'user5' => [
        'email' => 'yoshiko32@romaguera.com',
        'created_at' => '2019-09-25 12:11:43',
    ],
    'user6' => [
        'email' => 'ronaldo09@hessel.biz',
        'created_at' => '2019-09-25 12:11:43',
    ],
    'user7' => [
        'email' => 'caesar.zboncak@yahoo.com',
        'created_at' => '2019-09-25 12:11:43',
    ],
    'user8' => [
        'email' => 'amelia78@yahoo.com',
        'created_at' => '2019-09-25 12:11:43',
    ],
    'user9' => [
        'email' => 'ursula32@hotmail.com',
        'created_at' => '2019-09-25 12:11:43',
    ],
    'user10' => [
        'email' => 'kristin99@lowe.com',
        'created_at' => '2019-09-25 12:11:43',
    ],
    'user11' => [
        'email' => 'kilback.jess@mueller.com',
        'created_at' => '2019-09-25 12:11:43',
    ],
    'user12' => [
        'email' => 'rosalee95@durgan.biz',
        'created_at' => '2019-09-25 12:11:43',
    ],
    'user13' => [
        'email' => 'zgorczany@olson.com',
        'created_at' => '2019-09-25 12:11:43',
    ],
    'user14' => [
        'email' => 'stoltenberg.wendy@gmail.com',
        'created_at' => '2019-09-25 12:11:43',
    ],
    'user15' => [
        'email' => 'andre.franecki@hotmail.com',
        'created_at' => '2019-09-25 12:11:43',
    ],
    'user16' => [
        'email' => 'jacklyn46@satterfield.com',
        'created_at' => '2019-09-25 12:11:43',
    ],
    'user17' => [
        'email' => 'delmer.bergnaum@gutmann.com',
        'created_at' => '2019-09-25 12:11:43',
    ],
    'user18' => [
        'email' => 'carolanne.schneider@hotmail.com',
        'created_at' => '2019-09-25 12:11:43',
    ],
    'user19' => [
        'email' => 'xavier69@yahoo.com',
        'created_at' => '2019-09-25 12:11:43',
    ],
];

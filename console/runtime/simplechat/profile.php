<?php

return [
    'profile0' => [
        'first_name' => 'Mike',
        'last_name' => 'Stehr',
        'avatar' => 'F05.png',
    ],
    'profile1' => [
        'first_name' => 'Shayne',
        'last_name' => 'Konopelski',
        'avatar' => 'G05.png',
    ],
    'profile2' => [
        'first_name' => 'Deshawn',
        'last_name' => 'Howe',
        'avatar' => 'L01.png',
    ],
    'profile3' => [
        'first_name' => 'Daron',
        'last_name' => 'Schaefer',
        'avatar' => 'H02.png',
    ],
    'profile4' => [
        'first_name' => 'Clemens',
        'last_name' => 'Boehm',
        'avatar' => 'O04.png',
    ],
    'profile5' => [
        'first_name' => 'Collin',
        'last_name' => 'Doyle',
        'avatar' => 'J01.png',
    ],
    'profile6' => [
        'first_name' => 'Javonte',
        'last_name' => 'Stracke',
        'avatar' => 'B04.png',
    ],
    'profile7' => [
        'first_name' => 'Dawson',
        'last_name' => 'Smith',
        'avatar' => 'C04.png',
    ],
    'profile8' => [
        'first_name' => 'Casimir',
        'last_name' => 'Walsh',
        'avatar' => 'I05.png',
    ],
    'profile9' => [
        'first_name' => 'Gayle',
        'last_name' => 'Ullrich',
        'avatar' => 'G02.png',
    ],
    'profile10' => [
        'first_name' => 'Willy',
        'last_name' => 'Botsford',
        'avatar' => 'K02.png',
    ],
    'profile11' => [
        'first_name' => 'Jay',
        'last_name' => 'Orn',
        'avatar' => 'K01.png',
    ],
    'profile12' => [
        'first_name' => 'Ernesto',
        'last_name' => 'Bergnaum',
        'avatar' => 'L01.png',
    ],
    'profile13' => [
        'first_name' => 'Pierce',
        'last_name' => 'Greenholt',
        'avatar' => 'H01.png',
    ],
    'profile14' => [
        'first_name' => 'Niko',
        'last_name' => 'Brown',
        'avatar' => 'O01.png',
    ],
    'profile15' => [
        'first_name' => 'Arne',
        'last_name' => 'Schinner',
        'avatar' => 'C01.png',
    ],
    'profile16' => [
        'first_name' => 'Orrin',
        'last_name' => 'Metz',
        'avatar' => 'K03.png',
    ],
    'profile17' => [
        'first_name' => 'Ryley',
        'last_name' => 'Smitham',
        'avatar' => 'C02.png',
    ],
    'profile18' => [
        'first_name' => 'Rocky',
        'last_name' => 'Turcotte',
        'avatar' => 'D02.png',
    ],
    'profile19' => [
        'first_name' => 'Merl',
        'last_name' => 'Schneider',
        'avatar' => 'K03.png',
    ],
];

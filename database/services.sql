-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 10, 2022 at 09:22 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `services`
--
CREATE DATABASE IF NOT EXISTS `services` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `services`;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `confirmation_token` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `superadmin` smallint(6) DEFAULT 0,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `registration_ip` varchar(15) DEFAULT NULL,
  `bind_to_ip` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `email_confirmed` smallint(1) NOT NULL DEFAULT 0,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `address` varchar(1024) DEFAULT NULL,
  `country` varchar(512) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `rate_count` int(11) DEFAULT 0,
  `photo` varchar(255) DEFAULT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `last_seen` timestamp NULL DEFAULT NULL,
  `last_seen_ts` double DEFAULT NULL,
  `fb_id` varchar(100) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `mobile_confirmed` tinyint(4) NOT NULL DEFAULT 0,
  `symbol` varchar(15) DEFAULT NULL,
  `city` varchar(512) DEFAULT NULL,
  `state` varchar(512) DEFAULT NULL,
  `zip` varchar(512) DEFAULT NULL,
  `tax_id` varchar(512) DEFAULT NULL,
  `address_2` text DEFAULT NULL,
  `auth_expiry` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `auth_key`, `password_hash`, `confirmation_token`, `status`, `superadmin`, `created_at`, `updated_at`, `registration_ip`, `bind_to_ip`, `email`, `email_confirmed`, `lat`, `lng`, `address`, `country`, `rate`, `rate_count`, `photo`, `fname`, `lname`, `last_seen`, `last_seen_ts`, `fb_id`, `phone`, `mobile_confirmed`, `symbol`, `city`, `state`, `zip`, `tax_id`, `address_2`, `auth_expiry`) VALUES
(15, 'admin', '', '$2y$13$PgGR2mz5B3MM6bEqL8hUT.wMtCDnRAjGDSrguDX9hBjQJf3QCWioW', NULL, 1, 1, 151515, 151515, NULL, NULL, 'admin@services.com', 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_notification`
--

CREATE TABLE `admin_notification` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `content` varchar(2048) NOT NULL,
  `date` datetime NOT NULL,
  `icon` varchar(2048) DEFAULT NULL,
  `image` varchar(2048) DEFAULT NULL,
  `url` varchar(2048) NOT NULL,
  `read` tinyint(4) NOT NULL DEFAULT 0,
  `publish` tinyint(4) NOT NULL DEFAULT 0,
  `symbol` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `symbol` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) CHARACTER SET latin1 NOT NULL,
  `type` int(11) NOT NULL,
  `description` text CHARACTER SET latin1 DEFAULT NULL,
  `rule_name` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `data` text CHARACTER SET latin1 DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `group_code` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `symbol` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`, `group_code`, `symbol`, `order`) VALUES
('accounting_accounting-password', 2, 'Accounting Accounting-password', NULL, NULL, 1590564117, 1590564117, 'accounting', NULL, NULL),
('accounting_add-to-book-work', 2, 'Accounting Add-to-book-work', NULL, NULL, 1586854454, 1586854454, 'accounting', NULL, NULL),
('accounting_another-bill', 2, 'Accounting Another-bill', NULL, NULL, 1600086237, 1600086237, 'accounting', NULL, NULL),
('accounting_bills', 2, 'Accounting Bills', NULL, NULL, 1584222036, 1584222036, 'accounting', NULL, NULL),
('accounting_bills-book-work', 2, 'Accounting Bills-book-work', NULL, NULL, 1586854457, 1586854457, 'accounting', NULL, NULL),
('accounting_change-memo', 2, 'Accounting Change-memo', NULL, NULL, 1590569500, 1590569500, 'accounting', NULL, NULL),
('accounting_close-book-work', 2, 'Accounting Close-book-work', NULL, NULL, 1600101132, 1600101132, 'accounting', NULL, NULL),
('accounting_close-check', 2, 'Accounting Close-check', NULL, NULL, 1586066504, 1586066504, 'accounting', NULL, NULL),
('accounting_closed', 2, 'Accounting Closed', NULL, NULL, 1584310910, 1584310910, 'accounting', NULL, NULL),
('accounting_create', 2, 'Accounting Create', NULL, NULL, 1590563772, 1590563772, 'accounting', NULL, NULL),
('accounting_create-default-bills-for-con', 2, 'Accounting Create-default-bills-for-con', NULL, NULL, 1586856141, 1586856141, 'accounting', NULL, NULL),
('accounting_create-for-con', 2, 'Accounting Create-for-con', NULL, NULL, 1600086091, 1600086091, 'accounting', NULL, NULL),
('accounting_create-multi', 2, 'Accounting Create-multi', NULL, NULL, 1584374680, 1584374680, 'accounting', NULL, NULL),
('accounting_direct-print-check', 2, 'Accounting Direct-print-check', NULL, NULL, 1586556007, 1586556007, 'accounting', NULL, NULL),
('accounting_excel-checks', 2, 'Accounting Excel-checks', NULL, NULL, 1584376297, 1584376297, 'accounting', NULL, NULL),
('accounting_excel-un-printed', 2, 'Accounting Excel-un-printed', NULL, NULL, 1590568888, 1590568888, 'accounting', NULL, NULL),
('accounting_index', 2, 'Accounting Index', NULL, NULL, 1584222033, 1584222033, 'accounting', NULL, NULL),
('accounting_modal-print-check', 2, 'Accounting Modal-print-check', NULL, NULL, 1586556035, 1586556035, 'accounting', NULL, NULL),
('accounting_more-con-bills', 2, 'Accounting More-con-bills', NULL, NULL, 1586857671, 1586857671, 'accounting', NULL, NULL),
('accounting_pending', 2, 'Accounting Pending', NULL, NULL, 1584310897, 1584310897, 'accounting', NULL, NULL),
('accounting_print-book-work', 2, 'Accounting Print-book-work', NULL, NULL, 1601751365, 1601751365, 'accounting', NULL, NULL),
('accounting_print-multi-checks', 2, 'Accounting Print-multi-checks', NULL, NULL, 1586556039, 1586556039, 'accounting', NULL, NULL),
('accounting_print-multi-checks-preview', 2, 'Accounting Print-multi-checks-preview', NULL, NULL, 1586854509, 1586854509, 'accounting', NULL, NULL),
('accounting_remove-to-book-work', 2, 'Accounting Remove-to-book-work', NULL, NULL, 1590569262, 1590569262, 'accounting', NULL, NULL),
('accounting_un-print-check', 2, 'Accounting Un-print-check', NULL, NULL, 1590570147, 1590570147, 'accounting', NULL, NULL),
('accounting_update', 2, 'Accounting Update', NULL, NULL, 1592458836, 1592458836, 'accounting', NULL, NULL),
('accounting_view', 2, 'Accounting View', NULL, NULL, 1590563931, 1590563931, 'accounting', NULL, NULL),
('admin', 1, 'Admin', NULL, NULL, 1584020721, 1584020721, NULL, NULL, NULL),
('admin-permission_set', 2, 'Admin-permission Set', NULL, NULL, 1586557538, 1586557538, 'admin-permission', NULL, NULL),
('admin_index', 2, 'Admin Index', NULL, NULL, 1586557528, 1586557528, 'admin', NULL, NULL),
('aes_create', 2, 'Aes Create', NULL, NULL, 1598355183, 1598355183, 'aes', NULL, NULL),
('auth_login', 2, 'Auth Login', NULL, NULL, 1584221195, 1584221195, 'auth', NULL, NULL),
('auth_logout', 2, 'Auth Logout', NULL, NULL, 1586556421, 1586556421, 'auth', NULL, NULL),
('balance_bank-name', 2, 'Balance Bank-name', NULL, NULL, 1590570023, 1590570023, 'balance', NULL, NULL),
('balance_create', 2, 'Balance Create', NULL, NULL, 1590570084, 1590570084, 'balance', NULL, NULL),
('balance_index', 2, 'Balance Index', NULL, NULL, 1586561656, 1586561656, 'balance', NULL, NULL),
('balance_transfer', 2, 'Balance Transfer', NULL, NULL, 1590570133, 1590570133, 'balance', NULL, NULL),
('balance_view', 2, 'Balance View', NULL, NULL, 1590570120, 1590570120, 'balance', NULL, NULL),
('booking-provider_create', 2, 'Booking-provider Create', NULL, NULL, 1598290782, 1598290782, 'booking-provider', NULL, NULL),
('booking-provider_index', 2, 'Booking-provider Index', NULL, NULL, 1598290734, 1598290734, 'booking-provider', NULL, NULL),
('booking_add-booking-num', 2, 'Booking Add-booking-num', NULL, NULL, 1584302153, 1584302153, 'booking', NULL, NULL),
('booking_create', 2, 'Booking Create', NULL, NULL, 1584302087, 1584302087, 'booking', NULL, NULL),
('booking_download-file', 2, 'Booking Download-file', NULL, NULL, 1586840543, 1586840543, 'booking', NULL, NULL),
('booking_index', 2, 'Booking Index', NULL, NULL, 1584301760, 1584301760, 'booking', NULL, NULL),
('booking_role-booking', 2, 'Booking Role-booking', NULL, NULL, 1584306706, 1584306706, 'booking', NULL, NULL),
('booking_update', 2, 'Booking Update', NULL, NULL, 1584302155, 1584302155, 'booking', NULL, NULL),
('booking_view', 2, 'Booking View', NULL, NULL, 1584302230, 1584302230, 'booking', NULL, NULL),
('category_change-status', 2, 'Category Change-status', NULL, NULL, 1620395877, 1620395877, 'category', NULL, NULL),
('category_create', 2, 'Category Create', NULL, NULL, 1620382064, 1620382064, 'category', NULL, NULL),
('category_delete', 2, 'Category Delete', NULL, NULL, 1620396819, 1620396819, 'category', NULL, NULL),
('category_index', 2, 'Category Index', NULL, NULL, 1620381927, 1620381927, 'category', NULL, NULL),
('category_update', 2, 'Category Update', NULL, NULL, 1620395198, 1620395198, 'category', NULL, NULL),
('category_view', 2, 'Category View', NULL, NULL, 1620383840, 1620383840, 'category', NULL, NULL),
('colors_index', 2, 'Colors Index', NULL, NULL, 1586561100, 1586561100, 'colors', NULL, NULL),
('color_create', 2, 'Color Create', NULL, NULL, 1594561428, 1594561428, 'color', NULL, NULL),
('color_get-colors', 2, 'Color Get-colors', NULL, NULL, 1594563061, 1594563061, 'color', NULL, NULL),
('color_view', 2, 'Color View', NULL, NULL, 1594561800, 1594561800, 'color', NULL, NULL),
('consignee_create', 2, 'Consignee Create', NULL, NULL, 1594410738, 1594410738, 'consignee', NULL, NULL),
('consignee_delete', 2, 'Consignee Delete', NULL, NULL, 1620139248, 1620139248, 'consignee', NULL, NULL),
('consignee_get-consignee', 2, 'Consignee Get-consignee', NULL, NULL, 1619533903, 1619533903, 'consignee', NULL, NULL),
('container-inventory_another-update-loading-plan', 2, 'Container-inventory Another-update-loading-plan', NULL, NULL, 1586558332, 1586558332, 'container-inventory', NULL, NULL),
('container-inventory_booking-num-select', 2, 'Container-inventory Booking-num-select', NULL, NULL, 1584302736, 1584302736, 'container-inventory', NULL, NULL),
('container-inventory_con-check', 2, 'Container-inventory Con-check', NULL, NULL, 1586558383, 1586558383, 'container-inventory', NULL, NULL),
('container-inventory_confirm-admin', 2, 'Container-inventory Confirm-admin', NULL, NULL, 1586557938, 1586557938, 'container-inventory', NULL, NULL),
('container-inventory_confirm-change-status', 2, 'Container-inventory Confirm-change-status', NULL, NULL, 1586069768, 1586069768, 'container-inventory', NULL, NULL),
('container-inventory_delete-loading-plan', 2, 'Container-inventory Delete-loading-plan', NULL, NULL, 1594411506, 1594411506, 'container-inventory', NULL, NULL),
('container-inventory_get-booking-value', 2, 'Container-inventory Get-booking-value', NULL, NULL, 1586558138, 1586558138, 'container-inventory', NULL, NULL),
('container-inventory_get-consignee-data', 2, 'Container-inventory Get-consignee-data', NULL, NULL, 1586557639, 1586557639, 'container-inventory', NULL, NULL),
('container-inventory_get-inventory', 2, 'Container-inventory Get-inventory', NULL, NULL, 1586557639, 1586557639, 'container-inventory', NULL, NULL),
('container-inventory_get-port-of-loading', 2, 'Container-inventory Get-port-of-loading', NULL, NULL, 1584302649, 1584302649, 'container-inventory', NULL, NULL),
('container-inventory_get-shipper-consignees', 2, 'Container-inventory Get-shipper-consignees', NULL, NULL, 1595140609, 1595140609, 'container-inventory', NULL, NULL),
('container-inventory_inv-create-loading-plan', 2, 'Container-inventory Inv-create-loading-plan', NULL, NULL, 1586557624, 1586557624, 'container-inventory', NULL, NULL),
('container-inventory_prepare-create-loading-plan', 2, 'Container-inventory Prepare-create-loading-plan', NULL, NULL, 1584302257, 1584302257, 'container-inventory', NULL, NULL),
('container-inventory_prepare-loading-plan', 2, 'Container-inventory Prepare-loading-plan', NULL, NULL, 1586557625, 1586557625, 'container-inventory', NULL, NULL),
('container-inventory_shipping-con', 2, 'Container-inventory Shipping-con', NULL, NULL, 1586558485, 1586558485, 'container-inventory', NULL, NULL),
('container-inventory_update-prepare-loading-plan', 2, 'Container-inventory Update-prepare-loading-plan', NULL, NULL, 1594411380, 1594411380, 'container-inventory', NULL, NULL),
('container_ajax-prepare-loading-plan', 2, 'Container Ajax-prepare-loading-plan', NULL, NULL, 1595913882, 1595913882, 'container', NULL, NULL),
('container_bill-of-lading', 2, 'Container Bill-of-lading', NULL, NULL, 1598364423, 1598364423, 'container', NULL, NULL),
('container_bill-of-lading-pdf', 2, 'Container Bill-of-lading-pdf', NULL, NULL, 1598366076, 1598366076, 'container', NULL, NULL),
('container_booking-dispatched', 2, 'Container Booking-dispatched', NULL, NULL, 1598346503, 1598346503, 'container', NULL, NULL),
('container_booking-num-select', 2, 'Container Booking-num-select', NULL, NULL, 1598292878, 1598292878, 'container', NULL, NULL),
('container_cover-sheet', 2, 'Container Cover-sheet', NULL, NULL, 1598374916, 1598374916, 'container', NULL, NULL),
('container_cover-sheet-pdf', 2, 'Container Cover-sheet-pdf', NULL, NULL, 1598376419, 1598376419, 'container', NULL, NULL),
('container_create-loading-plan', 2, 'Container Create-loading-plan', NULL, NULL, 1595844900, 1595844900, 'container', NULL, NULL),
('container_delete-loading-plan', 2, 'Container Delete-loading-plan', NULL, NULL, 1595857325, 1595857325, 'container', NULL, NULL),
('container_delivered', 2, 'Container Delivered', NULL, NULL, 1606155188, 1606155188, 'container', NULL, NULL),
('container_draft', 2, 'Container Draft', NULL, NULL, 1598362941, 1598362941, 'container', NULL, NULL),
('container_edit-bill-of-lading', 2, 'Container Edit-bill-of-lading', NULL, NULL, 1598366733, 1598366733, 'container', NULL, NULL),
('container_edit-cover-sheet', 2, 'Container Edit-cover-sheet', NULL, NULL, 1598375501, 1598375501, 'container', NULL, NULL),
('container_edit-loading-plan', 2, 'Container Edit-loading-plan', NULL, NULL, 1598355565, 1598355565, 'container', NULL, NULL),
('container_get-customer-port-of-loading-inventories', 2, 'Container Get-customer-port-of-loading-inventories', NULL, NULL, 1596046175, 1596046175, 'container', NULL, NULL),
('container_get-inventory', 2, 'Container Get-inventory', NULL, NULL, 1595854646, 1595854646, 'container', NULL, NULL),
('container_get-port-of-loading-customers', 2, 'Container Get-port-of-loading-customers', NULL, NULL, 1596045887, 1596045887, 'container', NULL, NULL),
('container_get-port-of-loading-inventories', 2, 'Container Get-port-of-loading-inventories', NULL, NULL, 1595853935, 1595853935, 'container', NULL, NULL),
('container_index', 2, 'Container Index', NULL, NULL, 1617373199, 1617373199, 'container', NULL, NULL),
('container_loading-plan', 2, 'Container Loading-plan', NULL, NULL, 1598354752, 1598354752, 'container', NULL, NULL),
('container_loading-plan-pdf', 2, 'Container Loading-plan-pdf', NULL, NULL, 1605551809, 1605551809, 'container', NULL, NULL),
('container_pending-loading', 2, 'Container Pending-loading', NULL, NULL, 1598361755, 1598361755, 'container', NULL, NULL),
('container_prepare-create-loading-plan', 2, 'Container Prepare-create-loading-plan', NULL, NULL, 1598291234, 1598291234, 'container', NULL, NULL),
('container_prepare-loading-plan', 2, 'Container Prepare-loading-plan', NULL, NULL, 1595851011, 1595851011, 'container', NULL, NULL),
('container_prepare-loading-plans-list', 2, 'Container Prepare-loading-plans-list', NULL, NULL, 1595844420, 1595844420, 'container', NULL, NULL),
('container_sell-rate-select', 2, 'Container Sell-rate-select', NULL, NULL, 1617976411, 1617976411, 'container', NULL, NULL),
('container_send-tc-email', 2, 'Container Send-tc-email', NULL, NULL, 1613156395, 1613156395, 'container', NULL, NULL),
('container_shipped', 2, 'Container Shipped', NULL, NULL, 1600175287, 1600175287, 'container', NULL, NULL),
('container_status-back', 2, 'Container Status-back', NULL, NULL, 1598351653, 1598351653, 'container', NULL, NULL),
('container_status-forward', 2, 'Container Status-forward', NULL, NULL, 1598361349, 1598361349, 'container', NULL, NULL),
('container_update-prepare-loading-plan', 2, 'Container Update-prepare-loading-plan', NULL, NULL, 1595857886, 1595857886, 'container', NULL, NULL),
('default-bills_create', 2, 'Default-bills Create', NULL, NULL, 1600096040, 1600096040, 'default-bills', NULL, NULL),
('default-bills_create-default-bills-for-con', 2, 'Default-bills Create-default-bills-for-con', NULL, NULL, 1600084204, 1600084204, 'default-bills', NULL, NULL),
('default-bills_create-for-con', 2, 'Default-bills Create-for-con', NULL, NULL, 1600086144, 1600086144, 'default-bills', NULL, NULL),
('default-bills_index', 2, 'Default-bills Index', NULL, NULL, 1584374848, 1584374848, 'default-bills', NULL, NULL),
('default-bills_more-con-bills', 2, 'Default-bills More-con-bills', NULL, NULL, 1600085675, 1600085675, 'default-bills', NULL, NULL),
('default-bills_update', 2, 'Default-bills Update', NULL, NULL, 1600109992, 1600109992, 'default-bills', NULL, NULL),
('default-bills_update-modal', 2, 'Default-bills Update-modal', NULL, NULL, 1586856162, 1586856162, 'default-bills', NULL, NULL),
('default-bills_view', 2, 'Default-bills View', NULL, NULL, 1600098254, 1600098254, 'default-bills', NULL, NULL),
('email-record_index', 2, 'Email-record Index', NULL, NULL, 1586555955, 1586555955, 'email-record', NULL, NULL),
('email-record_view', 2, 'Email-record View', NULL, NULL, 1586555960, 1586555960, 'email-record', NULL, NULL),
('inland-fees_create-multi', 2, 'Inland-fees Create-multi', NULL, NULL, 1600096344, 1600096344, 'inland-fees', NULL, NULL),
('inland-fees_index', 2, 'Inland-fees Index', NULL, NULL, 1584374834, 1584374834, 'inland-fees', NULL, NULL),
('inland-fees_update', 2, 'Inland-fees Update', NULL, NULL, 1600098557, 1600098557, 'inland-fees', NULL, NULL),
('inland-fees_view', 2, 'Inland-fees View', NULL, NULL, 1600098553, 1600098553, 'inland-fees', NULL, NULL),
('inventory_add-check', 2, 'Inventory Add-check', NULL, NULL, 1586555671, 1586555671, 'inventory', NULL, NULL),
('inventory_add-check-dispatch', 2, 'Inventory Add-check-dispatch', NULL, NULL, 1593451077, 1593451077, 'inventory', NULL, NULL),
('inventory_add-container-image', 2, 'Inventory Add-container-image', NULL, NULL, 1586558931, 1586558931, 'inventory', NULL, NULL),
('inventory_add-form-details', 2, 'Inventory Add-form-details', NULL, NULL, 1588969066, 1588969066, 'inventory', NULL, NULL),
('inventory_add-image', 2, 'Inventory Add-image', NULL, NULL, 1586556134, 1586556134, 'inventory', NULL, NULL),
('inventory_auto-inspection-receipt-form', 2, 'Inventory Auto-inspection-receipt-form', NULL, NULL, 1586556093, 1586556093, 'inventory', NULL, NULL),
('inventory_bill-of-loading', 2, 'Inventory Bill-of-loading', NULL, NULL, 1586558547, 1586558547, 'inventory', NULL, NULL),
('inventory_booking-dispatched', 2, 'Inventory Booking-dispatched', NULL, NULL, 1584306699, 1584306699, 'inventory', NULL, NULL),
('inventory_by-container', 2, 'Inventory By-container', NULL, NULL, 1584314205, 1584314205, 'inventory', NULL, NULL),
('inventory_change-pending-status', 2, 'Inventory Change-pending-status', NULL, NULL, 1612790215, 1612790215, 'inventory', NULL, NULL),
('inventory_change-status', 2, 'Inventory Change-status', NULL, NULL, 1592461329, 1592461329, 'inventory', NULL, NULL),
('inventory_change-status-submit', 2, 'Inventory Change-status-submit', NULL, NULL, 1592462865, 1592462865, 'inventory', NULL, NULL),
('inventory_check-vin', 2, 'Inventory Check-vin', NULL, NULL, 1586070910, 1586070910, 'inventory', NULL, NULL),
('inventory_confirm', 2, 'Inventory Confirm', NULL, NULL, 1589051285, 1589051285, 'inventory', NULL, NULL),
('inventory_container-images', 2, 'Inventory Container-images', NULL, NULL, 1586558918, 1586558918, 'inventory', NULL, NULL),
('inventory_cover-sheet', 2, 'Inventory Cover-sheet', NULL, NULL, 1586558521, 1586558521, 'inventory', NULL, NULL),
('inventory_create-multi', 2, 'Inventory Create-multi', NULL, NULL, 1584301927, 1584301927, 'inventory', NULL, NULL),
('inventory_delete', 2, 'Inventory Delete', NULL, NULL, 1589051087, 1589051087, 'inventory', NULL, NULL),
('inventory_download-file', 2, 'Inventory Download-file', NULL, NULL, 1589048775, 1589048775, 'inventory', NULL, NULL),
('inventory_download-image', 2, 'Inventory Download-image', NULL, NULL, 1589049195, 1589049195, 'inventory', NULL, NULL),
('inventory_download-title', 2, 'Inventory Download-title', NULL, NULL, 1589048426, 1589048426, 'inventory', NULL, NULL),
('inventory_edit-pass', 2, 'Inventory Edit-pass', NULL, NULL, 1586556184, 1586556184, 'inventory', NULL, NULL),
('inventory_email-loading-plan', 2, 'Inventory Email-loading-plan', NULL, NULL, 1584304288, 1584304288, 'inventory', NULL, NULL),
('inventory_find-cargo', 2, 'Inventory Find-cargo', NULL, NULL, 1584299742, 1584299742, 'inventory', NULL, NULL),
('inventory_form-bill-of-loading', 2, 'Inventory Form-bill-of-loading', NULL, NULL, 1586558566, 1586558566, 'inventory', NULL, NULL),
('inventory_form-cover-sheet', 2, 'Inventory Form-cover-sheet', NULL, NULL, 1586558538, 1586558538, 'inventory', NULL, NULL),
('inventory_get-images-pdf', 2, 'Inventory Get-images-pdf', NULL, NULL, 1587988820, 1587988820, 'inventory', NULL, NULL),
('inventory_get-inventory-barcode', 2, 'Inventory Get-inventory-barcode', NULL, NULL, 1604941717, 1604941717, 'inventory', NULL, NULL),
('inventory_get-inventory-log', 2, 'Inventory Get-inventory-log', NULL, NULL, 1604941837, 1604941837, 'inventory', NULL, NULL),
('inventory_get-make-models', 2, 'Inventory Get-make-models', NULL, NULL, 1594552511, 1594552511, 'inventory', NULL, NULL),
('inventory_get-other-options', 2, 'Inventory Get-other-options', NULL, NULL, 1600967417, 1600967417, 'inventory', NULL, NULL),
('inventory_get-title-details', 2, 'Inventory Get-title-details', NULL, NULL, 1590560233, 1590560233, 'inventory', NULL, NULL),
('inventory_image-slider-modal', 2, 'Inventory Image-slider-modal', NULL, NULL, 1615246442, 1615246442, 'inventory', NULL, NULL),
('inventory_index', 2, 'Inventory Index', NULL, NULL, 1584020707, 1584020707, 'inventory', NULL, NULL),
('inventory_loading-plans', 2, 'Inventory Loading-plans', NULL, NULL, 1584304278, 1584304278, 'inventory', NULL, NULL),
('inventory_pending', 2, 'Inventory Pending', NULL, NULL, 1612789415, 1612789415, 'inventory', NULL, NULL),
('inventory_prepare-loading-plans-list', 2, 'Inventory Prepare-loading-plans-list', NULL, NULL, 1584302255, 1584302255, 'inventory', NULL, NULL),
('inventory_print', 2, 'Inventory Print', NULL, NULL, 1589047071, 1589047071, 'inventory', NULL, NULL),
('inventory_remove-images', 2, 'Inventory Remove-images', NULL, NULL, 1586556151, 1586556151, 'inventory', NULL, NULL),
('inventory_send-email', 2, 'Inventory Send-email', NULL, NULL, 1586556388, 1586556388, 'inventory', NULL, NULL),
('inventory_send-title-email', 2, 'Inventory Send-title-email', NULL, NULL, 1613152611, 1613152611, 'inventory', NULL, NULL),
('inventory_update', 2, 'Inventory Update', NULL, NULL, 1586556189, 1586556189, 'inventory', NULL, NULL),
('inventory_update-another-style', 2, 'Inventory Update-another-style', NULL, NULL, 1586556207, 1586556207, 'inventory', NULL, NULL),
('inventory_view', 2, 'Inventory View', NULL, NULL, 1584374618, 1584374618, 'inventory', NULL, NULL),
('inventory_view-container', 2, 'Inventory View-container', NULL, NULL, 1584304284, 1584304284, 'inventory', NULL, NULL),
('inventory_wa-form', 2, 'Inventory Wa-form', NULL, NULL, 1586449296, 1586449296, 'inventory', NULL, NULL),
('inventory_wa-form-edit', 2, 'Inventory Wa-form-edit', NULL, NULL, 1586556076, 1586556076, 'inventory', NULL, NULL),
('invoice_closed', 2, 'Invoice Closed', NULL, NULL, 1601553647, 1601553647, 'invoice', NULL, NULL),
('invoice_create', 2, 'Invoice Create', NULL, NULL, 1586558610, 1586558610, 'invoice', NULL, NULL),
('invoice_create-direct', 2, 'Invoice Create-direct', NULL, NULL, 1586560047, 1586560047, 'invoice', NULL, NULL),
('invoice_create-for-inv-release', 2, 'Invoice Create-for-inv-release', NULL, NULL, 1586557462, 1586557462, 'invoice', NULL, NULL),
('invoice_delete', 2, 'Invoice Delete', NULL, NULL, 1601554850, 1601554850, 'invoice', NULL, NULL),
('invoice_edit', 2, 'Invoice Edit', NULL, NULL, 1601486217, 1601486217, 'invoice', NULL, NULL),
('invoice_get-bill-to-from-user', 2, 'Invoice Get-bill-to-from-user', NULL, NULL, 1586560060, 1586560060, 'invoice', NULL, NULL),
('invoice_get-invoice-payments-fields', 2, 'Invoice Get-invoice-payments-fields', NULL, NULL, 1601534424, 1601534424, 'invoice', NULL, NULL),
('invoice_get-invoice-pdf', 2, 'Invoice Get-invoice-pdf', NULL, NULL, 1600093723, 1600093723, 'invoice', NULL, NULL),
('invoice_index', 2, 'Invoice Index', NULL, NULL, 1584310549, 1584310549, 'invoice', NULL, NULL),
('invoice_pending', 2, 'Invoice Pending', NULL, NULL, 1600088202, 1600088202, 'invoice', NULL, NULL),
('invoice_pre-view', 2, 'Invoice Pre-view', NULL, NULL, 1586558799, 1586558799, 'invoice', NULL, NULL),
('invoice_send-invoice-email', 2, 'Invoice Send-invoice-email', NULL, NULL, 1600094946, 1600094946, 'invoice', NULL, NULL),
('invoice_user-balance', 2, 'Invoice User-balance', NULL, NULL, 1586561198, 1586561198, 'invoice', NULL, NULL),
('invoice_view', 2, 'Invoice View', NULL, NULL, 1586559008, 1586559008, 'invoice', NULL, NULL),
('loading-fees_create-modal', 2, 'Loading-fees Create-modal', NULL, NULL, 1586558731, 1586558731, 'loading-fees', NULL, NULL),
('loading-fees_create-multi', 2, 'Loading-fees Create-multi', NULL, NULL, 1600096506, 1600096506, 'loading-fees', NULL, NULL),
('loading-fees_delete', 2, 'Loading-fees Delete', NULL, NULL, 1600100199, 1600100199, 'loading-fees', NULL, NULL),
('loading-fees_index', 2, 'Loading-fees Index', NULL, NULL, 1584374837, 1584374837, 'loading-fees', NULL, NULL),
('loading-fees_view', 2, 'Loading-fees View', NULL, NULL, 1600098650, 1600098650, 'loading-fees', NULL, NULL),
('memo-other_create', 2, 'Memo-other Create', NULL, NULL, 1590569761, 1590569761, 'memo-other', NULL, NULL),
('memo-other_index', 2, 'Memo-other Index', NULL, NULL, 1584374828, 1584374828, 'memo-other', NULL, NULL),
('memo-other_memo-other-closed-details', 2, 'Memo-other Memo-other-closed-details', NULL, NULL, 1584375687, 1584375687, 'memo-other', NULL, NULL),
('memo-other_update', 2, 'Memo-other Update', NULL, NULL, 1590569797, 1590569797, 'memo-other', NULL, NULL),
('memo-other_view', 2, 'Memo-other View', NULL, NULL, 1590569793, 1590569793, 'memo-other', NULL, NULL),
('order_index', 2, 'Order Index', NULL, NULL, 1586560242, 1586560242, 'order', NULL, NULL),
('other-fees_create-multi', 2, 'Other-fees Create-multi', NULL, NULL, 1600097968, 1600097968, 'other-fees', NULL, NULL),
('other-fees_index', 2, 'Other-fees Index', NULL, NULL, 1584374844, 1584374844, 'other-fees', NULL, NULL),
('other-fees_view', 2, 'Other-fees View', NULL, NULL, 1600098095, 1600098095, 'other-fees', NULL, NULL),
('port-of-loading_create', 2, 'Port-of-loading Create', NULL, NULL, 1584303735, 1584303735, 'port-of-loading', NULL, NULL),
('port-of-loading_index', 2, 'Port-of-loading Index', NULL, NULL, 1584303417, 1584303417, 'port-of-loading', NULL, NULL),
('port-of-loading_update', 2, 'Port-of-loading Update', NULL, NULL, 1584303862, 1584303862, 'port-of-loading', NULL, NULL),
('port-of-loading_view', 2, 'Port-of-loading View', NULL, NULL, 1584303421, 1584303421, 'port-of-loading', NULL, NULL),
('receipt_add-credit', 2, 'Receipt Add-credit', NULL, NULL, 1601554484, 1601554484, 'receipt', NULL, NULL),
('receipt_close-receipt', 2, 'Receipt Close-receipt', NULL, NULL, 1586559531, 1586559531, 'receipt', NULL, NULL),
('receipt_create', 2, 'Receipt Create', NULL, NULL, 1584300475, 1584300475, 'receipt', NULL, NULL),
('receipt_get-invoices', 2, 'Receipt Get-invoices', NULL, NULL, 1584300477, 1584300477, 'receipt', NULL, NULL),
('receipt_get-receipt-create-data', 2, 'Receipt Get-receipt-create-data', NULL, NULL, 1601729083, 1601729083, 'receipt', NULL, NULL),
('receipt_get-user-balance', 2, 'Receipt Get-user-balance', NULL, NULL, 1584300479, 1584300479, 'receipt', NULL, NULL),
('receipt_get-user-inventory-summery', 2, 'Receipt Get-user-inventory-summery', NULL, NULL, 1586847306, 1586847306, 'receipt', NULL, NULL),
('receipt_index', 2, 'Receipt Index', NULL, NULL, 1584300473, 1584300473, 'receipt', NULL, NULL),
('receipt_submit', 2, 'Receipt Submit', NULL, NULL, 1601737063, 1601737063, 'receipt', NULL, NULL),
('receipt_view', 2, 'Receipt View', NULL, NULL, 1584300510, 1584300510, 'receipt', NULL, NULL),
('released-inventory_create', 2, 'Released-inventory Create', NULL, NULL, 1586557422, 1586557422, 'released-inventory', NULL, NULL),
('released-inventory_index', 2, 'Released-inventory Index', NULL, NULL, 1584352820, 1584352820, 'released-inventory', NULL, NULL),
('released-inventory_view', 2, 'Released-inventory View', NULL, NULL, 1584374581, 1584374581, 'released-inventory', NULL, NULL),
('report_combos', 2, 'Report Combos', NULL, NULL, 1586065764, 1586065764, 'report', NULL, NULL),
('report_expenses', 2, 'Report Expenses', NULL, NULL, 1584310270, 1584310270, 'report', NULL, NULL),
('report_grid-page-size', 2, 'Report Grid-page-size', NULL, NULL, 1584310706, 1584310706, 'report', NULL, NULL),
('report_index', 2, 'Report Index', NULL, NULL, 1584221225, 1584221225, 'report', NULL, NULL),
('report_inventory-log', 2, 'Report Inventory-log', NULL, NULL, 1584221228, 1584221228, 'report', NULL, NULL),
('report_open-towing-fees', 2, 'Report Open-towing-fees', NULL, NULL, 1586561314, 1586561314, 'report', NULL, NULL),
('role_create', 2, 'Role Create', NULL, NULL, 1584020716, 1584020716, 'role', NULL, NULL),
('role_index', 2, 'Role Index', NULL, NULL, 1584020700, 1584020700, 'role', NULL, NULL),
('role_set-child-permissions', 2, 'Role Set-child-permissions', NULL, NULL, 1584020784, 1584020784, 'role', NULL, NULL),
('role_view', 2, 'Role View', NULL, NULL, 1584020721, 1584020721, 'role', NULL, NULL),
('shipper_assign-consignees', 2, 'Shipper Assign-consignees', NULL, NULL, 1594410919, 1594410919, 'shipper', NULL, NULL),
('shipper_create', 2, 'Shipper Create', NULL, NULL, 1594410694, 1594410694, 'shipper', NULL, NULL),
('shipper_get-shipper', 2, 'Shipper Get-shipper', NULL, NULL, 1619531548, 1619531548, 'shipper', NULL, NULL),
('shipping-fees_create-modal', 2, 'Shipping-fees Create-modal', NULL, NULL, 1586558825, 1586558825, 'shipping-fees', NULL, NULL),
('shipping-fees_create-multi', 2, 'Shipping-fees Create-multi', NULL, NULL, 1600098697, 1600098697, 'shipping-fees', NULL, NULL),
('shipping-fees_index', 2, 'Shipping-fees Index', NULL, NULL, 1584374839, 1584374839, 'shipping-fees', NULL, NULL),
('shipping-fees_view', 2, 'Shipping-fees View', NULL, NULL, 1600098754, 1600098754, 'shipping-fees', NULL, NULL),
('site_change-bank-status', 2, 'Site Change-bank-status', NULL, NULL, 1601485852, 1601485852, 'site', NULL, NULL),
('site_config', 2, 'Site Config', NULL, NULL, 1586561060, 1586561060, 'site', NULL, NULL),
('site_docs', 2, 'Site Docs', NULL, NULL, 1589552076, 1589552076, 'site', NULL, NULL),
('site_edit-aes-config', 2, 'Site Edit-aes-config', NULL, NULL, 1614451265, 1614451265, 'site', NULL, NULL),
('site_error', 2, 'Site Error', NULL, NULL, 1584222045, 1584222045, 'site', NULL, NULL),
('site_get-itn-number', 2, 'Site Get-itn-number', NULL, NULL, 1598360620, 1598360620, 'site', NULL, NULL),
('site_get-user-inventories-containers', 2, 'Site Get-user-inventories-containers', NULL, NULL, 1601548155, 1601548155, 'site', NULL, NULL),
('site_index', 2, 'Site Index', NULL, NULL, 1584221195, 1584221195, 'site', NULL, NULL),
('site_json-schema', 2, 'Site Json-schema', NULL, NULL, 1589552077, 1589552077, 'site', NULL, NULL),
('site_update-bank', 2, 'Site Update-bank', NULL, NULL, 1601485989, 1601485989, 'site', NULL, NULL),
('trucking-company_create', 2, 'Trucking-company Create', NULL, NULL, 1612906169, 1612906169, 'trucking-company', NULL, NULL),
('trucking-company_index', 2, 'Trucking-company Index', NULL, NULL, 1612906151, 1612906151, 'trucking-company', NULL, NULL),
('trucking-company_view', 2, 'Trucking-company View', NULL, NULL, 1612906186, 1612906186, 'trucking-company', NULL, NULL),
('user-balance_index', 2, 'User-balance Index', NULL, NULL, 1601554395, 1601554395, 'user-balance', NULL, NULL),
('user-credit_add-credit', 2, 'User-credit Add-credit', NULL, NULL, 1601555606, 1601555606, 'user-credit', NULL, NULL),
('user-credit_index', 2, 'User-credit Index', NULL, NULL, 1601554608, 1601554608, 'user-credit', NULL, NULL),
('user-credit_view', 2, 'User-credit View', NULL, NULL, 1601555673, 1601555673, 'user-credit', NULL, NULL),
('user_add-attachment', 2, 'User Add-attachment', NULL, NULL, 1586560948, 1586560948, 'user', NULL, NULL),
('user_change-password', 2, 'User Change-password', NULL, NULL, 1588965407, 1588965407, 'user', NULL, NULL),
('user_create', 2, 'User Create', NULL, NULL, 1586560209, 1586560209, 'user', NULL, NULL),
('user_delete', 2, 'User Delete', NULL, NULL, 1620139220, 1620139220, 'user', NULL, NULL),
('user_get-customers', 2, 'User Get-customers', NULL, NULL, 1595832759, 1595832759, 'user', NULL, NULL),
('user_index', 2, 'User Index', NULL, NULL, 1586560201, 1586560201, 'user', NULL, NULL),
('user_update', 2, 'User Update', NULL, NULL, 1588965425, 1588965425, 'user', NULL, NULL),
('user_view', 2, 'User View', NULL, NULL, 1586560897, 1586560897, 'user', NULL, NULL),
('vendor_create', 2, 'Vendor Create', NULL, NULL, 1590562547, 1590562547, 'vendor', NULL, NULL),
('vendor_index', 2, 'Vendor Index', NULL, NULL, 1590562406, 1590562406, 'vendor', NULL, NULL),
('vendor_submit-ten-nine-nine', 2, 'Vendor Submit-ten-nine-nine', NULL, NULL, 1584310274, 1584310274, 'vendor', NULL, NULL),
('vendor_view', 2, 'Vendor View', NULL, NULL, 1590562764, 1590562764, 'vendor', NULL, NULL),
('vessel-voyage_index', 2, 'Vessel-voyage Index', NULL, NULL, 1586558014, 1586558014, 'vessel-voyage', NULL, NULL),
('voyage_create', 2, 'Voyage Create', NULL, NULL, 1598290032, 1598290032, 'voyage', NULL, NULL),
('voyage_index', 2, 'Voyage Index', NULL, NULL, 1598289915, 1598289915, 'voyage', NULL, NULL),
('voyage_update', 2, 'Voyage Update', NULL, NULL, 1598293165, 1598293165, 'voyage', NULL, NULL),
('voyage_view', 2, 'Voyage View', NULL, NULL, 1598290150, 1598290150, 'voyage', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  `symbol` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`, `symbol`) VALUES
('admin', 'inventory_index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_group`
--

CREATE TABLE `auth_item_group` (
  `code` varchar(64) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `symbol` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_item_group`
--

INSERT INTO `auth_item_group` (`code`, `name`, `created_at`, `updated_at`, `symbol`) VALUES
('accounting', 'Accounting', 1584222033, 1584222033, NULL),
('admin', 'Admin', 1586557527, 1586557527, NULL),
('admin-permission', 'Admin-permission', 1586557538, 1586557538, NULL),
('aes', 'Aes', 1598355183, 1598355183, NULL),
('auth', 'Auth', 1584221195, 1584221195, NULL),
('balance', 'Balance', 1586561656, 1586561656, NULL),
('booking', 'Booking', 1584301760, 1584301760, NULL),
('booking-provider', 'Booking-provider', 1598290734, 1598290734, NULL),
('category', 'Category', 1620381927, 1620381927, NULL),
('color', 'Color', 1594561428, 1594561428, NULL),
('colors', 'Colors', 1586561100, 1586561100, NULL),
('consignee', 'Consignee', 1594410738, 1594410738, NULL),
('container', 'Container', 1595844420, 1595844420, NULL),
('container-inventory', 'Container-inventory', 1584302257, 1584302257, NULL),
('default-bills', 'Default-bills', 1584374848, 1584374848, NULL),
('email-record', 'Email-record', 1586555955, 1586555955, NULL),
('inland-fees', 'Inland-fees', 1584374834, 1584374834, NULL),
('inventory', 'Inventory', 1584020707, 1584020707, NULL),
('invoice', 'Invoice', 1584310549, 1584310549, NULL),
('loading-fees', 'Loading-fees', 1584374836, 1584374836, NULL),
('memo-other', 'Memo-other', 1584374828, 1584374828, NULL),
('order', 'Order', 1586560242, 1586560242, NULL),
('other-fees', 'Other-fees', 1584374844, 1584374844, NULL),
('port-of-loading', 'Port-of-loading', 1584303417, 1584303417, NULL),
('receipt', 'Receipt', 1584300473, 1584300473, NULL),
('released-inventory', 'Released-inventory', 1584352820, 1584352820, NULL),
('report', 'Report', 1584221225, 1584221225, NULL),
('role', 'Role', 1584020700, 1584020700, NULL),
('shipper', 'Shipper', 1594410694, 1594410694, NULL),
('shipping-fees', 'Shipping-fees', 1584374839, 1584374839, NULL),
('site', 'Site', 1584221195, 1584221195, NULL),
('trucking-company', 'Trucking-company', 1612906150, 1612906150, NULL),
('user', 'User', 1586560201, 1586560201, NULL),
('user-balance', 'User-balance', 1601554395, 1601554395, NULL),
('user-credit', 'User-credit', 1601554608, 1601554608, NULL),
('vendor', 'Vendor', 1584310274, 1584310274, NULL),
('vessel-voyage', 'Vessel-voyage', 1586558013, 1586558013, NULL),
('voyage', 'Voyage', 1598289915, 1598289915, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) NOT NULL,
  `data` text DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `symbol` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name_en` varchar(20) NOT NULL,
  `name_ge` varchar(20) NOT NULL,
  `description_en` text DEFAULT NULL,
  `description_ge` text DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `status` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name_en`, `name_ge`, `description_en`, `description_ge`, `image`, `image_name`, `created_at`, `status`) VALUES
(2, 'shoes', 'Schuhe', 'lovely felxible shoes', 'schöne flexible Schuhe', '[ Twitter Bootstrap 3 In Arabic ] #31 - Section Ultimate Footer Part 1.mp4_snapshot_13.27.660_609559d3a261c.jpg', NULL, '2021-05-07 07:18:17', 1),
(4, 'pijamas', 'akjfhefhuief', '', '', '60993720d250c.jpg', '[ Twitter Bootstrap 3 In Arabic ] #29 - Section Contact Us.mp4_snapshot_11.20.437.jpg', '2021-05-10 06:37:36', 1);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1453226110),
('m130524_201442_init', 1453226111),
('m181123_182700_add_column_symbol_to_all_tables', 1549135760),
('m181123_184143_set_symbol_to_all_tables', 1549136984),
('m181123_190937_create_table_queues', 1549136984),
('m181123_192249_test_migrate', 1549136984),
('m181126_212521_create_settings_table', 1549137228),
('m181126_213142_seeds_add_row_confing', 1549137246);

-- --------------------------------------------------------

--
-- Table structure for table `service`
--

CREATE TABLE `service` (
  `id` int(11) NOT NULL,
  `name_en` varchar(20) NOT NULL,
  `name_ge` varchar(20) NOT NULL,
  `sub_cat_id` int(11) NOT NULL,
  `description_en` text DEFAULT NULL,
  `description_ge` text DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `status` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service`
--

INSERT INTO `service` (`id`, `name_en`, `name_ge`, `sub_cat_id`, `description_en`, `description_ge`, `image`, `image_name`, `created_at`, `status`) VALUES
(14, 'canvas', 'grghrhr', 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '60a56c6f8872c.jpg', '67.jpg', '2021-05-19 12:52:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `service_details`
--

CREATE TABLE `service_details` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `title_en` varchar(20) NOT NULL,
  `title_ge` varchar(20) NOT NULL,
  `description_en` text DEFAULT NULL,
  `description_ge` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_details`
--

INSERT INTO `service_details` (`id`, `service_id`, `title_en`, `title_ge`, `description_en`, `description_ge`) VALUES
(7, 14, 'title1', 'JKSGJGER', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'),
(8, 14, 'TITLE 2 ', 'GJEJROIGH', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.');

-- --------------------------------------------------------

--
-- Table structure for table `service_images`
--

CREATE TABLE `service_images` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `image` varchar(20) NOT NULL,
  `unique_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_images`
--

INSERT INTO `service_images` (`id`, `service_id`, `name`, `image`, `unique_id`) VALUES
(18, 14, '866294-babylon-fanta', '60a56c6faaa0c.jpg', NULL),
(19, 14, 'Ahriman_Artwork.jpg', '60a56c6fb6d5c.jpg', NULL),
(20, 14, 'ancient_wisdom.jpg', '60a56c6fb946c.jpg', NULL),
(21, 14, 'blacksmith_anvil_art', '60a56c6fc7ecc.jpg', NULL),
(22, 14, 'creation-012.jpg', '60a56c6fcccec.jpg', NULL),
(23, 14, 'garden_passage_by_de', '60a56c6fcf3fc.jpg', NULL),
(24, 14, 'maxresdefault.jpg', '60a56c6fd692c.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `name_en` varchar(20) NOT NULL,
  `name_ge` varchar(20) NOT NULL,
  `description_en` text DEFAULT NULL,
  `description_ge` text DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `status` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `cat_id`, `name_en`, `name_ge`, `description_en`, `description_ge`, `image`, `image_name`, `created_at`, `status`) VALUES
(1, 2, 'snickers', 'kichert ', 'comfortable while snickers', 'bequeme weiße Turnschuhe ', '[ Twitter Bootstrap 3 In Arabic ] #39 - Create The Loading Screen.mp4_snapshot_18.37.380_609551ca4fa80.jpg', NULL, '2021-05-07 07:42:18', 1),
(2, 4, 'summer', 'agrgr', '', '', NULL, NULL, '2021-05-19 12:33:08', 1),
(3, 4, 'winter', 'gegegeg', '', '', NULL, NULL, '2021-05-19 12:33:19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `confirmation_token` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `superadmin` smallint(6) DEFAULT 0,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `registration_ip` varchar(15) DEFAULT NULL,
  `bind_to_ip` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `second_email` varchar(512) DEFAULT NULL,
  `third_email` varchar(512) DEFAULT NULL,
  `fourth_email` varchar(512) DEFAULT NULL,
  `email_confirmed` smallint(1) NOT NULL DEFAULT 0,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `address` varchar(1024) DEFAULT NULL,
  `city` varchar(512) DEFAULT NULL,
  `state` varchar(512) DEFAULT NULL,
  `zip_postal_code` varchar(512) DEFAULT NULL,
  `fax_num` varchar(512) DEFAULT NULL,
  `country` varchar(512) DEFAULT NULL,
  `rate` float DEFAULT NULL,
  `rate_count` int(11) NOT NULL DEFAULT 0,
  `photo` varchar(255) DEFAULT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `last_seen` timestamp NULL DEFAULT NULL,
  `last_seen_ts` double DEFAULT NULL,
  `fb_id` varchar(100) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `mobile_confirmed` tinyint(4) NOT NULL DEFAULT 0,
  `attachment` varchar(512) DEFAULT NULL,
  `symbol` varchar(15) DEFAULT NULL,
  `due_date_date_type` varchar(512) DEFAULT NULL,
  `due_date_before_or_after` int(11) DEFAULT NULL,
  `due_date_days` tinyint(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `confirmation_token`, `status`, `superadmin`, `created_at`, `updated_at`, `registration_ip`, `bind_to_ip`, `email`, `second_email`, `third_email`, `fourth_email`, `email_confirmed`, `lat`, `lng`, `address`, `city`, `state`, `zip_postal_code`, `fax_num`, `country`, `rate`, `rate_count`, `photo`, `fname`, `lname`, `last_seen`, `last_seen_ts`, `fb_id`, `phone`, `mobile_confirmed`, `attachment`, `symbol`, `due_date_date_type`, `due_date_before_or_after`, `due_date_days`) VALUES
(1, 'test', 'XupiKoug-_SROqOkhBAwTUNSq4zpBN9g', '$2y$13$6Jl3fit3LH313e9tsYW9j.BQ392lpX0k74vQbzYNu69SgILPoDLLW', NULL, 1, 0, 1588967051, 1600023397, '::1', '', 'yusef.shahoud@gmail.com', '', '', '', 1, NULL, NULL, '', '', '', '', '', NULL, NULL, 0, NULL, 'test', 'test', NULL, NULL, NULL, '123', 0, NULL, NULL, 'eta', 0, 1),
(2, 'user2', 'ZyTwQXr8Wttlktfq3f3jiZFpR0x1JV3P', '$2y$13$AqwTvWEiF4SRsySR3hJx9Ofg7SFIs9mmXVCb7paA6YSmN3PxjL5Ge', NULL, 1, 0, 1595140196, 1616337585, '::1', '', 'user2@hotmail.com', 'User2@gmail.com', 'user2@yahoo.com', 'user2@facebook.com', 0, NULL, NULL, '', '', '', '', '', NULL, NULL, 0, NULL, 'user2', 'user2', NULL, NULL, NULL, '1234', 0, NULL, NULL, 'eta', 0, NULL),
(3, 'yyyy', 'kwAp-3G4qC15j2ohUaTri9FBowAlwBr3', '$2y$13$muKsQD13m4vZnggPohfw1uXXin5uWZ25V8t0CtOnxtvUFnDxovASG', NULL, 1, 0, 1595832778, 1595832778, '::1', '', 'y@g.c', '', '', '', 0, NULL, NULL, '', '', '', '', '', NULL, NULL, 0, NULL, 'sadas', '', NULL, NULL, NULL, '', 0, NULL, NULL, NULL, NULL, NULL),
(4, 'rrr', 'P22pKRRG4t7tUc0qB10mT0NoAIFJSQ1b', '$2y$13$wszwHRrbDSc/C7kNfVgZIu8N6Z/y27HcDE9kUYLIf.9y02xEn8bW2', NULL, 1, 0, 1595832807, 1595832807, '::1', '', 'tt@xx.s', '', '', '', 0, NULL, NULL, '', '', '', '', '', NULL, NULL, 0, NULL, '', '', NULL, NULL, NULL, '', 0, NULL, NULL, NULL, NULL, NULL),
(5, 'xxxx', 'MWDvzD7dox3y4Qul8UpKchsC0-w1EZqa', '$2y$13$xylGYy/xbrcCNrEjyqRb6.RwoSJ/cf/i/oa6yzlb2gLkSYaVbFzuS', NULL, 1, 0, 1595833630, 1600022926, '::1', '', 'x@x.x', '', '', '', 0, NULL, NULL, '', '', '', '', '', NULL, NULL, 0, NULL, 'asd', '', NULL, NULL, NULL, '', 0, NULL, NULL, 'eta', 0, 1),
(6, 'qwe', 'mOwoSDN6XBYF-Uja-ymB-OscD4r57zyW', '$2y$13$UpYyBlayEfR75sCZ7I6WvuPvYxxG5lfYr49OKEviaAqlWUq2L.vIO', NULL, 1, 0, 1600880886, 1600880886, '::1', '', 'test@hotmxxail.com', '', '', '', 1, NULL, NULL, '', '', '', '', '', NULL, NULL, 0, NULL, '', '', NULL, NULL, NULL, '', 0, NULL, NULL, NULL, NULL, NULL),
(7, 'tett', 'MWUD-xdD4Zm1B7Dy9gFlGhmOifvwe27R', '$2y$13$PEzethkD02HVoAAtOQaxxONbiJrB.Vv9MzMzcq6L1faf8AlqZXEp6', NULL, 1, 0, 1600881161, 1600881161, '::1', '', 'test@hotmaiaal.com', '', '', '', 1, NULL, NULL, '', '', '', '', '', NULL, NULL, 0, NULL, 'tt', '', NULL, NULL, NULL, '', 0, NULL, NULL, NULL, NULL, NULL),
(8, 'admin@example.com', 'Kfoc1k7q6gmtypK668Fhj0x3Jlij0CQU', '$2y$13$CrJFIBPG5wdYQ4zm6dfmGOHykg1Y9gK9PeXgLvT5qL9j10U458uYu', NULL, 1, 0, 1601744903, 1601744903, '::1', '', 'test@TEST.TEST', '', '', '', 0, NULL, NULL, '', '', '', '', '', NULL, NULL, 0, NULL, '', '', NULL, NULL, NULL, '', 0, NULL, NULL, NULL, NULL, NULL),
(9, 'shighaf', '1_H_fhQD0fLgQoCT-U1zmrlN-wlQlRIX', '$2y$13$4nAHSEuDpxf3heCfD8UDAO2oIqXL.Pj2fQUET0I1aFd7CEcLp96Mi', NULL, 1, 0, 1615844259, 1618927010, '::1', '', 'SHIGHAF.abdallah@gmail.com', '', '', '', 1, NULL, NULL, '', '', '', '', '', NULL, NULL, 0, NULL, 'shighafoo', '', NULL, NULL, NULL, '', 0, NULL, NULL, NULL, NULL, NULL),
(10, 'hypta', '17CBjYMClY2tHXUodpeZqf7Sr8MioXHs', '$2y$13$iPl01fED9dBtYLT6oP5bBeGp9HDljvQZHvnFp/amwy1BejczSGbWy', NULL, 1, 0, 1619388758, 1619388758, '::1', '', 'hypatia@kgjehgeug.com', '', '', '', 1, NULL, NULL, '', '', '', '', '', NULL, NULL, 0, NULL, '', '', NULL, NULL, NULL, '', 0, NULL, NULL, NULL, NULL, NULL),
(11, 'abc', 'zZ11Df3ehyIt8L2Ypd92QE8g77gM3Jhl', '$2y$13$6hTgGGLeipDqncektk2j6uEfbk9TVjE.QPhZKIwXiGehwM5QV89D6', NULL, 1, 0, 1620139298, 1620139298, '::1', '', 'a@b.c', '', '', '', 1, NULL, NULL, '', '', '', '', '', NULL, NULL, 0, NULL, '', '', NULL, NULL, NULL, '', 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_mobile_email`
--

CREATE TABLE `user_mobile_email` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `confirm_code` varchar(50) NOT NULL,
  `is_confirmed` tinyint(4) NOT NULL DEFAULT 0,
  `is_primary` tinyint(4) NOT NULL DEFAULT 0,
  `symbol` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='user set of his emails and mobile numbers that can confirm';

-- --------------------------------------------------------

--
-- Table structure for table `user_notification`
--

CREATE TABLE `user_notification` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content` varchar(2048) NOT NULL,
  `date` datetime NOT NULL,
  `icon` varchar(2048) DEFAULT NULL,
  `image` varchar(2048) DEFAULT NULL,
  `url` varchar(2048) NOT NULL,
  `read` tinyint(4) NOT NULL,
  `publish` tinyint(4) NOT NULL,
  `symbol` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_visit_log`
--

CREATE TABLE `user_visit_log` (
  `id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `language` char(2) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `visit_time` int(11) NOT NULL,
  `browser` varchar(30) DEFAULT NULL,
  `os` varchar(20) DEFAULT NULL,
  `symbol` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phone` (`phone`),
  ADD KEY `mobile_confirmed` (`mobile_confirmed`);

--
-- Indexes for table `admin_notification`
--
ALTER TABLE `admin_notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_id` (`admin_id`);

--
-- Indexes for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Indexes for table `auth_item_group`
--
ALTER TABLE `auth_item_group`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cat_id` (`sub_cat_id`);

--
-- Indexes for table `service_details`
--
ALTER TABLE `service_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `service_images`
--
ALTER TABLE `service_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_id` (`service_id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cat_id` (`cat_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phone` (`phone`),
  ADD KEY `mobile_confirmed` (`mobile_confirmed`);

--
-- Indexes for table `user_mobile_email`
--
ALTER TABLE `user_mobile_email`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_mobile_email_key` (`user_id`,`mobile`,`email`);

--
-- Indexes for table `user_notification`
--
ALTER TABLE `user_notification`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_visit_log`
--
ALTER TABLE `user_visit_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `admin_notification`
--
ALTER TABLE `admin_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `service`
--
ALTER TABLE `service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `service_details`
--
ALTER TABLE `service_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `service_images`
--
ALTER TABLE `service_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `user_mobile_email`
--
ALTER TABLE `user_mobile_email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_notification`
--
ALTER TABLE `user_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_visit_log`
--
ALTER TABLE `user_visit_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_notification`
--
ALTER TABLE `admin_notification`
  ADD CONSTRAINT `admin_notification_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service`
--
ALTER TABLE `service`
  ADD CONSTRAINT `service_ibfk_1` FOREIGN KEY (`sub_cat_id`) REFERENCES `sub_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service_details`
--
ALTER TABLE `service_details`
  ADD CONSTRAINT `service_details_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service_images`
--
ALTER TABLE `service_images`
  ADD CONSTRAINT `service_images_ibfk_1` FOREIGN KEY (`service_id`) REFERENCES `service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD CONSTRAINT `sub_category_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

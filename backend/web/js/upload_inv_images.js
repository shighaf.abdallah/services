window.i = 0;
window.uploadPhotos = function (url) {
    $('.loader').removeClass('hidden');
    $('#images_div').empty();
    // Read in file
    $.each(event.target.files, function (key, value) {


        var file = value;

        // Ensure it's an image
        if (file.type.match(/image.*/)) {
            console.log('An image has been loaded');

            // Load the image
            var reader = new FileReader();
            reader.onload = function (readerEvent) {
                var image = new Image();
                image.onload = function (imageEvent) {

                    // Resize the image
                    var canvas = document.createElement('canvas'),
                            max_size = 544, // TODO : pull max size from a site config
                            width = image.width,
                            height = image.height;
                    if (width > height) {
                        if (width > max_size) {
                            height *= max_size / width;
                            width = max_size;
                        }
                    } else {
                        if (height > max_size) {
                            width *= max_size / height;
                            height = max_size;
                        }
                    }


//        if (width > height) {
//            ratio = width/height;
//            height= height/(ratio*2);
//         width= width/(ratio*2);
//        } else {
//        ratio = height/width;
//            height= height/(ratio*2);
//         width= width/(ratio*2);
//        }
                    canvas.width = width;
                    canvas.height = height;
                    canvas.getContext('2d').drawImage(image, 0, 0, width, height);
                    var dataUrl = canvas.toDataURL('image/jpeg');
                    var resizedImage = dataURLToBlob(dataUrl);
                    $.event.trigger({
                        type: "imageResized",
                        blob: resizedImage,
                        url: dataUrl
                    });
                }
                image.src = readerEvent.target.result;
            }
            reader.readAsDataURL(file);
        }
    });
    $('.loader').addClass('hidden');
};

/* Utility function to convert a canvas to a BLOB */
var dataURLToBlob = function (dataURL) {
    var BASE64_MARKER = ';base64,';
    if (dataURL.indexOf(BASE64_MARKER) == -1) {
        var parts = dataURL.split(',');
        var contentType = parts[0].split(':')[1];
        var raw = parts[1];

        return new Blob([raw], {type: contentType});
    }

    var parts = dataURL.split(BASE64_MARKER);
    var contentType = parts[0].split(':')[1];
    var raw = window.atob(parts[1]);
    var rawLength = raw.length;

    var uInt8Array = new Uint8Array(rawLength);

    for (var i = 0; i < rawLength; ++i) {
        uInt8Array[i] = raw.charCodeAt(i);
    }

    return new Blob([uInt8Array], {type: contentType});
}
/* End Utility function to convert a canvas to a BLOB      */

/* Handle image resized events */
$(document).on("imageResized", function (event) {
    if (event.blob && event.url) {
        $('#images_div').prepend('<img style="height: 125px;margin: 5px;" src="' + event.url + '" />');
        $('#images_div').prepend('<input name="Inventory[imagefile][' + window.i + ']" style="margin: 5px; display: none;" value="' + event.url + '" />');
        window.i = window.i + 1;
    }

});


window.compress = function (url) {

    if ($('#image_compress').is(':checked')) {
//            $('#takePictureField').removeClass('hidden');
//            $('#image_uncompressed').addClass('hidden');
        $('#image_uncompressed').remove();
        $('#image_uncompressed_label').remove();
        $('#compressed_div').prepend('<label id="takePictureField_label" style="margin: 0; margin-top: 1%;">Compressed Images</label><input style="margin-top: 10px" name="imagefile[]" multiple type="file" id="takePictureField" accept="image/*" onchange="uploadPhotos()" />');
    } else {
//            $('#takePictureField').addClass('hidden');
//            $('#image_uncompressed').removeClass('hidden');
        $('#takePictureField').remove();
        $('#takePictureField_label').remove();
        $('#images_div').empty();
        $('#input_image_modal').prepend('<label id="image_uncompressed_label" style="margin: 0; margin-top: 1%;">Uncompressed Images</label><input type="file" id="image_uncompressed" name="InventoryImage[imagesfiles][]" multiple="" accept="image/*">');
    }
}
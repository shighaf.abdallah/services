$(document).on('change', '.category', function(e) {
    var field_class = 'subCategory';
    $('.' + field_class).val(null).trigger('change');
    $('.' + field_class).empty().trigger("change");
    var selected_filters = [];
    selected_filters.push($(this).val());
    $.get(window.url + '/en/sub-category/get-cats?cat_id=' + selected_filters.toString())
        .done(function(data) {
            var data = $.parseJSON(data);
            var lastItem = 1;
            data.forEach(function(item) {
                lastItem = item.id;
                var newOption = new Option(item.text, item.id, false, false);
                $('.' + field_class).append(newOption);
            });
            $('.' + field_class).val(lastItem).trigger('change');
            $('.' + field_class).trigger('change');
        });
});
<?php

namespace backend\controllers;


use common\models\ServiceDetails;
use common\models\ServiceImages;
use webvimark\modules\UserManagement\models\ZAdmin;
use Yii;
use common\models\Service;
use common\models\search\ServiceSearch;
use common\models\search\ServiceDetailsSearch;

use common\enums\ActiveInactiveStatus;
use common\enums\Constents;

use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

use yii\filters\VerbFilter;


/**
 * ServiceController implements the CRUD actions for Service model.
 */
class ServiceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'change-status',
                            'delete','file-delete','description', 'add-detail', 'update-detail','delete-detail'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function() {
                            $permissionName = Yii::$app->controller->id . '_' . Yii::$app->controller->action->id;
                            return ZAdmin::hasPermission($permissionName);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Service models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ServiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Service model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $detailSearchModel = new ServiceDetailsSearch();
        $detailSearchModel->service_id = $id;
        $detailsDataProvider = $detailSearchModel->search(Yii::$app->request->queryParams);

        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
            'detailSearchModel' => $detailSearchModel,
            'detailsDataProvider' => $detailsDataProvider,
            'detailModel' => $model->serviceDetails,
        ]);
    }

    /**
     * Creates a new Service model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Service();
        $imageModel = new ServiceImages();
        $request = Yii::$app->request;

        if ($model->load(Yii::$app->request->post())) {
            $model->created_at = date(Constents::full_date_format);
            $model->status = ActiveInactiveStatus::active;

            $model->imageFile = UploadedFile::getInstance($model,'imageFile');
            if($model->imageFile){
                $model->saveFile();
            }

            if(!$model->save()){
                stopv($model->errors);
            }

            // Save Images
            $imageModel->imageFile =  UploadedFile::getInstances($imageModel,'imageFile');
            if($imageModel->imageFile){
                $model->saveImages($imageModel->imageFile);
            }

            // Save Details
            $model->saveDetails($request->post('details'));

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'imageModel' => $imageModel,
        ]);
    }

    /**
     * Updates an existing Service model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $imageModel = new ServiceImages();
        $request = Yii::$app->request;

        if ($model->load(Yii::$app->request->post())) {
            $model->imageFile = UploadedFile::getInstance($model,'imageFile');
            if($model->imageFile){
                $model->saveFile();
            }

            if(!$model->save()){
                stopv($model->errors);
            }

            // Save Images
            $imageModel->imageFile =  UploadedFile::getInstances($imageModel,'imageFile');
            if($imageModel->imageFile){
                $model->saveImages($imageModel->imageFile);
            }

            // Save Details
            $model->saveDetails($request->post('details'));

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Service model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionChangeStatus($id, $s)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if ($post) {
            $old_status = $model->status;
            $model->status = $post['status'];
            if (!$model->save()) {
                stopv($model->errors);
            }
            Yii::$app->getResponse()->redirect(Yii::$app->request->referrer);
            return;
        } else {
            $title = \Yii::t('all', 'Change status to') . ': "' . ActiveInactiveStatus::LabelOf($s) . '"';
            return $this->renderAjax('/shared/change_status', [
                'title' => $title,
                'model' => $model,
                'newStatus' => $s,
            ]);
        }
    }

    /**
     * Finds the Service model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Service the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Service::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionFileDelete()
    {
        $post = Yii::$app->request->post();
        $id = isset($post['key']) ? $post['key'] : '';
        if($id){
            $img = ServiceImages::findOne($id);
            if($img){
                $img->delete();
            }
        }

        return json_encode(1);
    }



    public function actionDescription($id = 1)
    {
        $detailModel = new ServiceDetails();
        return $this->renderPartial('_details',[
            'id' => $id,
            'model' => $detailModel
        ]);
    }


    /*################# ServiceDetails Functions ###############*/
    public function actionAddDetail($id)
    {
        // $id refer to model id
        $model = $this->findModel($id);
        $detailModel = new ServiceDetails();
        $request = Yii::$app->request;
        $title = "Add Description";

        if($detailModel->load(Yii::$app->request->post()) ) {
            $detailModel->service_id=$id;
            if(!$detailModel->save()) {
                stopv($detailModel->errors);
            }
            Yii::$app->session->setFlash('success',"Saved");
            return $this->redirect(['view','id' => $model->id]);
        }


        return $this->renderAjax('modal_details', [
            'id' => $id,
            'model' => $detailModel,
        ]);
    }

    public function actionUpdateDetail($id)
    {
        $detailModel = ServiceDetails::findOne($id);

        if($detailModel->load(Yii::$app->request->post()) ) {
            if(!$detailModel->save()) {
                stopv($detailModel);
            }
            Yii::$app->session->setFlash('success',"Updates");
            return $this->redirect(['view','id' => $detailModel->service_id]);
        }

        return $this->renderAjax('modal_details', [
            'id' => $id,
            'model' => $detailModel,
        ]);
    }

    public function actionDeleteDetail($id)
    {

        $detailModel = ServiceDetails::findOne($id);

        $title = 'Delete Detail ';

        if (Yii::$app->request->post()) {
            $detailModel->delete();
            return $this->redirect(['view','id' => $detailModel->service_id]);
        }
        // Modal Type
        // primary, info, warning, success, danger

        return $this->renderAjax('/shared/confirm', [
            'title' =>  $title,
            'text' => 'Are You Sure?',
        ]);
    }


}

<?php

namespace backend\controllers;


use Yii;
use common\models\SubCategory;
use common\models\search\SubCategorySearch;
use webvimark\modules\UserManagement\models\ZAdmin;

use common\enums\Constents;
use common\enums\ActiveInactiveStatus;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * SubCategoryController implements the CRUD actions for SubCategory model.
 */
class SubCategoryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'change-status',
                            'delete','get-cats'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function() {
                            $permissionName = Yii::$app->controller->id . '_' . Yii::$app->controller->action->id;
                            return ZAdmin::hasPermission($permissionName);
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SubCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SubCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SubCategory model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SubCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SubCategory();

        if ($model->load(Yii::$app->request->post())){
            $model->created_at = date(Constents::full_date_format);
            $model->status = ActiveInactiveStatus::active;

            $model->imageFile = UploadedFile::getInstance($model,'imageFile');
            if($model->imageFile){
                $model->saveFile();
            }

            if(!$model->save()){
                stopv($model->errors);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing SubCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->imageFile = UploadedFile::getInstance($model,'imageFile');
            if($model->imageFile){
                $model->saveFile();
            }

            if(!$model->save()){
                stopv($model->errors);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing SubCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionChangeStatus($id, $s)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if ($post) {
            $old_status = $model->status;
            $model->status = $post['status'];
            if (!$model->save()) {
                stopv($model->errors);
            }
            Yii::$app->getResponse()->redirect(Yii::$app->request->referrer);
            return;
        } else {
            $title = \Yii::t('all', 'Change status to') . ': "' . ActiveInactiveStatus::LabelOf($s) . '"';
            return $this->renderAjax('/shared/change_status', [
                'title' => $title,
                'model' => $model,
                'newStatus' => $s,
            ]);
        }
    }

    /**
     * Finds the SubCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SubCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SubCategory::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGetCats($cat_id='')
    {
        $data[] = [
            'id' => '',
            'text' => '',
        ];

        $cat_id = explode(',',$cat_id);

        $items = SubCategory::find();
        if($cat_id){
            $items = $items->andWhere(['cat_id' => $cat_id]);
        }

        $items = $items->all();
        foreach ($items as $one) {
            $data[] = [
                'id' => $one->id,
                'text' => $one->name_en,
            ];
        }
        return json_encode($data);

    }
}

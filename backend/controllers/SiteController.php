<?php

namespace backend\controllers;

use common\enums\ActiveInactiveStatus;
use common\enums\AdminType;
use common\enums\ContainerStatus;
use common\enums\InventoryStatus;
use common\libs\LibInventory;
use common\models\Admin;
use common\models\AesFreightForwarder;
use common\models\Area;
use common\models\BankInfo;
use common\models\BillOfLoading;
use common\models\BranchRooms;
use common\models\Category;
use common\models\City;
use common\models\Container;
use common\models\ContainerInventory;
use common\models\CoverSheet;
use common\models\generated\search\BankInfoSearch;
use common\models\generated\search\RegionSearch;
use common\models\generated\search\ZUserMobileEmailSearch;
use common\models\GenericCourse;
use common\models\Inventory;
use common\models\Region;
use common\models\State;
use common\models\SubCategory;
use common\models\User;
use FFMpeg\Coordinate\Dimension;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\Format\Video\WebM;
use FFMpeg\Format\Video\WMV;
use FFMpeg\Format\Video\X264;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use common\models\generated\Contact;
use common\models\ContactForm;
use webvimark\modules\UserManagement\models\ZAdmin;
use webvimark\modules\UserManagement\libs\AdminService;
use common\services\LibFacebookPage;
use FFMpeg\FFMpeg;

//include_once (Yii::getAlias('@backend') . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'phpmailer' . DIRECTORY_SEPARATOR.'phpmailer'.DIRECTORY_SEPARATOR.'class.phpmailer.php');

class SiteController extends \common\utils\Controller {

    public $configFiles = 'config_files';

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'get-user-inventories-containers', 'error', 'get-itn-number', 'itn-number','edit-aes-config'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['config', 'user-welcome-email','setting-cron-sent-email',
                            'create-bank', 'update-bank', 'change-bank-status'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule, $action) {
                            return true;
                        }
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'facebookLogin'],
            ],
        ];
    }

    public function facebookLogin($client) {
        $attributes = $client->getUserAttributes();

        $auth = ZAdmin::find()->where([
                    'facebook_id' => $attributes['id'],
                ])->one();
        $accessToken = $client->getAccessToken()->getToken();

        $accept = FALSE;
        if ($auth) { // login
            $attributes['access_token'] = $accessToken;
            $this->updateUserInfo($auth, $attributes);
            $accept = TRUE;
        } else { // signup
            if ($attributes['email'] !== null && ZAdmin::find()->where(['email' => $attributes['email']])->exists()) {
                stopv('email already used');
            } else {
                $password = Yii::$app->security->generateRandomString(6);
                list($res, $auth, $msg) = AdminService::profileCreate([
                            'username' => $attributes['name'],
                            'email' => isset($attributes['email']) ? $attributes['email'] : $attributes['id'] . '@facebook.com',
                            'facebook_id' => $attributes['id'],
                            'facebook_name' => $attributes['name'],
                            'facebook_email' => $attributes['email'],
                            'facebook_access_token' => $accessToken,
                            'created_at' => time(),
                            'updated_at' => time(),
                            'password' => $password,
                ]);
                if ($res) {
                    $accept = TRUE;
                } else {
                    stopv($msg);
                }
            }
        }

        if ($accept) {
            Yii::$app->user->login($auth);
        }
    }

    private function updateUserInfo($user, $attributes) {
        $user['facebook_name'] = $attributes['name'];
        $user['facebook_access_token'] = $attributes['access_token'];
        $user['facebook_id'] = $attributes['id'];
        if (isset($attributes['email'])) {
            $user['email'] = isset($attributes['email']) ? $attributes['email'] : $attributes['id'] . '@facebook.com';
        }
        if (!$user->save()) {
            stopv($user->errors);
        }
        return TRUE;
    }

    public function actionIndex() {
//        stopv($warehousesCharts);
        return $this->render('index', [
        ]);
    }

    public function actionConfig() {
        $contact = Contact::findOne(1);

        $bankInfoSearch = new BankInfoSearch();
        $bankInfoDataProvider = $bankInfoSearch->search(Yii::$app->request->queryParams);

        $invoiceSentenceFile = $this->getFileData('invoice_sentence.txt');
        $invoiceSentence = json_decode(file_get_contents($invoiceSentenceFile));

        //aes
        $aesModel = AesFreightForwarder::find()->one();
        //

        $post = Yii::$app->request->post();
        if ($contact->load($post)) {
            if (!$contact->save()) {
                stopv($contact->errors);
            }

            $invoiceSentence->invoice_button_sentence = $post['config']['invoice_button_sentence '];
            $invoiceSentenceJson = json_encode($invoiceSentence);
            file_put_contents($invoiceSentenceFile, $invoiceSentenceJson);

            \Yii::$app->getSession()->setFlash('flash_success', "Saved");
            return $this->refresh();
        } else {
            return $this->render('config', [
                'contact' => $contact,
//                'bankInfo' => $bankInfoModel,
                'bankInfoDataProvider' => $bankInfoDataProvider,
                'invoiceSentence' => $invoiceSentence,
                'aesModel' => $aesModel,
            ]);
        }
    }

    public function getFileData($fileName) {
        $path = Yii::getAlias('@backend') . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . $this->configFiles . DIRECTORY_SEPARATOR;
        $BankInfoFile = $path . $fileName;
        if (!file_exists($BankInfoFile)) {
            stopv('File not exist');
        }
        return $BankInfoFile;
    }

    public function actionContact() {
        $model = new ContactForm();
        $email = Contact::findOne(1)->email;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail($email)) {
                Yii::$app->session->setFlash('flash_success', 'Your massage is received and will be answered shortly.');
            } else {
                Yii::$app->session->setFlash('flash_error', 'Error in sending massage, Try again later');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                        'model' => $model,
            ]);
        }
    }

    public function actionLang($l = 'en') {
        Yii::$app->session->set('lang', $l);
        return $this->redirect(['index']);
    }

    public function actionGetStates($id) {
        $data[] = [
            'id' => '',
            'text' => '',
        ];
        $items = State::find()
            ->andWhere(['is_active' => ActiveInactiveStatus::active])
            ->andWhere(['country_id' => $id])->all();
        foreach ($items as $one){
            $data[] = [
                'id' => $one->id,
                'text' => $one->name,
            ];
        }
        return json_encode($data);
    }

    public function actionGetCities($id) {
        $data[] = [
            'id' => '',
            'text' => '',
        ];
        $items = City::find()
            ->andWhere(['is_active' => ActiveInactiveStatus::active])
            ->andWhere(['state_id' => $id])->all();
        foreach ($items as $one){
            $data[] = [
                'id' => $one->id,
                'text' => $one->name,
            ];
        }
        return json_encode($data);
    }

    public function actionGetAreas($id) {
        $data[] = [
            'id' => '',
            'text' => '',
        ];
        $items = Area::find()
            ->andWhere(['is_active' => ActiveInactiveStatus::active])
            ->andWhere(['city_id' => $id])->all();
        foreach ($items as $one){
            $data[] = [
                'id' => $one->id,
                'text' => $one->name,
            ];
        }
        return json_encode($data);
    }

    public function actionGetSubCategories($id) {
        $data = [];
        $items = SubCategory::find()
            ->where(['status' => ActiveInactiveStatus::active])
            ->andWhere(['category_id' => $id])->all();
        foreach ($items as $one){
            $data[] = [
                'id' => $one->id,
                'text' => $one->title,
            ];
        }
        return json_encode($data);
    }

    public function actionTestEmail($r) {
        $send = Yii::$app->mailer->compose()
            ->setFrom('rosit.application@gmail.com')
            ->setTo($r)
            ->setSubject('Test Message')
            ->setTextBody('Plain text content. YII2 Application')
            ->setHtmlBody('<b>HTML content <i>Ram Pukar</i></b>')
            ->send();
        if($send){
            stopv('success');
        } else {
            stopv('error');
        }
    }

    public function actionConfirmationCodes()
    {
        $searchModel = new ZUserMobileEmailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('confrimation_codes', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTestPlayer()
    {
        return $this->render('test_player', [
        ]);
    }

    public function actionProcessVideo()
    {
        $binaries_dir = Yii::getAlias('@backend') . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'ffmpeg' . DIRECTORY_SEPARATOR . 'ffmpeg' . DIRECTORY_SEPARATOR . 'bin' . DIRECTORY_SEPARATOR;
        $ffmpeg = FFMpeg::create(array(
            'ffmpeg.binaries'  => $binaries_dir.'ffmpeg.exe',
            'ffprobe.binaries' => $binaries_dir.'ffprobe.exe',
            //'timeout'          => 3600, // The timeout for the underlying process
            //'ffmpeg.threads'   => 12,   // The number of threads that FFMpeg should use
        ));
        $dir = Yii::getAlias('@backend') . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'test_video' . DIRECTORY_SEPARATOR;
        $video = $ffmpeg->open($dir."1.mp4");
        $video
            ->filters()
            ->resize(new Dimension(320, 240))
            ->synchronize();
        $video
            ->frame(TimeCode::fromSeconds(10))
            ->save($dir.'frame.jpg');
        $video
            ->save(new X264(), $dir.'export-x264.mp4')
            ->save(new WMV(), $dir.'export-wmv.wmv')
            ->save(new WebM(), $dir.'export-webm.webm');

        stopv('done');
    }

    public function actionItnNumber($FID = NULL, $SRN = NULL, $STA = NULL, $ITN = NULL) {
        $ciId = $SRN;
        $containerInventory = ContainerInventory::find()
            ->where(['id' => $ciId])
            ->one();
        $container = $containerInventory->container;

        if ($container && $ITN) {
            $shipperId = $containerInventory->client_shipper_id;
//            stopv($shipperId);
            $coverSheet = CoverSheet::find()
                ->andWhere(['shipper_id' => $shipperId])
                ->andWhere(['container_id' => $container->id])
                ->all();

            $billOfLading = BillOfLoading::find()
//                    ->where(['shipper_id' => $shipperId])
                ->andWhere(['container_id' => $container->id])
                ->all();

            foreach ($coverSheet as $oneItem) {
                if ($oneItem->itn_num) {
                    break;
                }
                $oneItem->itn_num = $ITN;
                if(!$oneItem->save()){
                    var_dump('v');
                    stopv($oneItem->errors);
                }

            }
            foreach ($billOfLading as $oneBillOfLading) {
                $itn_number_value = [];
                if($oneBillOfLading->itn_number){
                    $itn_number_value = (array)json_decode($oneBillOfLading->itn_number);
                }
//                if(!is_array($export_reference_value_tmp) && $oneBillOfLading->export_reference){
//                    $export_reference_value[] = $oneBillOfLading->export_reference;
//                } else {
//                    foreach ($export_reference_value_tmp as $oneKey => $oneV) {
//                        $export_reference_value[$oneKey] = $oneV;
//                    }
//                }
                $itn_number_value[$shipperId] = $ITN;
                $oneBillOfLading->itn_number = json_encode($itn_number_value);



//                if ($oneBillOfLading->export_reference) {
//                    break;
//                }
//                $oneBillOfLading->export_reference = $ITN;
                if(!$oneBillOfLading->save()){
                    var_dump('b');
                    stopv($oneBillOfLading->errors);
                }
            }

            //aes check
            $container->aes_check = 1;
            if(!$container->save()){
                stopv($container->errors);
            }

            foreach ($container->containerInventories as $one){
                if($one->client_shipper_id == $shipperId){
                    //add log
                    $inventory = Inventory::findOne($one->inventory_id);
                    LibInventory::AddLog($inventory, 'Add ITN# to container ('.$ITN.')', $one->container_id, $one->status, null, null, null, null, null);
                    //
                }
            }
            //
        }
        return $this->render('itn_number');
    }

    public function actionGetItnNumber($id) {
        $container = Container::findOne($id);
        $urls = [];
        $ref_num = '';
        foreach ($container->containerInventories as $oneCInv){
            if($ref_num != $oneCInv->id){
                $returnUrl = Yii::$app->urlManager->hostInfo . \yii\helpers\Url::base() . '/en/site/itn-number';
                $itnUrl = 'https://ace.cbp.dhs.gov/aesd/ta/aes-direct/secured/weblinkFilingInquiry?SRN=' . $oneCInv->id . '&FID=814201224&URL=' . $returnUrl;
                $urls[] = $itnUrl;
                $ref_num = $oneCInv->id;
            }
        }
        return json_encode(['urls' => $urls]);
    }

    public function actionChangeBankStatus($id, $s) {
        $model = BankInfo::findOne($id);
        if(!$model){
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $post = Yii::$app->request->post();
        if ($post) {
            $all_banks = BankInfo::find()->all();
            foreach ($all_banks as $one){
                $one->status = ActiveInactiveStatus::inactive;
                if(!$one->save()){
                    stopv($one->errors);
                }
            }
            $old_status = $model->status;
            $model->status = $post['status'];
            if (!$model->save()) {
                stopv($model->errors);
            }
            Yii::$app->getResponse()->redirect(Yii::$app->request->referrer);
            return;
//            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $title = t('Change status to: "') . ActiveInactiveStatus::LabelOf($s).'"';
            return $this->renderAjax('/shared/change_status', [
                'title' => $title,
                'model' => $model,
                'newStatus' => $s,
            ]);
        }
    }

    public function actionUpdateBank($id)
    {
        $model = BankInfo::findOne($id);
        if(!$model){
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        if ($model->load(Yii::$app->request->post())) {
            if(!$model->save()){
                stopv($model->errors);
            }
            return $this->redirect(['config']);
        } else {
            return $this->render('update_bank', [
                'model' => $model,
            ]);
        }
    }

    public function actionGetUserInventoriesContainers($id)
    {
        $model = User::findOne($id);
        $inventories = [];
        $containers = [];
        $bill_to = '';
        if($model){
            $userInvs = Inventory::find()
                ->andWhere(['customer' => $id])
                ->andWhere(['not', ['in', 'vehicle_status', [InventoryStatus::delivered_unpaid, InventoryStatus::delivered]]])
                ->all();
            foreach ($userInvs as $one){
                $inventories[] = [
                    'id' => $one->id,
                    'text' => $one->vin,
                ];
            }

            $containers = ContainerInventory::find()
                ->joinWith('inventory')
                ->joinWith('container')
                ->andWhere(['inventory.customer' => $id])
                ->andWhere(['not', ['in', 'container.status', [ContainerStatus::delivered_paid, ContainerStatus::delivered_unpaid]]])
                ->all();

            foreach ($containers as $one){
                $containers[] = [
                    'id' => $one->container->id,
                    'text' => $one->container->containerNumBookingNum,
                ];
            }

            $bill_to = $model->username
                . "\n" . $model->address
                . "\n" . $model->city . ", " . $model->state . " " . $model->zip_postal_code
                . "\nTEL:" . $model->phone
                . "\nEmail: " . $model->email;
        }
        return json_encode([
            'inventories' => $inventories,
            'containers' => $containers,
            'bill_to' => $bill_to,
        ]);
    }

    public function actionEditAesConfig() {
        $model = AesFreightForwarder::find()->one();
        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            if(!$model->save()){
                stopv($model->errors);
            }
            \Yii::$app->getSession()->setFlash('flash_success', "Saved");
            return $this->redirect(['config']);
        }
        return $this->render('edit_aes_config', [
            'model' => $model,
        ]);
    }

}

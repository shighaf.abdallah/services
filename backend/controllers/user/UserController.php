<?php

namespace backend\controllers\user;

use common\enums\PasswordIds;
use common\libs\LibInventory;
use common\models\search\InlandFeesSearch;
use common\models\search\LoadingFeesSearch;
use common\models\search\OtherFeesSearch;
use common\models\search\ShippingFeesSearch;
use common\models\UserConsignee;
use common\models\UserShippers;
use webvimark\modules\UserManagement\models\ZAdmin;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Accounting;
use common\models\InlandFeesChack;
use common\models\search\InlandFeesChackSearch;
use common\enums\CheckStatus;
use common\models\TotalBalance;
use common\models\Balance;
use common\libs\AccountingLib;
use common\enums\BalanceStatus;
use common\models\CheckMemo;
use yii\helpers\Html;
use common\models\Invoice;
use common\models\search\InvoiceSearch;
use common\models\search\InvoiceCheckSearch;
use common\models\ContainerInventory;
use common\models\BillsWorkBook;
use common\models\DefaultBills;
use common\models\Vendor;
use common\models\Inventory;
use yii\data\ActiveDataProvider;
use NumberFormatter;
use common\models\Password;
use kartik\mpdf\Pdf;
use common\enums\BankNumber;

class UserController extends \webvimark\modules\UserManagement\controllers\UserController {

    public $modelClass = 'common\models\User';

    public function actionView($id, $a = 0) {
        if (!$a) {
            $model = $this->findModel($id);
            $shippers = UserShippers::find()->where(['user_id' => $id]);
            $shippers_DB = new ActiveDataProvider([
                'query' => $shippers,
            ]);

            $consignee = UserConsignee::find()->where(['user_id' => $id]);
            $consignee_DB = new ActiveDataProvider([
                'query' => $consignee,
            ]);

            //fees
            $inlandSM = new InlandFeesSearch();
            $inlandDP = $inlandSM->searchUser(Yii::$app->request->queryParams, $id);

            $loadingSM = new LoadingFeesSearch();
            $loadingDP = $loadingSM->searchUser(Yii::$app->request->queryParams, $id);

            $shippingSM = new ShippingFeesSearch();
            $shippingDP = $shippingSM->searchUser(Yii::$app->request->queryParams, $id);

            $otherSM = new OtherFeesSearch();
            $otherDP = $otherSM->searchUser(Yii::$app->request->queryParams, $id);

            return $this->renderIsAjax('view', [
                'model' => $model,
                'shippers_DB' => $shippers_DB,
                'consignee_DB' => $consignee_DB,
                'inlandDP' => $inlandDP,
                'loadingDP' => $loadingDP,
                'shippingDP' => $shippingDP,
                'otherDP' => $otherDP,
            ]);
        } else {
            return $this->renderIsAjax('view_admin', [
                'model' => $this->findModel($id),
            ]);
        }
    }

}

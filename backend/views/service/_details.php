<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;

/** @var $id common\models\Service */
/* @var $model cmmon\models\ServiceDetails */

?>

<?php
$init_val = $model->id !== null ? $model->id : $id;
$title_en_value = $model->title_en ? $model->title_en : '';
$title_ge_value = $model->title_ge ? $model->title_ge : '';
$description_en_value = $model->description_en ? $model->description_en: '';
$description_ge_value = $model->description_ge ? $model->description_ge: '';

?>
<div class = 'panel panel-default sub-desc' data-id = "<?= $init_val ?>" id="sub-desc-<?= $init_val ?>">
    <div class = 'panel-heading'><h4>Additional Description
            <i class = 'fa fa-minus btn btn-danger btn-sm pull-right desc-minus' data-id = "<?= $init_val ?>"></i>
        </h4>
    </div>
    <div class = 'panel-body sub-desc-body'>

        <div class="row">
            <div class="col-sm-6">
                <label><?= $model->getAttributeLabel('title_en') ?></label>
                <input type="text" class="form-control" name="details[<?= $init_val ?>][title_en]" value="<?= $title_en_value  ?>">
            </div>
            <div class="col-sm-6">
                <label><?= $model->getAttributeLabel('title_ge') ?></label>
                <input type="text" class="form-control" name="details[<?= $init_val ?>][title_ge]" value="<?= $title_ge_value ?>">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-6">
                <label><?= $model->getAttributeLabel('description_en') ?></label>
                 <textarea class="form-control" name="details[<?= $init_val ?>][description_en]"><?= $description_en_value ?></textarea>
            </div>
            <div class="col-sm-6">
                <label><?= $model->getAttributeLabel('description_ge') ?></label>
                 <textarea class="form-control" name="details[<?= $init_val ?>][description_ge]"><?= $description_ge_value ?></textarea>
            </div>
        </div><!-- .row -->


    </div>
</div>

<?php

/*####### Things can be usefule #####*/

$script = <<< JS

JS;
$this->registerJs($script);
?>


<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$this->title = 'Add New Detail';
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span></button>
    <h4 class="modal-title"><?= $this->title ?></h4>
</div>
<?php $form = ActiveForm::begin(); ?>
<div class="modal-body">
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'title_en')->textInput(); ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'title_ge')->textInput(); ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($model, 'description_en')->textarea(); ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($model, 'description_ge')->textarea(); ?>
            </div>
        </div><!-- .row -->
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-warning" data-dismiss="modal"><?= \Yii::t('all', 'Close') ?></button>
    <?= Html::submitButton(\Yii::t('all', 'Save'), [
        'class' => 'btn btn-primary ajax_save_reset_select2',
        //'data-select_reset_url' => Yii::$app->urlManager->createUrl("/dispatch/get-dispatch"),
        //'data-select_reset_field_class' => 'inventory_dispatch',
        'data-dismiss' => 'modal',
    ]) ?>
</div>
<?php ActiveForm::end(); ?>



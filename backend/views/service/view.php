<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\StringHelper;

use common\models\ServiceImages;

use common\enums\ActiveInactiveStatus;
/* @var $this yii\web\View */
/* @var $model common\models\Service */
/* @var $detailsDataProvider yii\data\ActiveDataProvider */

$this->title = $model->name_en;
$this->params['breadcrumbs'][] = ['label' => 'Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
setViewParam('liActive', 'category');
setViewParam('liinActive', 'service_index');
?>
<div class="service-view">
    <p>
        <?php $form = ActiveForm::begin(); ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php ActiveForm::end(); ?>
    </p>

    <div class="box box-success">
        <div class="box-header ui-sortable-handle" style="cursor: move;">
            <i class="fa fa-calculator"></i>
            <h3 class="box-title">Image and Description</h3>
        </div>
        <row class="slimScrollDiv">
            <div class="box-body chat" id="chat-box">
                <div class="col-sm-6">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th colspan="1"><?= $model->getAttributeLabel('cat_id') ?></th>
                                <td colspan="3">
                                    <span><?= $model->subCat->cat->name_en . ' / '.$model->subCat->name_en ?></span>
                                </td>
                            </tr>
                            <tr>
                                <th ><?= $model->getAttributeLabel('name_en') ?></th>
                                <td>
                                    <span><?= $model->name_en?></span>
                                </td>
                                <th><?= $model->getAttributeLabel('name_ge') ?></th>
                                <td>
                                    <span><?= $model->name_ge ?></span>
                                </td>
                            </tr>
                            <tr>
                                <th><?= $model->getAttributeLabel('description_en') ?></th>
                                <td colspan="3">

                                    <span class="less_data" style=""><?= substr($model->description_en,0,100) ?>...</span><span class='more_data'> <?= $model->description_en ?></span>
                                    <a style="color:blue;" class='read_more'>More</a>
                                </td>
                            </tr>
                            <tr>
                                <th><?= $model->getAttributeLabel('description_ge') ?></th>
                                <td colspan="3">
                                    <span class="less_data" style=""><?= substr($model->description_ge,0,100) ?>...</span><span class='more_data'> <?= $model->description_ge ?></span>
                                    <a style="color:blue;" class='read_more'>More</a>
                                </td>
                            </tr>
                            <tr>
                                <th><?= $model->getAttributeLabel('created_at') ?></th>
                                <td>
                                    <span><?= $model->created_at ?></span>
                                </td>
                                <th><?= $model->getAttributeLabel('status') ?></th>
                                <td>
                                  <?php
                                        $btns = '';
                                        if ($model->status == ActiveInactiveStatus::active) {
                                            $btns = Html::button(\Yii::t('all', 'Deactivate'), [
                                                'class' => 'activity-view-link btn btn-sm btn-danger',
                                                'data-toggle' => 'modal',
                                                'data-target' => '#myModal',
                                                'value' => Yii::$app->urlManager->createUrl("/service/change-status?id=$model->id&s=" . ActiveInactiveStatus::inactive)]);
                                        } else {
                                            $btns = Html::button(\Yii::t('all', 'Activate'), [
                                                'class' => 'activity-view-link btn btn-sm btn-success ',
                                                'data-toggle' => 'modal',
                                                'data-target' => '#myModal',
                                                'value' => Yii::$app->urlManager->createUrl("/service/change-status?id=$model->id&s=" . ActiveInactiveStatus::active)]);
                                        }
                                        echo $btns;
                                     ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-sm-6">
                    <img class="img-responsive" src="<?= $model->fileUrl ?>" alt ="<?= $model->image ?>" style="width: 90%; height: 90%">
                </div>
            </div>
        </div>
    </div>


    <!-- ############# Add Detail ############ -->

    <div class="box box-success">
        <div class="box-header ui-sortable-handle" style="cursor: move;">
            <i class="fa fa-address-card-o"></i>
            <h3 class="box-title">More Details</h3>
            <?= Html::button('<i class="fa fa-plus"></i>', [
                'class' => 'pull-right btn btn-sm activity-view-link',
                'style' => '',
                'value' => Yii::$app->urlManager->createUrl("/service/add-detail?id=".$model->id)
            ]) ?>
        </div>
        <row class="slimScrollDiv">
        <div class="box-body chat" id="chat-box">
            <?= GridView::widget([
                'dataProvider' => $detailsDataProvider,
                //'filterModel' => $descriptionSearchModel,
                'tableOptions' => ['class' => 'table tblSec'],
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'title_en',
                        'headerOptions' => ['style' => 'width:15%; color:black'],
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'title_ge',
                        'headerOptions' => ['style' => 'width:15%'],
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'description_en',
                        'headerOptions' => ['style' => 'width:30%'],
                        'value' => function($model){
                            return  '<span class="less_data" style="">'. substr($model->description_en,0,100) .'...</span><span class="more_data">'.$model->description_en .'</span>
                                        <a style="color:blue;" class="read_more">More</a>';
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'description_ge',
                        'headerOptions' => ['style' => 'width:30%'],
                        'value' => function($model){
                            return  '<span class="less_data" style="">'. substr($model->description_ge,0,100) .'...</span><span class="more_data">'.$model->description_ge .'</span>
                                        <a style="color:blue;" class="read_more">More</a>';
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{delete}{update}',
                        'buttons' => [
                            'delete' => function ($url, $model){
                                return  Html::button('<span class="glyphicon glyphicon-trash"></span>', [
                                    'class' => 'activity-view-link',
                                    'style' => '',
                                    'value' => Yii::$app->urlManager->createUrl("/service/delete-detail?id=".$model->id)
                                ]);
                            },
                            'update' => function ($url, $model){
                                return  Html::button('<span class="glyphicon glyphicon-pencil"></span>', [
                                    'class' => 'activity-view-link',
                                    'style' => '',
                                    'value' => Yii::$app->urlManager->createUrl("/service/update-detail?id=".$model->id)
                                ]);
                            }
                        ]
                    ],
                ],
            ]); ?>
        </div>
        </row>
    </div>

    <div class="box box-warning">
        <div class="box-header ui-sortable-handle" style="cursor: move;">
            <i class="fa fa-image"></i>
            <h3 class="box-title">Images</h3>
        </div>
        <div class="slimScrollDiv">
            <div class="box-body chat" id="chat-box">
                <br />
                <form action="<?= Yii::$app->urlManager->createUrl("/inventory/remove-images?id=$model->id") ?>"
                      method="post" enctype="multipart/form-data">
                    <?=yii\helpers\Html::hiddenInput(Yii::$app->request->csrfParam, Yii::$app->request->csrfToken)?>
                    <?= Html::button('Add Images', ['class' => 'btn btn-sm btn-success activity-view-link', 'style' => 'border: none; margin: 2px;', 'value' => Yii::$app->urlManager->createUrl("/inventory/add-image?id=$model->id")]) ?>
                    <?=
                    Html::button('Remove Selected', ['class' => 'btn btn-sm btn-danger form_btn',
                        'style' => 'border: none; margin: 2px;',
                        'value' => \yii\helpers\Url::base().'/en/inventory/remove-images?id=' . $model->id,
                        'data' => [
                            'confirm' => 'Are you sure you want to delete?',
                            'method' => 'post',
                        ],])
                    ?>
                    <br />
                    <div class="gallery">
                        <table class="table table-striped table-bordered detail-view">
                            <tbody>
                            <?php
                            foreach ($model->serviceImages as $key => $oneImage) {
                                $images[] = $oneImage->fileUrl;
                                if ($key % 4 == 0) {
                                    echo '<tr>';
                                }
                                ?>
                                <td style="text-align: center;">
                                    <img src="<?= $oneImage->fileUrl ?>" onclick="lightbox(<?= $key ?>)" style="width: 200px; height:200px"/>
                                    <div style="width: 100%; display: block;">
                                        <input class="checkbox_c" type="checkbox" name="images[]"
                                               value="<?= $oneImage->id ?>">
                                        <a
                                            href="<?= Yii::$app->urlManager->createUrl("/inventory/download-image?id=$oneImage->id") ?>"><i
                                                class="fa fa-download"></i></a>
                                    </div>
                                </td>
                                <?php
                                if ($key % 4 == 3) {
                                    echo '</tr>';
                                }
                                ?>
                            <?php } ?>
                            </tbody>
                        </table>

                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<style>

</style>

<?php
$script = <<< JS
   
JS;
$this->registerJs($script);
?>
<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\file\FileInput;

use common\enums\ActiveInactiveStatus;
use common\models\SubCategory;
use common\models\Category;
use common\models\ServiceImages;

/* @var $this yii\web\View */
/* @var $model common\models\Service */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="service-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <div class="col-sm-6">
        <?= $form->field($model, 'cat_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Category::find()
                ->andWhere(['status'=> ActiveInactiveStatus::active])
                ->all(),'id','name_en'),
            'language' => 'en',
            'options' => [
                    'placeholder' => 'Select Category',
                    'class' => 'category',
                ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($model, 'sub_cat_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(SubCategory::find()
                ->andWhere(['status'=> ActiveInactiveStatus::active])
                ->all(),'id','name_en'),
            'language' => 'en',
            'options' => [
                    'placeholder' => 'Select Category',
                    'class' => 'subCategory',
                ],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>


    <div class="col-sm-6">
        <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($model, 'name_ge')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($model, 'description_en')->textarea(['rows' => 6]) ?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($model, 'description_ge')->textarea(['rows' => 6]) ?>
    </div>

    <div class="col-sm-12">
        <?= $form->field($model, 'imageFile')->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*'],
            'pluginOptions' =>[
                'showUpload' => false,
                'initialPreview' =>!empty($model->image) ? $model->fileUrl : '',
                'initialPreviewAsData'=>true,
                'overwriteInitial'=>true,
            ]
        ]); ?>
    </div>


    <!--############ Secondary Images ############## -->
    <div class="col-sm-12">
        <?php
        $imageModel = new ServiceImages();
        $initialConfig = [];
        if(!empty($model->serviceImages)){
            $imagesModel = $model->serviceImages;
            $initialImages=[];
            foreach ($imagesModel as $img){
                $initialImages[] = $img->fileUrl;
                $initialConfig[] = [
                    'key' => $img->id,
                ];
            }
        }

        echo $form->field($imageModel, 'imageFile[]')->widget(FileInput::classname(), [
            'options' => [
                'accept' => 'image/*',
                'multiple'=>true
            ],
            'pluginOptions' => [
                'showUpload' => false,
                'deleteUrl'=> Url::to('file-delete'),
                'initialPreview' => !empty($model->serviceImages) ? $initialImages : false,
                'initialPreviewConfig' => $initialConfig,
                'initialPreviewAsData'=>true,
                'overwriteInitial'=>false,
                'maxFileCount' => 10,
            ]
        ]);
        ?>
    </div>

    </br>

    <!--########## Description Dynamic Form ########### -->
    <?php
    $detailsModel = $model->serviceDetails
    ?>
    <div class="col-sm-12">
        <div class = "panel panel-default description">
            <div class = "panel-heading">
                <h4> Other Info
                    <i class="fa fa-plus pull-right btn btn-success btn-sm desc-plus"></i>
                </h4>
            </div>
            <div class = "panel-body description-body">
                <?php
                $id = 1;
                if(!is_array($detailsModel))
                {
                    echo $this->render('_details',[
                        'id' => $id,
                        'model' => $detailsModel
                    ]);
                }
                else
                {
                    foreach ($detailsModel as $detail) {
                        echo $this->render('_details',[
                            'id' => $id,
                            'model' => $detail
                        ]);
                        $id = $id +1 ;
                    }
                }

                ?>
            </div>
        </div>
    </div>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php $this->registerJsFile('@web/js/custom/dynamic_form.js', ['depends' => [yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJsFile('@web/js/custom/select2.js', ['depends' => [yii\web\JqueryAsset::className()]]); ?>

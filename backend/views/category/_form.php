<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-sm-6">
        <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($model, 'name_ge')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($model, 'description_en')->textarea(['rows' => 6]) ?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($model, 'description_ge')->textarea(['rows' => 6]) ?>
    </div>

    <div class="col-sm-12">
        <?= $form->field($model, 'imageFile')->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*'],
            'pluginOptions' =>[
                'showUpload' => false,
                'initialPreview' =>!empty($model->image) ? $model->fileUrl : '',
                'initialPreviewAsData'=>true,
                'overwriteInitial'=>true,
            ]
        ]); ?>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

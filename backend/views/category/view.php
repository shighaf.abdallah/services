<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

use common\enums\ActiveInactiveStatus;

/* @var $this yii\web\View */
/* @var $model common\models\Category */

$this->title = $model->name_en;
$this->params['breadcrumbs'][] = ['label' => 'Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
setViewParam('liActive', 'category');
setViewParam('liinActive', 'category_index');
?>
<div class="category-view">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

        <?php $form = ActiveForm::begin(); ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php ActiveForm::end(); ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name_en',
            'name_ge',
            'description_en:ntext',
            'description_ge:ntext',
            'image',
            'created_at:date',
            [
                'label' => $model->getAttributeLabel('status'),
                'value' => function($model){
                    return ActiveInactiveStatus::labelOf($model->status);
                }
            ],
        ],
    ]) ?>

</div>

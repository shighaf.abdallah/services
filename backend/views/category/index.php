<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

use common\enums\ActiveInactiveStatus;

/* @var $this yii\web\View */
/* @var $searchModel common\models\generated\search\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
setViewParam('liActive', 'category');
setViewParam('liinActive', 'category_index');
?>
<div class="category-index">

    <p>
        <?= Html::a('Create Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php $form = ActiveForm::begin(); ?>
    <?php Pjax::begin([
        'id' => 'vendor-grid-pjax',
        ])
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name_en',
            //'name_ge',
            'description_en:ntext',
            //'description_ge:ntext',
            'image',
            [
                'label' => '',
                'value' => function($model) {
                    $btns = '';
                    if ($model->status == ActiveInactiveStatus::active) {
                        $btns = Html::button(\Yii::t('all', 'Deactivate'), [
                            'class' => 'activity-view-link btn btn-sm btn-danger',
                            'data-toggle' => 'modal',
                            'data-target' => '#myModal',
                            'value' => Yii::$app->urlManager->createUrl("/category/change-status?id=$model->id&s=" . ActiveInactiveStatus::inactive)]);
                    } else {
                        $btns = Html::button(\Yii::t('all', 'Activate'), [
                            'class' => 'activity-view-link btn btn-sm btn-success ',
                            'data-toggle' => 'modal',
                            'data-target' => '#myModal',
                            'value' => Yii::$app->urlManager->createUrl("/category/change-status?id=$model->id&s=" . ActiveInactiveStatus::active)]);
                    }
                    return $btns;
                },
                'format' => 'raw',
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end() ?>
    <?php ActiveForm::end(); ?>


</div>

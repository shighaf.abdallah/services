<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;
use bupy7\cropbox\CropboxWidget;
use dosamigos\datepicker\DatePicker;
use common\enums\ContainerInventoryStatus;

/* @var $this yii\web\View */
/* @var $model common\models\Certificate */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title"><?= $title ?></h4>
</div>
<?php
$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);
?>
<div class="modal-body">
    <input name="any" value="1" class="hidden">
    <?= $text ?>

</div>
<div class="modal-footer">
    <?= Html::submitButton('Yes', ['class' => 'btn btn-success', 'autofocus' => 'true']) ?>
    <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cancel</button>

</div>
<?php ActiveForm::end(); ?>





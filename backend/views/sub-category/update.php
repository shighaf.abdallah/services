<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SubCategory */

$this->title = 'Update Sub Category: ' . $model->name_en;
$this->params['breadcrumbs'][] = ['label' => 'Sub Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
setViewParam('liActive', 'category');
setViewParam('liinActive', 'sub-category_index');
?>
<div class="sub-category-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

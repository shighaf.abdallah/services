<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\file\FileInput;

use common\enums\ActiveInactiveStatus;
use common\enums\Constents;

use common\models\Category;
/* @var $this yii\web\View */
/* @var $model common\models\SubCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sub-category-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-sm-12">
        <?= $form->field($model, 'cat_id')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Category::find()
                ->andWhere(['status'=> ActiveInactiveStatus::active])
                ->all(),'id','name_en'),
            'language' => 'en',
            'options' => ['placeholder' => 'Select Category'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
    </div>


    <div class="col-sm-6">
        <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($model, 'name_ge')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($model, 'description_en')->textarea(['rows' => 6]) ?>
    </div>

    <div class="col-sm-6">
        <?= $form->field($model, 'description_ge')->textarea(['rows' => 6]) ?>
    </div>

    <div class="col-sm-12">
        <?= $form->field($model, 'imageFile')->widget(FileInput::classname(), [
            'options' => ['accept' => 'image/*'],
            'pluginOptions' =>[
                'showUpload' => false,
                'initialPreview' =>!empty($model->image) ? $model->fileUrl : '',
                'initialPreviewAsData'=>true,
                'overwriteInitial'=>true,
            ]
        ]); ?>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

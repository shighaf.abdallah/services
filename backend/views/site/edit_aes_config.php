<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Loader */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="loader-form">

    <h3>Edit AES Config</h3>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_number_type')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'id_number')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'first_last_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'address_line_1')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'address_line_2')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'postal_code')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'loading_location_contact')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

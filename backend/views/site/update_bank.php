<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Loader */

$this->title = 'Update Bank Account';
$this->params['breadcrumbs'][] = ['label' => 'Loaders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loader-create">


    <?= $this->render('_form_bank', [
        'model' => $model,
    ]) ?>

</div>

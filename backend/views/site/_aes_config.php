 <?php
use yii\helpers\Html;
use yii\widgets\DetailView;
?>


 
    <h5 style="font-weight: bold;">AES Config</h5>
    <p>
        <?= Html::a('Edit', ['edit-aes-config'], ['class' => 'btn btn-sm btn-success']) ?>
    </p>
    <?= DetailView::widget([
        'model' => $aesModel,
        'attributes' => [
            'id_number_type',
            'id_number',
            'company_name',
            'first_last_name',
            'phone',
            'city',
            'address_line_1',
            'address_line_2',
            'postal_code',
            'state',
            'loading_location_contact'
        ],
    ]) ?>
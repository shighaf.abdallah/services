<?php
/* @var $this yii\web\View */

use yii\web\View;

setViewParam('liActive', 1);
$this->title = Yii::t('all', Yii::$app->params['title']) . ' ' .Yii::t('all','Dashboard');
?>
<div class="site-index">
    <div class="panel panel-default">
        <div class="panel-body">
            <p class="lead"><?= t('Welcome to the backend') . ' "' . user()->fullname . '"'; ?></p>
        </div>
    </div>
</div>



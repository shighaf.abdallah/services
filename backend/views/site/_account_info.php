<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\enums\ActiveInactiveStatus;


?>


<br/>
        <p>
            <?= Html::a('Create Bank Account', ['create-bank'], ['class' => 'btn btn-sm btn-success']) ?>
        </p>
        <?= GridView::widget([
            'dataProvider' => $bankInfoDataProvider,
//        'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'bank_name',
                'account_name',
                'business_account_address',
                'account_number',
                'routing_number',
                'create_date',
                [
                    'attribute' => 'status',
                    'value' => function($model) {
                        return ActiveInactiveStatus::LabelOfStyle($model->status);
                    },
                    'format' => 'raw',
                ],
                [
                    'label' => t(''),
                    'value' => function($model) {
                        $btns = Html::a('Edit', ['update-bank', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']);
                        if ($model->status != ActiveInactiveStatus::active) {
                            $btns = $btns . ' '. Html::button('Activate', [
                                    'style' => 'padding: 2px;margin-left: 3px;',
                                    'class' => 'activity-view-link btn btn-sm btn-warning',
                                    'data-toggle' => 'modal',
                                    'data-target' => '#myModal',
                                    'value' => Yii::$app->urlManager->createUrl("/site/change-bank-status?id=$model->id&s=" . ActiveInactiveStatus::active)]);
                        }
                        return $btns;
                    },
                    'format' => 'raw',
                ],

            ],
        ]); ?>



<?php

use common\models\PortOfLoading;
use webvimark\modules\UserManagement\models\ZUser;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\enums\ActiveInactiveStatus;
use yii\widgets\DetailView;

$this->title = "Configurations";
setViewParam('liActive', 'config');
setViewParam('liinActive', 'config');
?>
<div class="">
    <?php if (Yii::$app->session->hasFlash('flash_error')): ?>
        <div class="alert alert-danger" role="alert">
            <?= Yii::$app->session->getFlash('flash_error'); ?>
        </div>
    <?php endif; ?>
    <?php if (Yii::$app->session->hasFlash('flash_warning')): ?>
        <div class="alert alert-warning" role="warning">
            <?= Yii::$app->session->getFlash('flash_warning'); ?>
        </div>
    <?php endif; ?>
    <?php if (Yii::$app->session->hasFlash('flash_success')): ?>
        <div class="alert alert-success" role="alert">
            <?= Yii::$app->session->getFlash('flash_success'); ?>
        </div>
    <?php endif; ?>
    <div class="">

        
        <div class="panel panel-default executed_panel" id="config_Tabs">
            <div class="panel-body executed_body">
                <ul class="nav nav-tabs">
                    <li class="active"> <a data-toggle="tab" href="#site_config_tab" style="font-weight: bold">Site config</a></li>
                    <li><a data-toggle="tab" href="#aes_config_tab" style="font-weight: bold">Aes Config</a></li>
                    <li><a data-toggle="tab" href="#account_info_tab" style="font-weight: bold">Account Info</a></li>
                </ul>

                <div class="tab-content">
                    <div id="site_config_tab" class="tab-pane fade in active">
                        <?= $this->render('_site_config',[
                            'contact' => $contact,
                            'invoiceSentence' => $invoiceSentence
                         ]); ?>
                    </div>

                    <div id="aes_config_tab" class="tab-pane fade">
                        <?= $this->render('_aes_config',[
                            'aesModel' => $aesModel,
                         ]); ?>
                    </div>

                    <div id="account_info_tab" class="tab-pane fade">
                        <?= $this->render('_account_info',[
                            'bankInfoDataProvider' => $bankInfoDataProvider,
                        ]); ?>
                    </div>
                </div>

            </div>
        </div>




    </div>
</div>


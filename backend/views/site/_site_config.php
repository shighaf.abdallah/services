<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>



<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
            
        <div class="col-md-6">
            <br/>
            <?= $form->field($contact, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <br/>
            <?= $form->field($contact, 'info_email')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-md-6">
            <?= $form->field($contact, 'phone')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-md-6">
            <?= $form->field($contact, 'fax')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-md-6">
           <?= $form->field($contact, 'document_address')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-md-6">
            <?= $form->field($contact, 'document_title')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-md-6">
            <?= $form->field($contact, 'nvocc')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-md-6">
            <?= $form->field($contact, 'oti')->textInput(['maxlength' => true]) ?>
        </div>

        
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="form-group field-product-sku required">
                <label class="control-label" for="product-sku">Invoice button sentence</label>
                <textarea type="text" class="form-control" name="config[invoice_button_sentence ]"><?= $invoiceSentence->invoice_button_sentence ?></textarea>
                <div class="help-block"></div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-sm btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>
<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use backend\assets\DashboardAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\helpers\Url;
use yii\helpers\BaseUrl;
use common\models\Order;
use common\enums\OrderStatus;
use common\enums\ContainerInventoryStatus;

DashboardAsset::register($this);
$liActive = viewParam('liActive', '');
$liinActive = viewParam('liinActive', '');
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="description" content="overview &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <?= Html::csrfMetaTags() ?>
    <!--<link rel="icon" type="image/png" sizes="16x16" href="<?= imageURL("favicon.png") ?>">-->
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php DashboardAsset::register($this); ?>
</head>
<body class="no-skin">
<?php $this->beginBody() ?>
<div id="navbar" class="navbar navbar-default          ace-save-state">
    <div class="navbar-container ace-save-state" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
            <span class="sr-only">Toggle sidebar</span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>

            <span class="icon-bar"></span>
        </button>

        <div class="navbar-header pull-left">
            <a href="<?= Yii::$app->urlManager->createUrl("/") ?>" class="navbar-brand">
                <small>
                    <i class="fa fa-leaf"></i>
                    <?= Yii::$app->params['title'] . ' Backend' ?>
                </small>
            </a>
        </div>

        <div class="navbar-buttons navbar-header pull-right" role="navigation">
            <?php if (!isGuest()) { ?>
                <ul class="nav ace-nav">
                    <?php
                    $notifications = Order::find()->where(['status' => OrderStatus::sent])->all();
                    ?>
                    <?php if (\webvimark\modules\UserManagement\models\User::hasRole(["Admin"])) { ?>
                        <li class="purple dropdown-modal">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                <!--<i class="ace-icon fa fa-bell icon-animated-bell"></i>-->
                                <i class="ace-icon fa fa-bell"></i>
                                <span class="badge badge-important"><?= count($notifications) ?></span>
                            </a>

                            <ul class="dropdown-menu-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
                                <li class="dropdown-header">
                                    <i class="ace-icon fa fa-exclamation-triangle"></i>
                                    <?= count($notifications) ?> Notifications
                                </li>

                                <li class="dropdown-content">
                                    <ul class="dropdown-menu dropdown-navbar navbar-pink">
                                        <?php foreach ($notifications as $oneNote) { ?>
                                            <li>
                                                <a href="<?= Yii::$app->urlManager->createUrl("/order/view?id=$oneNote->id") ?>">
                                                    <div class="clearfix">
                                                                <span class="pull-left">
                                                                    <i class="btn btn-xs no-hover btn-pink fa fa-send"></i>
                                                                    New Order
                                                                </span>
                                                        <span class="pull-right badge badge-info"><?= date('m/d h:i A', strtotime($oneNote->date)) ?></span>
                                                    </div>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </li>

                                <li class="dropdown-footer">
                                    <a href="<?= Yii::$app->urlManager->createUrl("/order/new") ?>">
                                        See all
                                        <i class="ace-icon fa fa-arrow-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>


                    <li class="light-blue dropdown-modal">
                        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                            <img class="nav-user-photo" src="<?= user()->imageUrl ?>" alt="Jason's Photo" />
                            <span class="user-info">
                                        <small>Welcome,</small>
                                <?= user()->fullname ?>
                                    </span>

                            <i class="ace-icon fa fa-caret-down"></i>
                        </a>

                        <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                            <li>
                                <a href="<?= Yii::$app->urlManager->createUrl("/user/profile") ?>">
                                    <i class="ace-icon fa fa-user"></i>
                                    Profile
                                </a>
                            </li>

                            <li class="divider"></li>

                            <li>
                                <a href="<?= Yii::$app->urlManager->createUrl("/user/auth/logout") ?>">
                                    <i class="ace-icon fa fa-power-off"></i>
                                    Logout
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            <?php } else { ?>
                <ul class="nav ace-nav">
                    <li class="light-blue dropdown-modal">
                        <a href="<?= Yii::$app->urlManager->createUrl("/user/auth/registration") ?>">
                            Register
                        </a>
                    </li>

                    <li class="light-blue dropdown-modal">
                        <a href="<?= Yii::$app->urlManager->createUrl("/user/auth/login") ?>">
                            Login
                        </a>
                    </li>
                </ul>
            <?php } ?>
        </div>
    </div><!-- /.navbar-container -->
</div>
<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <div id="sidebar" class="sidebar responsive ace-save-state">
        <script type="text/javascript">
            try {
                ace.settings.loadState('sidebar')
            } catch (e) {
            }
        </script>

        <!-- /.sidebar-shortcuts -->

        <ul class="nav nav-list">
            <li class="<?php
            if ($liActive == 1) {
                echo 'active';
            }
            ?>">
                <a href="<?= Yii::$app->urlManager->createUrl("/") ?>">
                    <i class="menu-icon fa fa-home"></i>
                    <span class="menu-text"> Dashboard </span>
                </a>
                <b class="arrow"></b>
            </li>

            <li class="<?php
            if ($liActive == 2) {
                echo 'active open';
            }
            ?> open">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-dashboard"></i>
                    <span class="menu-text">
                                Inventory
                            </span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <?php if (\webvimark\modules\UserManagement\models\User::hasRole(["ADD_IMAGES_INVENTORY"]) && !\webvimark\modules\UserManagement\models\User::hasRole(["Admin"])) { ?>
                        <li class="">
                            <a style="background: #4b9cca; color: white;" href="<?= Yii::$app->urlManager->createUrl("/inventory/inventory-images?id=") ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Add Inventory Images
                            </a>
                            <b class="arrow"></b>
                        </li>
                    <?php } else if (\webvimark\modules\UserManagement\models\User::hasRole(["ADD_IMAGES_CONTAINER"]) && !\webvimark\modules\UserManagement\models\User::hasRole(["Admin"])) { ?>
                        <li class="">
                            <a style="background: #4b9cca; color: white;" href="<?= Yii::$app->urlManager->createUrl("/inventory/container-images?id=") ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Add Container Images
                            </a>
                            <b class="arrow"></b>
                        </li>
                    <?php } else { ?>
                        <li class="open">
                            <a style="background: #438eb9; color: white" href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-car"></i>
                                <span class="menu-text">
                                            Inventories
                                        </span>
                                <b class="arrow fa fa-angle-down"></b>
                            </a>
                            <b class="arrow"></b>
                            <ul class="submenu">
                                <?php if (\webvimark\modules\UserManagement\models\User::hasRole(["Admin"])) { ?>
                                    <li class="<?php
                                    if ($liinActive == 'find_cargo') {
                                        echo 'active';
                                    }
                                    ?>">
                                        <a style="background: #4b9cca; color: white;" href="<?= Yii::$app->urlManager->createUrl("/inventory/find-cargo") ?>">
                                            <i class="menu-icon fa fa-caret-right"></i>
                                            Find Cargo
                                        </a>
                                        <b class="arrow"></b>
                                    </li>
                                <?php } ?>
                                <li class="<?php
                                if ($liinActive == 1) {
                                    echo 'active';
                                }
                                ?>">
                                    <a style="background: #54aadc; color: white;" href="<?= Yii::$app->urlManager->createUrl("/inventory") ?>">
                                        <i class="menu-icon fa fa-caret-right"></i>
                                        In Warehouse
                                    </a>
                                    <b class="arrow"></b>
                                </li>
                                <?php if (\webvimark\modules\UserManagement\models\User::hasRole(["Admin"])) { ?>
                                    <li class="<?php
                                    if ($liinActive == 'in_con') {
                                        echo 'active';
                                    }
                                    ?>">
                                        <a style="background: #5db8ed; color: white;" href="<?= Yii::$app->urlManager->createUrl("/inventory/container") ?>">
                                            <i class="menu-icon fa fa-caret-right"></i>
                                            In Container
                                        </a>
                                        <b class="arrow"></b>
                                    </li>
                                <?php } ?>
                                <li class="<?php
                                if ($liinActive == 'create_inv_multi') {
                                    echo 'active';
                                }
                                ?>">
                                    <a style="background: #66c7ff; color: white;" href="<?= Yii::$app->urlManager->createUrl("/inventory/create-multi") ?>">
                                        <i class="menu-icon fa fa-caret-right"></i>
                                        <!-- Create Inventory -->
                                        Warehouse Delivery
                                    </a>
                                    <b class="arrow"></b>
                                </li>
                                <?php if (\webvimark\modules\UserManagement\models\User::hasRole(["Admin"])) { ?>
                                    <!--                                            <li class="<?php
                                    if ($liinActive == 'inv_title') {
                                        echo 'active';
                                    }
                                    ?>">
                                                <a style="background: #77caf9; color: white;" href="<?= Yii::$app->urlManager->createUrl("/inventory-title") ?>">
                                                    <i class="menu-icon fa fa-caret-right"></i>
                                                    Inventory Title
                                                </a>
                                                <b class="arrow"></b>
                                            </li>-->
                                    <li class="<?php
                                    if ($liinActive == 'released_inv') {
                                        echo 'active';
                                    }
                                    ?>">
                                        <a style="background: #77caf9; color: white;" href="<?= Yii::$app->urlManager->createUrl("/released-inventory") ?>">
                                            <i class="menu-icon fa fa-caret-right"></i>
                                            Released Inventories
                                        </a>
                                        <b class="arrow"></b>
                                    </li>
                                <?php } ?>
                                <?php if (\webvimark\modules\UserManagement\models\User::hasRole(["Admin"])) { ?>
                                    <!--                                            <li class="<?php
                                    if ($liinActive == 'test') {
                                        echo 'active';
                                    }
                                    ?>">
                                                                                            <a style="background: #66c7ff; color: white;" href="<?= Yii::$app->urlManager->createUrl("/test/test") ?>">
                                                                                                <i class="menu-icon fa fa-caret-right"></i>
                                                                                                Test For Accounting
                                                                                            </a>
                                                                                            <b class="arrow"></b>
                                                                                        </li>-->
                                <?php } ?>
                                <!--                                    <li class="<?php
                                if ($liinActive == 'create_inv') {
                                    echo 'active';
                                }
                                ?>">
                                                                                <a style="background: #66c7ff; color: white;" href="<?= Yii::$app->urlManager->createUrl("/inventory/create") ?>">
                                                                                    <i class="menu-icon fa fa-caret-right"></i>
                                                                                    Create Inventory
                                                                                </a>
                                                                                <b class="arrow"></b>
                                                                            </li>

                                                                            <li class="<?php
                                if ($liinActive == 'create_inv_multi') {
                                    echo 'active';
                                }
                                ?>">
                                                                                <a style="background: #85c8ef; color: white;" href="<?= Yii::$app->urlManager->createUrl("/inventory/create-multi") ?>">
                                                                                    <i class="menu-icon fa fa-caret-right"></i>
                                                                                    Create Inventory Multi
                                                                                </a>
                                                                                <b class="arrow"></b>
                                                                            </li>-->
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if (\webvimark\modules\UserManagement\models\User::hasRole(["CREATE_LOADING_PLAN"])) { ?>
                        <li class="open">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-file-text"></i>
                                <span class="menu-text">
                                            Loading Plans
                                        </span>
                                <b class="arrow fa fa-angle-down"></b>
                            </a>
                            <b class="arrow"></b>
                            <ul class="submenu">
                                <li class="<?php
                                if ($liinActive == 'loading_plans') {
                                    echo 'active';
                                }
                                ?>">
                                    <a style="color: black;" href="<?= Yii::$app->urlManager->createUrl("/inventory/loading-plans") ?>">
                                        <i class="menu-icon fa fa-caret-right"></i>
                                        Pending Loadings
                                    </a>
                                    <b class="arrow"></b>
                                </li>
                                <li class="<?php
                                if ($liinActive == 'create_loading_plan') {
                                    echo 'active';
                                }
                                ?>">
                                    <a style="color: black;" href="<?= Yii::$app->urlManager->createUrl("/container-inventory/create-loading-plan") ?>">
                                        <i class="menu-icon fa fa-caret-right"></i>
                                        Create Loading Plan
                                    </a>
                                    <b class="arrow"></b>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                    <?php if (\webvimark\modules\UserManagement\models\User::hasRole(["Admin"])) { ?>

                        <li class="open">
                            <a href="#" class="dropdown-toggle">
                                <i class="menu-icon fa fa-ship"></i>
                                <span class="menu-text">
                                            Containers
                                        </span>
                                <b class="arrow fa fa-angle-down"></b>
                            </a>
                            <b class="arrow"></b>
                            <ul class="submenu">
                                <li class="<?php
                                if ($liinActive == 'shipped_containers') {
                                    echo 'active';
                                }
                                ?>">
                                    <a style="color: black;" href="<?= Yii::$app->urlManager->createUrl("/inventory/by-container?id=1") ?>">
                                        <i class="menu-icon fa fa-caret-right"></i>
                                        Shipped Containers
                                    </a>
                                    <b class="arrow"></b>
                                </li>
                                <li class="<?php
                                if ($liinActive == 'delivered_containers') {
                                    echo 'active';
                                }
                                ?>">
                                    <a style="color: black;" href="<?= Yii::$app->urlManager->createUrl("/inventory/by-container?id=2") ?>">
                                        <i class="menu-icon fa fa-caret-right"></i>
                                        Delivered Containers
                                    </a>
                                    <b class="arrow"></b>
                                </li>
                                <li class="<?php
                                if ($liinActive == 'by_con') {
                                    echo 'active';
                                }
                                ?>">
                                    <a style="color: black;" href="<?= Yii::$app->urlManager->createUrl("/inventory/by-container") ?>">
                                        <i class="menu-icon fa fa-caret-right"></i>
                                        All Containers
                                    </a>
                                    <b class="arrow"></b>
                                </li>
                            </ul>
                        </li>
                    <?php } ?>
                </ul>
            </li>
            <?php if (\webvimark\modules\UserManagement\models\User::hasRole(["Admin"])) { ?>
                <li class="<?php
                if ($liActive == 'users') {
                    echo 'active open';
                }
                ?>">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-users"></i>
                        <span class="menu-text">
                                    Users Manage
                                </span>
                        <b class="arrow fa fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>
                    <ul class="submenu">
                        <li class="<?php
                        if ($liinActive == 'users_all') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/user/user/index") ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                All
                            </a>
                            <b class="arrow"></b>
                        </li>
                    </ul>
                </li>
            <?php } ?>
            <?php if (\webvimark\modules\UserManagement\models\User::hasRole(["Admin"])) { ?>
                <li class="<?php
                if ($liActive == 'admins') {
                    echo 'active open';
                }
                ?>">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-users"></i>
                        <span class="menu-text">
                                    Admins Manage
                                </span>
                        <b class="arrow fa fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>
                    <ul class="submenu">
                        <li class="<?php
                        if ($liinActive == 'admins_all') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/user/admin/index") ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                All
                            </a>
                            <b class="arrow"></b>
                        </li>
                    </ul>
                </li>
            <?php } ?>

            <?php if (\webvimark\modules\UserManagement\models\User::hasRole(["Admin"])) { ?>
                <li class="<?php
                if ($liActive == 'order') {
                    echo 'active open';
                }
                ?>">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-send"></i>
                        <span class="menu-text">
                                    Orders
                                </span>
                        <b class="arrow fa fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>
                    <ul class="submenu">
                        <li class="<?php
                        if ($liinActive == 'order_all') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/order") ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                All
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <li class="<?php
                        if ($liinActive == 'order_archived') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/order/archived") ?>">
                                <i class="menu-icon fa fa-caret-right"></i>
                                Archived
                            </a>
                            <b class="arrow"></b>
                        </li>
                    </ul>
                </li>
            <?php } ?>
            <?php if (\webvimark\modules\UserManagement\models\User::hasRole(["Admin"])) { ?>
                <li class="<?php
                if ($liActive == 'email-record') {
                    echo 'active';
                }
                ?>">
                    <a href="<?= Yii::$app->urlManager->createUrl("/email-record") ?>">
                        <i class="menu-icon fa fa-send-o"></i>
                        <span class="menu-text"> Email Records </span>
                    </a>
                    <b class="arrow"></b>
                </li>
            <?php } ?>
            <?php if (\webvimark\modules\UserManagement\models\User::hasRole(["Admin"])) { ?>
                <li class="<?php
                if ($liActive == 'config') {
                    echo 'active open';
                }
                ?>">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-cogs"></i>
                        <span class="menu-text">
                                    Configuration
                                </span>
                        <b class="arrow fa fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>
                    <ul class="submenu">
                        <li class="<?php
                        if ($liinActive == 'places') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/place") ?>">
                                <i class="menu-icon fa fa-map-marker"></i>
                                <span class="menu-text"> Destinations </span>
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <li class="<?php
                        if ($liinActive == 'ss_line') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/ss-line") ?>">
                                <i class="menu-icon fa fa-tags"></i>
                                <span class="menu-text"> SS Lines </span>
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <li class="<?php
                        if ($liinActive == 'port_of_loading') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/port-of-loading") ?>">
                                <i class="menu-icon fa fa-tags"></i>
                                <span class="menu-text"> Port of Loading </span>
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <li class="<?php
                        if ($liinActive == 'inventory_weight') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/inventory-weight") ?>">
                                <i class="menu-icon fa fa-tags"></i>
                                <span class="menu-text"> Inventory Weights </span>
                            </a>
                            <b class="arrow"></b>
                        </li>


















                        <li class="<?php
                        if ($liinActive == 'vendor') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/vendor") ?>">
                                <i class="menu-icon fa fa-tags"></i>
                                <span class="menu-text"> Vendors </span>
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <li class="<?php
                        if ($liinActive == 'loader') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/loader") ?>">
                                <i class="menu-icon fa fa-tags"></i>
                                <span class="menu-text"> Loaders </span>
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <li class="<?php
                        if ($liinActive == 'booking-provider') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/booking-provider") ?>">
                                <i class="menu-icon fa fa-tags"></i>
                                <span class="menu-text"> Booking Providers </span>
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <li class="<?php
                        if ($liinActive == 'trucking-company') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/trucking-company") ?>">
                                <i class="menu-icon fa fa-tags"></i>
                                <span class="menu-text"> Trucking Companies </span>
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <li class="<?php
                        if ($liinActive == 'vessel-voyage') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/vessel-voyage") ?>">
                                <i class="menu-icon fa fa-tags"></i>
                                <span class="menu-text"> Vessel/Voyage </span>
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <li class="<?php
                        if ($liinActive == 'cover-sheet-terminal') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/cover-sheet-terminal") ?>">
                                <i class="menu-icon fa fa-tags"></i>
                                <span class="menu-text"> Terminal </span>
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <li class="<?php
                        if ($liinActive == 'contianer-equp-re') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/contianer-equp-re") ?>">
                                <i class="menu-icon fa fa-tags"></i>
                                <span class="menu-text"> EQUP-RE </span>
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <li class="<?php
                        if ($liinActive == 'title-state') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/title-state") ?>">
                                <i class="menu-icon fa fa-tags"></i>
                                <span class="menu-text"> Title State </span>
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <li class="<?php
                        if ($liinActive == 'payment-by') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/payment-by") ?>">
                                <i class="menu-icon fa fa-tags"></i>
                                <span class="menu-text"> Payment By </span>
                            </a>
                            <b class="arrow"></b>
                        </li>




















                        <li class="<?php
                        if ($liinActive == 'export') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/site/export-sql") ?>">
                                <i class="menu-icon fa fa-cog"></i>
                                <span class="menu-text"> Export SQL </span>
                            </a>
                            <b class="arrow"></b>
                        </li>

                        <li class="<?php
                        if ($liinActive == 'import') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/site/import-sql") ?>">
                                <i class="menu-icon fa fa-cog"></i>
                                <span class="menu-text">Import SQL</span>
                            </a>
                            <b class="arrow"></b>
                        </li>

                        <li class="<?php
                        if ($liinActive == 'sync') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/site/sync") ?>">
                                <i class="menu-icon fa fa-cog"></i>
                                <span class="menu-text">Sync</span>
                            </a>
                            <b class="arrow"></b>
                        </li>






                        <li class="<?php
                        if ($liinActive == 'config') {
                            echo 'active';
                        }
                        ?>">
                            <a href="<?= Yii::$app->urlManager->createUrl("/site/config") ?>">
                                <i class="menu-icon fa fa-cog"></i>
                                <span class="menu-text"> Configuration </span>
                            </a>
                            <b class="arrow"></b>
                        </li>
                    </ul>
                </li>



            <?php } ?>
        </ul><!-- /.nav-list -->

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
    </div>

    <div class="main-content">
        <div class="main-content-inner">
            <div class="breadcrumbs ace-save-state" id="breadcrumbs">
                <ul class="breadcrumb">
                    <li>
                        <i class="ace-icon fa fa-home home-icon"></i>
                        <a href="<?= Yii::$app->urlManager->createUrl("/") ?>">Home</a>
                    </li>
                    <?php if (\webvimark\modules\UserManagement\models\User::hasRole(["Admin"])) { ?>
                        <li>
                            <i class="ace-icon fa fa-money"></i>
                            <a href="<?= Yii::$app->urlManager->createUrl("/accounting") ?>">Accounting System</a>
                        </li>
                    <?php } ?>
                </ul><!-- /.breadcrumb -->
            </div>

            <div class="page-content">
                <div class="ace-settings-container" id="ace-settings-container">
                    <div class="btn btn-app btn-xs btn-warning ace-settings-btn" id="ace-settings-btn">
                        <i class="ace-icon fa fa-cog bigger-130"></i>
                    </div>

                    <div class="ace-settings-box clearfix" id="ace-settings-box">
                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <div class="pull-left">
                                    <select id="skin-colorpicker" class="hide">
                                        <option data-skin="no-skin" value="#438EB9">#438EB9</option>
                                        <option data-skin="skin-1" value="#222A2D">#222A2D</option>
                                        <option data-skin="skin-2" value="#C6487E">#C6487E</option>
                                        <option data-skin="skin-3" value="#D0D0D0">#D0D0D0</option>
                                    </select>
                                </div>
                                <span>&nbsp; Choose Skin</span>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-navbar" autocomplete="off" />
                                <label class="lbl" for="ace-settings-navbar"> Fixed Navbar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-sidebar" autocomplete="off" />
                                <label class="lbl" for="ace-settings-sidebar"> Fixed Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-breadcrumbs" autocomplete="off" />
                                <label class="lbl" for="ace-settings-breadcrumbs"> Fixed Breadcrumbs</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-rtl" autocomplete="off" />
                                <label class="lbl" for="ace-settings-rtl"> Right To Left (rtl)</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2 ace-save-state" id="ace-settings-add-container" autocomplete="off" />
                                <label class="lbl" for="ace-settings-add-container">
                                    Inside
                                    <b>.container</b>
                                </label>
                            </div>
                        </div><!-- /.pull-left -->

                        <div class="pull-left width-50">
                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-hover" autocomplete="off" />
                                <label class="lbl" for="ace-settings-hover"> Submenu on Hover</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-compact" autocomplete="off" />
                                <label class="lbl" for="ace-settings-compact"> Compact Sidebar</label>
                            </div>

                            <div class="ace-settings-item">
                                <input type="checkbox" class="ace ace-checkbox-2" id="ace-settings-highlight" autocomplete="off" />
                                <label class="lbl" for="ace-settings-highlight"> Alt. Active Item</label>
                            </div>
                        </div><!-- /.pull-left -->
                    </div><!-- /.ace-settings-box -->
                </div><!-- /.ace-settings-container -->
                <?= $content ?>
            </div><!-- /.page-content -->
        </div>
    </div><!-- /.main-content -->

    <div class="footer">
        <div class="footer-inner">
            <div class="footer-content">
                        <span class="bigger-120">
                            <span class="blue bolder"><?= Yii::$app->params['title'] ?></span>
                            &copy; <?= date('Y') ?>
                        </span>

                &nbsp; &nbsp;
                <span class="action-buttons">
                            <a href="#">
                                <i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
                            </a>

                            <a href="#">
                                <i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
                            </a>

                            <a href="#">
                                <i class="ace-icon fa fa-rss-square orange bigger-150"></i>
                            </a>
                        </span>
            </div>
        </div>
    </div>
    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
    </a>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<script>
    var url = <?= json_encode(yii\helpers\BaseUrl::base()) ?>;
    if ('ontouchstart' in document.documentElement)
        document.write("<script src='" + url + "/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>

<style>
    input {
        text-transform: uppercase;
    }
    th {
        border: 1px solid black !important;
    }
    .form-group.has-success input, .form-group.has-success select, .form-group.has-success textarea {
        border-color: #9cc573;
        color: #8BAD4C;
        -webkit-box-shadow: none;
        box-shadow: none;
        background: #faffbd;
    }
</style>











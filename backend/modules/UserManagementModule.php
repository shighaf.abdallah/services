<?php

namespace backend\modules;

use Yii;
use yii\helpers\ArrayHelper;
use webvimark\modules\UserManagement\UserManagementModule as BaseUserManagementModule;

class UserManagementModule extends BaseUserManagementModule
{
    public $controllerNamespace = 'backend\controllers\user';
}

<?php

namespace frontend\controllers;

use Yii;
use yii\filters\AccessControl;
use common\utils\Controller;
use yii\filters\VerbFilter;
use common\models\ContactForm;
use common\models\generated\Contact;
use common\models\AboutUs;
use webvimark\modules\UserManagement\models\ZUser;
use webvimark\modules\UserManagement\libs\LibUser;
use yii\helpers\Url;
use yii2mod\swagger\OpenAPIRenderer;
use yii2mod\swagger\SwaggerUIRenderer;

class SiteController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'error', 'contact', 'captcha', 'about', 'lang', 'auth'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'facebookLogin'],
            ],
        ];
    }

    public function actionIndex() {

        return $this->render('index', [
        ]);
    }

    public function actionContact() {
        $model = new ContactForm();
        $email = Contact::findOne(1)->email;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail($email)) {
                Yii::$app->session->setFlash('flash_success', 'Your massage is received and will be answered shortly.');
            } else {
                Yii::$app->session->setFlash('flash_error', 'Error in sending massage, Try again later');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                        'model' => $model,
            ]);
        }
    }

    public function actionAbout() {
        $model = AboutUs::find()->one();
        if (!$model) {
            stopv('About Model Not Found');
        }
        return $this->render('about', [
                    'model' => $model,
        ]);
    }

    public function actionLang($l = 'en') {
        Yii::$app->session->set('lang', $l);
        return $this->redirect(['index']);
    }

    public function facebookLogin($client) {
        $attributes = $client->getUserAttributes();

        $auth = ZUser::find()
                ->where(['facebook_id' => $attributes['id']])
                ->orWhere(['email' => $attributes['email']])
                ->one();
        $accessToken = $client->getAccessToken()->getToken();

        $accept = FALSE;
        if ($auth) { // login
            $attributes['access_token'] = $accessToken;
            $this->updateUserInfo($auth, $attributes);
            $accept = TRUE;
        } else { // signup
            if ($attributes['email'] !== null && ZUser::find()->where(['email' => $attributes['email']])->exists()) {
                stopv('email already used');
            } else {
                $password = Yii::$app->security->generateRandomString(6);
                list($res, $auth, $msg) = LibUser::profileFbCreate([
                            'username' => $attributes['name'],
                            'email' => isset($attributes['email']) ? $attributes['email'] : $attributes['id'] . '@facebook.com',
                            'facebook_id' => $attributes['id'],
                            'facebook_name' => $attributes['name'],
                            'facebook_email' => $attributes['email'],
                            'facebook_access_token' => $accessToken,
                            'created_at' => time(),
                            'updated_at' => time(),
                            'password' => $password,
                ]);
                if ($res) {
                    $accept = TRUE;
                } else {
                    stopv($msg);
                }
            }
        }

        if ($accept) {
            Yii::$app->user->login($auth);
        }
    }

    private function updateUserInfo($user, $attributes) {
        $user['facebook_name'] = $attributes['name'];
        $user['username'] = $attributes['name'];
        $user['facebook_access_token'] = $attributes['access_token'];
        $user['facebook_id'] = $attributes['id'];
        if (isset($attributes['email'])) {
            $user['email'] = isset($attributes['email']) ? $attributes['email'] : $attributes['id'] . '@facebook.com';
        }

        if (!$user->save()) {
            stopv($user->errors);
        }
        return TRUE;
    }

}

<?php
/* @var $this \yii\web\View */

/* @var $content string */

use frontend\assets\DashboardAsset;
use yii\helpers\Html;
use common\models\Admin;

$liActive = viewParam('liActive', '');
$liinActive = viewParam('liinActive', '');
$liininActive = viewParam('liininActive', '');
DashboardAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <?php
    DashboardAsset::register($this);
    ?>
    <link rel="apple-touch-icon" href="<?= imageURL('apple-icon-120.png') ?>') ?>">
    <link rel="shortcut icon" type="image/x-icon" href="<?= imageURL('favicon.ico') ?>">

</head>
<body class="hold-transition skin-blue sidebar-mini">

<?php $this->beginBody() ?>
<div class="wrapper">
    <header class="main-header">
        <!-- Logo -->
        <a href="<?= Yii::$app->urlManager->createUrl("/") ?>" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>S</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><?= Yii::$app->params['title'] ?></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">

                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?= user()->imageUrl ?>" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?= user()->fullname ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="<?= user()->imageUrl ?>" class="img-circle" alt="User Image">

                                <p>
                                    <?= user()->fullname ?>
                                    <small>Member since <?= date('M Y', user()->created_at) ?></small>
                                </p>
                            </li>
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="<?= Yii::$app->urlManager->createUrl("/user/profile") ?>" class="btn btn-default btn-flat">
                                        Profile
                                    </a>
                                </div>
                                <div class="pull-right">
                                    <a href="<?= Yii::$app->urlManager->createUrl("/user/auth/logout") ?>" class="btn btn-default btn-flat">
                                        Sign out
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">MAIN NAVIGATION</li>
                <li>
                    <a href="<?= Yii::$app->urlManager->createUrl("/") ?>">
                        <i class="fa fa-th"></i> <span>Dashboard</span>
                        <span class="pull-right-container"></span>
                    </a>
                </li>

            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <?= $this->title ?>
            </h1>
            <ol class="breadcrumb">
                <li>
                    <a href="<?= Yii::$app->urlManager->createUrl("/") ?>">
                        <i class="fa fa-dashboard"></i> Home
                    </a>
                </li>
                <li class="active"><?= $this->title ?></li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <?= $content ?>
        </section>
        <!-- /.content -->
    </div>

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0
        </div>
        <strong>Copyright &copy; 2020.</strong> All rights
        reserved.
    </footer>
</div>

<div class="modal fade" id="myModal" data-loading="<?= \Yii::t('all', 'Loading...') ?>"
     data-close="<?= \Yii::t('all', 'Close') ?>" tabindex="-1" aria-labelledby="myModalLabel" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog" id="modalContent" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <?= \Yii::t('all', 'Loading...') ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" style=""><?= \Yii::t('all', 'Close') ?></button>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<script>
    var url = <?= json_encode(yii\helpers\BaseUrl::base()) ?>;
    var lang = <?= json_encode(Yii::$app->language) ?>;
</script>
<?php

use webvimark\modules\UserManagement\components\GhostHtml;
use webvimark\modules\UserManagement\models\rbacDB\Role;
use webvimark\modules\UserManagement\models\User;
use webvimark\modules\UserManagement\UserManagementModule;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var yii\web\View $this
 * @var webvimark\modules\UserManagement\models\User $model
 */
setViewParam('liActive', 'admins');
setViewParam('liinActive', 'admins_all');
$this->title = 'Admin: ' . $model->username;
$this->params['breadcrumbs'][] = ['label' => UserManagementModule::t('back', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">
    <?php if (Yii::$app->session->hasFlash('flash_success')): ?>
        <div class="alert alert-success" role="alert">
            <?= Yii::$app->session->getFlash('flash_success'); ?>
        </div>
    <?php endif; ?>

    <h2 class="lte-hide-title"><?= $this->title ?></h2>

    <div class="panel panel-default">
        <div class="panel-body">

            <p>
                <?= GhostHtml::a(UserManagementModule::t('back', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                <?= GhostHtml::a(UserManagementModule::t('back', 'Create'), ['create'], ['class' => 'btn btn-sm btn-success']) ?>
                <?=
                GhostHtml::a(
                        UserManagementModule::t('back', 'Roles and permissions'), ['/user/admin-permission/set', 'id' => $model->id], ['class' => 'btn btn-sm btn-default']
                )
                ?>

                <?= Html::a(UserManagementModule::t('back', 'Change Password'), ['change-password', 'id' => $model->id], ['class' => 'btn btn-sm btn-warning']) ?>

                <?=
                GhostHtml::a(UserManagementModule::t('back', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-sm btn-danger pull-right',
                    'data' => [
                        'confirm' => UserManagementModule::t('back', 'Are you sure you want to delete this user?'),
                        'method' => 'post',
                    ],
                ])
                ?>
            </p>

            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                        'attribute' => 'status',
                        'value' => User::getStatusValue($model->status),
                    ],
                    'username',
                    'fname',
                    'lname',
                    [
                        'attribute' => 'email',
                        'value' => $model->email,
                        'format' => 'email',
                        'visible' => User::hasPermission('viewUserEmail'),
                    ],
                    [
                        'attribute' => 'email_confirmed',
                        'value' => $model->email_confirmed,
                        'format' => 'boolean',
                        'visible' => User::hasPermission('viewUserEmail'),
                    ],
                    'phone',
                    [
                        'label' => UserManagementModule::t('back', 'Roles'),
                        'value' => implode('<br>', ArrayHelper::map(Role::getUserRoles($model->id), 'name', 'description')),
                        'visible' => User::hasPermission('viewUserRoles'),
                        'format' => 'raw',
                    ],
//					[
//						'attribute'=>'bind_to_ip',
//						'visible'=>User::hasPermission('bindUserToIp'),
//					],
//					array(
//						'attribute'=>'registration_ip',
//						'value'=>Html::a($model->registration_ip, "http://ipinfo.io/" . $model->registration_ip, ["target"=>"_blank"]),
//						'format'=>'raw',
//						'visible'=>User::hasPermission('viewRegistrationIp'),
//					),
                    'created_at:datetime',
                    'updated_at:datetime',
                ],
            ])
            ?>

        </div>
    </div>
</div>

<?php if ($model->superadmin) { ?>
    <div class="panel panel-default" style="background: #dbf2ff">
        <div class="panel-body">
            <div class="col-xs-12">
                <h3 class="header smaller lighter green">Configuration</h3>

                <p></p>

                <a style="width: 20%;" href="<?= Yii::$app->urlManager->createUrl("/accounting/change-password") ?>" class="btn btn-default btn-app radius-4">
                    <i class="ace-icon fa fa-cog bigger-230"></i>
                    Accounting Password
                    <span class="badge badge-pink"></span>
                </a>
                <a style="width: 20%;" href="<?= Yii::$app->urlManager->createUrl("/accounting/change-config-password?id=1") ?>" class="btn btn-default btn-app radius-4">
                    <i class="ace-icon fa fa-cog bigger-230"></i>
                    Container Back Password
                    <span class="badge badge-pink"></span>
                </a>
                <a style="width: 20%;" href="<?= Yii::$app->urlManager->createUrl("/accounting/change-config-password?id=2") ?>" class="btn btn-default btn-app radius-4">
                    <i class="ace-icon fa fa-cog bigger-230"></i>
                    Invoices Edit Password
                    <span class="badge badge-pink"></span>
                </a>
                <a style="width: 20%;" href="<?= Yii::$app->urlManager->createUrl("/accounting/change-config-password?id=3") ?>" class="btn btn-default btn-app radius-4">
                    <i class="ace-icon fa fa-cog bigger-230"></i>
                    Inventory Edit Prices Password
                    <span class="badge badge-pink"></span>
                </a>
                <a style="width: 20%;" href="<?= Yii::$app->urlManager->createUrl("/accounting/change-config-password?id=4") ?>" class="btn btn-default btn-app radius-4">
                    <i class="ace-icon fa fa-cog bigger-230"></i>
                    Inventory Edit/Update Password
                    <span class="badge badge-pink"></span>
                </a>
                <a style="width: 20%;" href="<?= Yii::$app->urlManager->createUrl("/accounting/change-config-password?id=5") ?>" class="btn btn-default btn-app radius-4">
                    <i class="ace-icon fa fa-cog bigger-230"></i>
                    Inventory Delete Password
                    <span class="badge badge-pink"></span>
                </a>
                <a style="width: 20%;" href="<?= Yii::$app->urlManager->createUrl("/accounting/change-config-password?id=6") ?>" class="btn btn-default btn-app radius-4">
                    <i class="ace-icon fa fa-cog bigger-230"></i>
                    Release Inventory Password
                    <span class="badge badge-pink"></span>
                </a>
                <a style="width: 20%;" href="<?= Yii::$app->urlManager->createUrl("/accounting/change-config-password?id=7") ?>" class="btn btn-default btn-app radius-4">
                    <i class="ace-icon fa fa-cog bigger-230"></i>
                    Unprint Bill Password
                    <span class="badge badge-pink"></span>
                </a>
            </div>
        </div>


    </div>
<?php } ?>


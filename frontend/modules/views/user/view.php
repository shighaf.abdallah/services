<?php

use webvimark\modules\UserManagement\components\GhostHtml;
use webvimark\modules\UserManagement\models\rbacDB\Role;
use webvimark\modules\UserManagement\models\User;
use webvimark\modules\UserManagement\UserManagementModule;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var webvimark\modules\UserManagement\models\User $model
 */
$this->title = $model->username;
?>
<div class="user-view">



    <?php if (Yii::$app->session->hasFlash('flash_error')): ?>
        <div class="alert alert-danger" role="alert">
            <?= Yii::$app->session->getFlash('flash_error'); ?>
        </div>
    <?php endif; ?>
    <?php if (Yii::$app->session->hasFlash('flash_success')): ?>
        <div class="alert alert-success" role="alert">
            <?= Yii::$app->session->getFlash('flash_success'); ?>
        </div>
    <?php endif; ?>

    <div class="panel panel-default">
        <div class="panel-body">

            <p>
                <?= Html::a('Edit', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
                <?= Html::a('Create', ['create'], ['class' => 'btn btn-sm btn-success']) ?>
                <?= Html::a('Change Password', ['change-password', 'id' => $model->id], ['class' => 'btn btn-sm btn-warning']) ?>
                <?=
                Html::button('Add Attachment', ['class' => 'activity-view-link btn btn-sm btn-primary',
                    'style' => 'border-color: #475ca9; color: white; margin: 2px;background-color: #475ca9!important',
                    'value' => Yii::$app->urlManager->createUrl("/user/user/add-attachment?id=$model->id")])
                ?>
                <?=
                Html::a(UserManagementModule::t('back', 'Delete'), ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-sm btn-danger pull-right',
                    'data' => [
                        'confirm' => UserManagementModule::t('back', 'Are you sure you want to delete this user?'),
                        'method' => 'post',
                    ],
                ])
                ?>
            </p>

            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    [
                        'attribute' => 'status',
                        'value' => User::getStatusValue($model->status),
                    ],
                    'username',
                    'fname',
                    'lname',
                    [
                        'attribute' => 'email',
                        'value' => $model->email,
                        'format' => 'email',
                        'visible' => User::hasPermission('viewUserEmail'),
                    ],
                    [
                        'attribute' => 'email_confirmed',
                        'value' => $model->email_confirmed,
                        'format' => 'boolean',
                        'visible' => User::hasPermission('viewUserEmail'),
                    ],
                    'second_email',
                    'third_email',
                    'fourth_email',
                    'phone',
                    'address',
                    'city',
                    'state',
                    'zip_postal_code',
                    'fax_num',
                    'created_at:datetime',
                    'updated_at:datetime',
                    [
                        'attribute' => 'attachment',
                        'value' => function($model) {
                            return Html::img($model->attachmentUrl, ['class' => 'img-lg', 'style' => 'max-height: 200px;']);
                        },
                        'format' => 'raw',
                    ],
                    'due_date_days',
                    [
                        'attribute' => 'due_date_date_type',
                        'value' => function($model) {
                            return \common\enums\DateTypeEnum::LabelOf($model->due_date_date_type);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'due_date_before_or_after',
                        'value' => function($model) {
                            return $model->due_date_before_or_after ? 'After' : 'Before';
                        },
                        'format' => 'raw',
                    ],
                ],
            ])
            ?>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <h2>Shippers</h2>
            <p>
                <?= Html::button('Add Shipper', ['class' => 'activity-view-link btn btn-primary', 'style' => 'border: none; ', 'value' => Yii::$app->urlManager->createUrl("/shipper/create?id=" . $model->id)]) ?>
            </p>
            <?php
            Pjax::begin([
                'id' => 'user_shipper',
            ])
            ?>

            <?=
            GridView::widget([
                'dataProvider' => $shippers_DB,
                'columns' => [
                    'name',
                    'first_name',
                    'last_name',
                    [
                        'attribute' => 'id_number_type',
                        'value' => function($model) {
                            $value = "T - FOREIGN ENTITY";
                            switch ($model->id_number_type) {
                                case 'D':
                                    $value = "D - DUNS";
                                    break;
                                case 'E':
                                    $value = "E - EIN";
                                    break;
                                case 'T':
                                    $value = "T - FOREIGN ENTITY";
                                    break;

                                default:
                                    break;
                            }
                            return $value;
                        },
                        'format' => 'raw',
                    ],
                    'tax_id_passport_num',
                    'address',
                    'city',
                    'state',
                    'zip_postal_code',
                    'phone',
                    'fax_num',
                    'email',
                    [
                        'attribute' => 'image',
                        'value' => function($model) {
                            return Html::img($model->imageUrl, ['class' => 'img-lg', 'style' => 'max-height: 200px;']);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'label' => 'Consignees',
                        'value' => function($model) {
                            $consignees = [];
                            foreach ($model->shipperConsignees as $one) {
                                $consignees[] = $one->consignee->name;
                            }
                            return implode(' || ', $consignees);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'label' => 'Actions',
                        'value' => function($model) {
                            $btns = Html::button('Assign Consignees', ['class' => 'activity-view-link btn btn-sm btn-primary', 'style' => 'border: none; ', 'value' => Yii::$app->urlManager->createUrl("/shipper/assign-consignees?id=" . $model->id)])
                                . ' ' . Html::button('Edit', ['class' => 'activity-view-link btn btn-sm btn-warning', 'style' => 'border: none; ', 'value' => Yii::$app->urlManager->createUrl("/shipper/update?id=" . $model->id)])
                                . ' ' . Html::a('Delete', ['/shipper/delete', 'id' => $model->id], [
                                    'class' => 'btn btn-sm btn-danger',
                                    'style' => 'border: none; ',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                ]);

                            return $btns;
                        },
                        'format' => 'raw',
                    ],
                ],
            ]);
            ?>

            <?php Pjax::end() ?>

        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <h2>Consignees</h2>
            <p>
                <?= Html::button('Add Consignee', ['class' => 'activity-view-link btn btn-primary', 'style' => 'border: none;', 'value' => Yii::$app->urlManager->createUrl("/consignee/create?id=" . $model->id)]) ?>
            </p>
            <?php
            Pjax::begin([
                'id' => 'user_consignees',
            ])
            ?>

            <?=
            GridView::widget([
                'dataProvider' => $consignee_DB,
                'columns' => [
                    [
                        'label' => 'Shipper',
                        'value' => function($model) {
                            $shippers = [];
                            foreach ($model->shipperConsignees as $one) {
                                $shippers[] = $one->shipper->name;
                            }
                            return implode(' || ', $shippers);
                        },
                        'format' => 'raw',
                    ],
                    'name',
                    'first_name',
                    'last_name',
                    'address',
                    'city',
                    'state',
                    'country',
                    'zip_postal_code',
                    'phone',
                    'fax_num',
                    'email',
                    [
                        'attribute' => 'image',
                        'value' => function($model) {
                            return Html::img($model->imageUrl, ['class' => 'img-lg', 'style' => 'max-height: 200px;']);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'label' => 'Actions',
                        'value' => function($model) {
                            $btns = Html::button('Edit', ['class' => 'activity-view-link btn btn-sm btn-warning', 'style' => 'border: none; ', 'value' => Yii::$app->urlManager->createUrl("/consignee/update?id=" . $model->id)])
                                . ' ' . Html::a('Delete', ['/consignee/delete', 'id' => $model->id], [
                                    'class' => 'btn btn-sm btn-danger',
                                    'style' => 'border: none; ',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                ]);

                            return $btns;
                        },
                        'format' => 'raw',
                    ],
                ],
            ]);
            ?>

            <?php Pjax::end() ?>

        </div>
    </div>

</div>



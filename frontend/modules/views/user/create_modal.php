<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\User;

/* @var $this yii\web\View */

$this->title = 'Add New Customer';
?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span></button>
    <h4 class="modal-title"><?= $this->title ?></h4>
</div>
<?php $form = ActiveForm::begin(); ?>
<div class="modal-body">
    <?=
    $form->field($model->loadDefaultValues(), 'status')
        ->dropDownList(User::getStatusList())
    ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => 255])
        ->label($model->getAttributeLabel('email').'<span class="required_field">*</span>') ?>
    <?= $form->field($model, 'email_confirmed')->checkbox() ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => 255, 'autocomplete' => 'off'])
        ->label($model->getAttributeLabel('username').'<span class="required_field">*</span>') ?>

    <?= $form->field($model, 'fname')->textInput(['maxlength' => 50, 'autocomplete' => 'off', 'autofocus' => true]) ?>

    <?= $form->field($model, 'lname')->textInput(['maxlength' => 50, 'autocomplete' => 'off', 'autofocus' => true]) ?>

    <?php if ($model->isNewRecord): ?>

        <?= $form->field($model, 'password')->passwordInput(['maxlength' => 255, 'autocomplete' => 'off'])
            ->label($model->getAttributeLabel('password').'<span class="required_field">*</span>') ?>

        <?= $form->field($model, 'repeat_password')->passwordInput(['maxlength' => 255, 'autocomplete' => 'off'])
            ->label($model->getAttributeLabel('repeat_password').'<span class="required_field">*</span>') ?>
    <?php endif; ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => 50, 'autocomplete' => 'off', 'autofocus' => true]) ?>



    <?= $form->field($model, 'second_email')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'third_email')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'fourth_email')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'address')->textInput([]) ?>
    <?= $form->field($model, 'city')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'state')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'zip_postal_code')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'fax_num')->textInput(['maxlength' => 255]) ?>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-warning" data-dismiss="modal"><?= \Yii::t('all', 'Close') ?></button>
    <?= Html::submitButton(\Yii::t('all', 'Save'), [
        'class' => 'btn btn-primary ajax_save_reset_select2',
        'data-select_reset_url' => Yii::$app->urlManager->createUrl("/user/user/get-customers"),
        'data-select_reset_field_class' => 'customer',
        'data-dismiss' => 'modal',
    ]) ?>
</div>
<?php ActiveForm::end(); ?>



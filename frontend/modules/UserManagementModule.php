<?php

namespace frontend\modules;

use Yii;
use yii\helpers\ArrayHelper;
use webvimark\modules\UserManagement\UserManagementModule as BaseUserManagementModule;

class UserManagementModule extends BaseUserManagementModule
{
    public $controllerNamespace = 'frontend\controllers\user';
}

$(function () {
    $(document).on('change', '.memo_items', function (e) {
        e.preventDefault();
        var chosen_memo = $(this).val();
        show_input_field(chosen_memo);
    });
});

function show_input_field(class_name) {
    $('.memo_input').each(function () {
        if (!$(this).hasClass('hidden')) {
            $(this).addClass('hidden');
        }
    });
    //reset values
    $(".memo_input_field").val('').trigger('change')
    $('.' + class_name).removeClass('hidden');
}
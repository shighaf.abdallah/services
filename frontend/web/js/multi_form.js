$(function() {
    var displayFormDuration = 500;



    $(document).on('change', 'input[type=radio][name="Inventory[type]"]', function(e) {
        e.preventDefault();

        $('#vehicle_content').empty();
        $.get(window.url + '/en/inventory/add-form-details', { type: this.value })
            .done(function(data) {
                $('#vehicle_content').append(data);
            });
    });

    $(document).on('change', 'input[type=radio][name="Inventory[vehicles][1][is_personal_items]"]', function(e) {
        e.preventDefault();
        if (this.value == 1) {
            if ($('.is_personal_items_images').hasClass('hidden')) {
                $('.is_personal_items_images').removeClass('hidden')
            }
        } else {
            if (!$('.is_personal_items_images').hasClass('hidden')) {
                $('.is_personal_items_images').addClass('hidden')
            }
        }

    });

    $(document).on('click', '#submit_form', function(e) {
        e.preventDefault();
        var validate = form_validate();

        if (validate) {
            $('.multi_images_input').each(function(e) {
                $('.multi_images_input').remove();
            });

            $.ajax({
                type: "POST",
                url: window.url + '/' + window.lang + '/inventory/create-multi',
                data: $('form').serialize(),
                dataType: "json",
                success: function(data) {
                    notif({
                        msg: data.msg,
                        type: data.type
                    });
                    if (data.type == 'success') {
                        window.location.replace(data.link);
                    }
                },
                error: function(e) {
                    // debugger
                }
            });
        }
    });

    $(document).on('change', 'input', function(e) {
        var field_div = $(this).closest('.has-error');
        $(field_div).removeClass('has-error');

        var error_div = $(this).closest('.form-group').find('.help-block');
        $(error_div).empty();
    });

    $(document).on('change', 'textarea', function(e) {
        var field_div = $(this).closest('.has-error');
        $(field_div).removeClass('has-error');

        var error_div = $(this).closest('.form-group').find('.help-block');
        $(error_div).empty();
    });

    $(document).on('change', '.inventory_make', function(e) {
        var field_class = 'inventory_model';
        $('.' + field_class).val(null).trigger('change');
        $('.' + field_class).empty().trigger("change");
        $.get(window.url + '/en/car-model/get-models?make=' + $(this).val())
            .done(function(data) {
                var data = $.parseJSON(data);
                var lastItem = 1;
                data.forEach(function(item) {
                    lastItem = item.id;
                    var newOption = new Option(item.text, item.id, false, false);
                    $('.' + field_class).append(newOption);
                });
                $('.' + field_class).val(lastItem).trigger('change');
                $('.' + field_class).trigger('change');
            });
    });

    function form_validate() {
        var res = false;
        $('.has-error').each(function() {
            $(this).removeClass('has-error');
        });
        $('.help-block').each(function() {
            $(this).empty();
        });

        //customer
        var customer = check_error_input('Customer', $('#inventory-customer'));
        if (!customer) {
            return customer;
        }

        //loading_facility
        var loading_facility = check_error_input('Loading Facility', $('#inventory-loading_facility_id'));
        if (!loading_facility) {
            return loading_facility;
        }

        //port_of_loading
        var port_of_loading = check_error_input('Port of Loading', $('#inventory-port_of_loading_id'));
        if (!port_of_loading) {
            return port_of_loading;
        }

        $('.one_vehicle').each(function(from) {
            //type
            var type = $('input[type=radio][name="Inventory[type]"]:checked').val();
            if (type == 2) {
                //description
                res = check_error_text($(this).attr('data_number'), 'description', 'Description', 'textarea');
                if (!res) {
                    return res;
                }
            } else {
                //vin
                res = check_error_text($(this).attr('data_number'), 'vin', 'VIN');
                if (!res) {
                    return res;
                }

                //title
                res = check_error_radio($(this).attr('data_number'), 'title', 'Title');
                if (!res) {
                    return res;
                }
            }
        });
        return res;
    }





    function check_error_input(big_name, v_field) {
        var res = true;
        // var v_field = $(input);
        var v_field_value = v_field.val();
        if (!v_field_value) {
            var top = parseInt(v_field.offset().top) + 33;
            var error = big_name + ' cannot be blank.';
            $(v_field).parent().find('.help-block').append(error);
            $(v_field).parent().find('.help-block').addClass('has-error');
            $(v_field).parent().parent().parent().addClass('has-error');
            $('html, body').animate({
                scrollTop: top - 100
            }, 1000);
            res = false;
        }
        return res;
    }

    function check_error_text(id, name, big_name, input = 'input') {
        var res = true;
        var v_field = $(input + '[name=' + '"Inventory[vehicles][' + id + '][' + name + ']"]');
        var v_field_value = v_field.val();
        if (!v_field_value) {
            var top = parseInt(v_field.offset().top) + 33;
            var error = big_name + ' cannot be blank.';
            $('#' + name + '_error_' + id).append(error);
            $('.field-inventory-' + name + '_' + id).addClass('has-error')
            $('html, body').animate({
                scrollTop: top - 100
            }, 1000);
            res = false;
        }
        return res;
    }


    function check_error_radio(id, name, big_name) {
        var res = true;
        var v_field = $('input[name=' + '"Inventory[vehicles][' + id + '][' + name + ']"]');
        var checked = false;
        var top = 0;
        v_field.each(function() {
            if ($(this).is(':checked')) {
                checked = true;
            }
            top = parseInt($(this).offset().top) + 33;
        });
        if (!checked) {
            var error = big_name + ' cannot be blank.';
            $('#' + name + '_error_' + id).append(error);
            $('.field-inventory-' + name + '_' + id).addClass('has-error')
            $('html, body').animate({
                scrollTop: top - 100
            }, 1000);
            res = false;
        }
        return res;

    }


});

$(document).ready(function() {
    editCheckValue(1);
});

function check_if_exists(id, name, big_name) {
    var res = true;
    var v_field = $('input[name=' + '"Inventory[vehicles][' + id + '][' + name + ']"]');
    var v_field_value = v_field.val();
    if (v_field_value) {
        $.get(window.url + '/en/inventory/check-vin', { vin: v_field_value })
            .done(function(data) {
                var data = $.parseJSON(data);
                if (data.result.vinUsed) {
                    var top = parseInt(v_field.offset().top) + 33;
                    var error = big_name + ' already exists.';
                    $('#' + name + '_error_' + id).append(error);
                    $('.field-inventory-' + name + '_' + id).addClass('has-error')
                    $('html, body').animate({
                        scrollTop: top - 100
                    }, 1000);
                    res = false;
                } else if (data.result) {
                    if (data.vin_with_no_car_exists) {
                        alert('Found an inventory with same vin but no vehicle');
                    }
                    // debugger;

                    // $('input[name=' + '"Inventory[vehicles][' + id + '][make]"]').val(data.result.make);
                    // $('input[name=' + '"Inventory[vehicles][' + id + '][model]"]').val(data.result.model);
                    // resetSelect(window.url + "/car-make/get-makes", "inventory_make");
                    if (data.result.make != "") {
                        if (data.result.is_new_make) {
                            var newOption = new Option(data.result.make, data.result.make, true, true);
                            $('.inventory_make').append(newOption).trigger('change');
                        } else {
                            $('.inventory_make').val(data.result.make);
                            $('.inventory_make').trigger('change');
                        }

                        // var newOption = new Option(data.result.model, data.result.model, true, true);
                        // $('.inventory_model').append(newOption).trigger('change');
                        $('.inventory_model').val(data.result.model);

                        $('input[name=' + '"Inventory[vehicles][' + id + '][year]"]').val(data.result.year);
                        $('input[name=' + '"Inventory[vehicles][' + id + '][color]"]').val(data.result.color);
                        $('input[name=' + '"Inventory[vehicles][' + id + '][weight]"]').val(data.result.weight);
                        $('input[name=' + '"Inventory[vehicles][' + id + '][price]"]').val(data.result.value);
                    }
                }
            });
    }
    return res;
}

function title_edit(id) {
    var title_box = $('.title_box_' + id);
    var title_details_box = $('.title_box_details_' + id);
    var title_val = $('input[name="Inventory[vehicles][' + id + '][title]"]:checked').val();
    if (title_val == 2) {
        if (title_box.hasClass('hidden')) {
            title_box.removeClass('hidden');
        }
        if (!title_details_box.hasClass('hidden')) {
            title_details_box.addClass('hidden');
        }
    } else if (title_val == 1) {
        if (title_box.hasClass('hidden')) {
            title_box.removeClass('hidden');
        }
        if (title_details_box.hasClass('hidden')) {
            title_details_box.removeClass('hidden');
        }
    } else {
        if (!title_box.hasClass('hidden')) {
            title_box.addClass('hidden');
        }
        if (!title_details_box.hasClass('hidden')) {
            title_details_box.addClass('hidden');
        }
    }
}

function delivery_edit(id) {
    var box = $('.delivery_box_' + id);
    var val = $('input[name="Inventory[vehicles][' + id + '][delivery]"]:checked').val();
    if (val == 2) {
        if (box.hasClass('hidden')) {
            box.removeClass('hidden');
        }
    } else {
        if (!box.hasClass('hidden')) {
            box.addClass('hidden');
        }
    }
}

function editCheckValue(id) {
    var value = 0;
    value = getFloatValue($('#inventory-towing_charge' + id).val()) +
        getFloatValue($('#inventory-storage_charge' + id).val()) +
        getFloatValue($('#inventory-title_charge' + id).val()) +
        getFloatValue($('#inventory-another_payment_charge' + id).val());
    $('#inventory-check_value_' + id).val(value);
}

function getFloatValue(att) {
    var value = 0;
    if (att) {
        value = parseFloat(att);
    }
    return value;
}

function assignReceiverName(id) {
    var data = $('#inventory_fees_check_company_name_' + id).select2('data')
        //        var value = $('#Inventory_vehicle_fees_check_company_name_' + id).find(":selected").text();
    $('#Inventory_vehicle_fees_check_receiver_name_' + id).val(data[0].text);
}

function addVehicleCheck(id) {
    var check_box = $('.check_box_' + id);
    var prevNum = id - 1;
    var checkNum = $('#inventory-check_num_' + prevNum).val();

    if ($('input[name=' + '"Inventory[vehicles][' + id + '][add_check]"]').is(":checked")) {
        $.get(window.url + '/en/inventory/add-check', { id: id, check_num: checkNum })
            .done(function(data) {
                check_box.html(data).show(500);
                editCheckValue(id);
            });
    } else {
        check_box.html('').show(500);
    }
}

function addVehicleCheckDispatch(id) {
    var dispatch_box = $('.dispatch_content_' + id);
    var prevNum = id - 1;
    var checkNum = $('#inventory-check_num_' + prevNum).val();

    if ($('input[name=' + '"Inventory[vehicles][' + id + '][add_check_dispatch]"]').is(":checked")) {
        $.get(window.url + '/en/inventory/add-check-dispatch', { id: id, check_num: checkNum })
            .done(function(data) {
                dispatch_box.html(data).show(500);
                editCheckValue(id);
            });
    } else {
        dispatch_box.html('').show(500);
    }
}



function getInventoryInfo(id) {
    var vin_value = $('input[name=' + '"Inventory[vehicles][' + id + '][vin]"]').val();
    $.get(window.url + '/en/inventory/get-data-by-vin', { vin: vin_value })
        .done(function(data) {
            var data = $.parseJSON(data);
            $('input[name=' + '"Inventory[vehicles][' + id + '][make]"]').val(data.make);
            $('input[name=' + '"Inventory[vehicles][' + id + '][model]"]').val(data.model);
            $('input[name=' + '"Inventory[vehicles][' + id + '][year]"]').val(data.year);
            $('input[name=' + '"Inventory[vehicles][' + id + '][weight]"]').val(data.weight);
            $('input[name=' + '"Inventory[vehicles][' + id + '][price]"]').val(data.value);

            if (data.titleData != '') {
                //                    alert('Title Found.');
                var title_box = $('.title_box_' + id);
                $('input[name=' + '"Inventory[vehicles][' + id + '][title]"]').val(data.titleData.title);
                $("#title_radio" + data.titleData.title).attr('checked', 'checked');
                if (data.titleData.title != 0) {
                    if (title_box.hasClass('hidden')) {
                        title_box.removeClass('hidden');
                    }
                } else {
                    $('#title_receive_date_' + id).val('');
                    if (!title_box.hasClass('hidden')) {
                        title_box.addClass('hidden');
                    }
                }
                $('input[name=' + '"Inventory[vehicles][' + id + '][title_number]"]').val(data.titleData.title_num);
                $('input[name=' + '"Inventory[vehicles][' + id + '][title_state]"]').val(data.titleData.title_state);
                $('input[name=' + '"Inventory[vehicles][' + id + '][title_receive_date]"]').val(data.titleData.title_receive_date);
            }
        });
}

function car_included(id) {
    var isChecked = $('input[name=' + '"Inventory[vehicles][' + id + '][car_included]"]').is(":checked");
    if (isChecked) {
        if (!$('.add_check_box_' + id).hasClass('hidden')) {
            $('.add_check_box_' + id).addClass('hidden');
        }
    } else {
        if ($('.add_check_box_' + id).hasClass('hidden')) {
            $('.add_check_box_' + id).removeClass('hidden');
        }
    }

    $("#title_radio1").attr('checked', 'checked');
    var title_box = $('.title_box_' + id);
    if (title_box.hasClass('hidden')) {
        title_box.removeClass('hidden');
    }
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();

    if (dd < 10) {
        dd = '0' + dd
    }

    if (mm < 10) {
        mm = '0' + mm
    }

    //        today = yyyy + '-' + mm + '-' + dd;
    today = mm + '/' + dd + '/' + yyyy;
    $('#title_receive_date_' + id).val(today);
    if (title_box.hasClass('hidden')) {
        title_box.removeClass('hidden');
    }

}

function getMakeModels() {
    // debugger
    $('#inventory-model').empty();
    // var selectedMake = document.getElementById('inventory-make');
    // var selectVal = selectedMake.options[selectedMake.selectedIndex].value;
    var selectedMakeId = $("#inventory-make option:selected").attr('name');

    // debugger

    $.get(window.url + '/en/inventory/get-make-models?makeId=' + selectedMakeId)
        .done(function(response) {
            debugger
            var models = "";
            for (var i = 0; i < response.data.length; i++) {
                models = models + "<option  name='" + response.data[i].name + "' value='" +
                    response.data[i].name + "'>" + response.data[i].name + "</option>";
            }
            $("#inventory-model").append(models);
        });
}
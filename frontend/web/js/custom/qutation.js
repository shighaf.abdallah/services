$(document).on('click', '.other-fees-plus',function(e){
    e.preventDefault;
    var id = $('.other-fees').last().data('id');

    if (typeof id === "undefined") {
        id = 0;
    }
    id = parseInt(id)+1;
    $.get(window.url + '/en/qutation/get-services?id='+id)
        .done(function(data) {
            $("#quatation_other_fees_tabel_body").append(data);
        });
});

$(document).on('click', '.other-fees-minus', function(){
    var id = $(this).data('id');
    $('#other_fees_'+id).remove();
});


$(document).on('click', '.generate-buy-rate',function(e){

    e.preventDefault();
    portOfLoading = $('#qutation_port_of_loading').val();
    portOfDestination = $('#qutation_port_of_destination').val();
    ssLine = $('#qutation_ss_line').val();
    commodity = $('#qutation_commodity').val();
    equipt = $('#qutation_equipt').val();
    //var selected_filters = [portOfLoading, portOfDestination, ssLine, commodity, equipt];
    var selected_filters = 'pol='+portOfLoading+'&pod='+portOfDestination+'&ssl='+ssLine+'&com='+commodity+'&equ='+equipt;
    //alert("value = "+selected_filters);
    $.get(window.url + '/en/qutation/generate-buy-rate?'+selected_filters)
            .done(function(data){
                $('.buy-rate-view').html(data);
            });
});

$(document).on('change','.radio_buyrate_qutation',function(){

        var id = $(this).data('id');
        var buy_rate_total = $('#radio_buyrate_qutation_'+id).val();
        //alert(buy_rate_total);
        $('#ocean_freight_cost').val(buy_rate_total);
        var value = getFloatValue($('#ocean_freight_cost').val());
        $('.service_cost').each(function(){
            value = value  + getFloatValue($(this).val());
        });
        $('#qutation_total').val(value);
});

$(document).on('change','.service_cost',function(){
    var value = getFloatValue($('#ocean_freight_cost').val());
    $('.service_cost').each(function(){
        value = value  + getFloatValue($(this).val());
    });
    $('#qutation_total').val(value);
});

$(document).on('change','#ocean_freight_charge',function(){

    var value = getFloatValue($('#ocean_freight_charge').val());
    $('.service_charge').each(function(){
        value = value  + getFloatValue($(this).val());
    });
    $('#qutation_total_charge').val(value);
});

$(document).on('change','.service_charge',function(){
    var value = getFloatValue($('#ocean_freight_charge').val());
    $('.service_charge').each(function(){
        value = value  + getFloatValue($(this).val());
    });
    $('#qutation_total_charge').val(value);
});


function getFloatValue(att) {
    var value = 0;
    if (att) {
        value = parseFloat(att);
    }
    return value;
}

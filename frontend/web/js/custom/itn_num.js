

// $(document).ready(function () {
$(document).on('click', '.get_itn_btn', function (e) {
    e.preventDefault();
    var container_num = $('#container_number').val();
    var itn_number = $('#itn_number').val();
    if(!itn_number){
        getItnNumber(container_num);
    }
});


function getItnNumber(container_num) {
    $.get(window.url + '/en/site/get-itn-number', {id: container_num})
        .done(function (data) {
            data = $.parseJSON(data);
            $.each(data.urls, function( index, value ) {
                // var win = window.open(
                //     value,
                //     "Get AES ITN#");
                // var timer = setInterval(function() {
                //     if (win.closed) {
                //         clearInterval(timer);
                //         alert("'Get AES ITN#' window closed !");
                //     }
                // }, 500);

                var win = window.open(value, '_blank');
                if (win) {
                    //Browser has allowed it to be opened
                    win.focus();
                } else {
                    //Browser has blocked it
                    alert('Please allow popups for this website');
                }
                // $.ajax({
                //     url: value,
                //     headers: {
                //         'Content-Type': 'application/x-www-form-urlencoded'
                //     },
                //     type: "POST", /* or type:"GET" or type:"PUT" */
                //     dataType: "json",
                //     data: {
                //     },
                //     success: function (result) {
                //         console.log(result);
                //     },
                //     error: function () {
                //         console.log("error");
                //     }
                // });
            });

        });
}
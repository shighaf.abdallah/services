$(document).on('change', '.invoice_input', function (e) {
    e.preventDefault();
    var data_id = $(this).attr("data-id");
    var total = 0;
    $(".invoice_input").each(function () {
        if ($(this).attr("data-id") == data_id) {
            if (parseFloat($(this).val())) {
                total = total + parseFloat($(this).val());
            }
        }
    });
    $("#sub_total1_" + data_id).html(total);
});



$(document).on('change', '.buy_rate_place', function(e) {
        var field_class = 'buy_rate_port_of_destination';
        $('.' + field_class).val(null).trigger('change');
        $('.' + field_class).empty().trigger("change");
        $.get(window.url + '/en/port-of-destination/get-ports?place=' + $(this).val())
            .done(function(data) {
                var data = $.parseJSON(data);
                var lastItem = 1;
                data.forEach(function(item) {
                    lastItem = item.id;
                    var newOption = new Option(item.text, item.id, false, false);
                    $('.' + field_class).append(newOption);
                });
                $('.' + field_class).val(lastItem).trigger('change');
                $('.' + field_class).trigger('change');
            });
    });
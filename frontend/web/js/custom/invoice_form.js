function stopBtn(e) {
    e.preventDefault();
}
function submitForm(sendEmail) {
    if(!$('.submit_form').attr('disabled')){
        $('.submit_form').attr('disabled', true);
        var sendEmail = $("<input>")
            .attr("type", "hidden")
            .attr("name", "sendEmail").val(sendEmail);
        $('#invoice_form').append($(sendEmail));
        $('#invoice_form').append($(".send_to_emails"));
        $('#invoice_form').submit();
        // alert(1);
    } else {
        // alert(0);
    }

}

function hidePrice(inv_key, key) {
    var select_value = $('input[name="Invoice[' + inv_key + '][inventory]['+key+'][calculate_way]"]:checked').val();
    var price_box_name = '#invoice_price_' + inv_key + '_' + key;
    var price_field_name = 'input[name="Invoice[' + key + '][inventory]['+key+'][price]"]';
    var price_value = $(price_field_name).val();
    var inland_fee_box_name = '#invoice_inland_fees_' + inv_key+ '_' + key;
    var inland_fee_field_name = '#inland_fees_' + inv_key+ '_' + key;
    var inland_fee_value = $(inland_fee_field_name).val();
    if (select_value == 1) {
        $(price_field_name).val(price_value ? price_value : 0);
        $(price_box_name).hide();
        $(inland_fee_field_name).val(inland_fee_value ? inland_fee_value : '');
        $(inland_fee_box_name).hide();
    } else if (select_value == 2) {
        $(price_field_name).val(price_value ? price_value : 0);
        $(price_box_name).show();
        $(inland_fee_field_name).val(inland_fee_value ? inland_fee_value : '');
        $(inland_fee_box_name).hide();
    } else {
        $(price_field_name).val(price_value ? price_value : 0);
        $(price_box_name).hide();
        $(inland_fee_field_name).val(inland_fee_value ? inland_fee_value : '');
        $(inland_fee_box_name).show();
    }

    //recalculate the total
    var total_value = calculateTotal(inv_key, key);
    $('#invoice_' + inv_key+ '_' + key).html(total_value);
}

$(document).ready(function () {
    $('.inventory_invoice').each(function () {
        var inv_key = $(this).attr('data-inv_id');
        var key = $(this).attr('data-id');
        var total_value = calculateTotal(inv_key, key);
        $('#invoice_' + inv_key + '_' + key).html(total_value);

        //hide
        hidePrice(inv_key, key);

        //sub-total1
        calculatSubTotal1(inv_key, key);
    })
});

function calculatSubTotal1(inv_key, key) {
    var val1 = parseFloat($('input[name="Invoice[' + inv_key + '][inventory][' + key + '][storage_price]"]').val());
    var val2 = parseFloat($('input[name="Invoice[' + inv_key + '][inventory][' + key + '][towing_price]"]').val());
    var val3 = parseFloat($('input[name="Invoice[' + inv_key + '][inventory][' + key + '][title_price]"]').val());
    var val4 = parseFloat($('input[name="Invoice[' + inv_key + '][inventory][' + key + '][another_payment]"]').val());
    $('#sub_total1_' +inv_key+'_'+ key).html(val1 + val2 + val3 + val4);
}

$(document).on('change', '.invoice_input', function (e) {
    var inv_key_key = $(this).attr('data-id');
    var inv_key = inv_key_key.split('_')[0];
    var key = inv_key_key.split('_')[1];
    var total_value = calculateTotal(inv_key, key);
    $('#invoice_' + inv_key+'_'+ key).html(total_value);
});

function calculateTotal(inv_key, key) {
    var total_value = 0;
    

    var inland_fees = $('#inland_fees_' + inv_key+'_'+ key).val();
    if (!inland_fees) {
        inland_fees = 0;
    }

    var fees_prices = parseFloat(inland_fees) +
            parseFloat($('input[name="Invoice[' + inv_key + '][inventory][' + key + '][loading_fees]"]').val()) +
            parseFloat($('input[name="Invoice[' + inv_key + '][inventory][' + key + '][shipping_fees]"]').val()) +
            parseFloat($('input[name="Invoice[' + inv_key + '][inventory][' + key + '][other_fees]"]').val());

    var added_fee = parseFloat($('input[name="Invoice[' + inv_key + '][inventory][' + key + '][price]"]').val());

    var select_type_value = $('input[name="Invoice[' + inv_key + '][inventory][' + key + '][calculate_way]"]:checked').val();

    var towingPrice = 0;
    if (select_type_value == 2) {
        towingPrice = added_fee;
    } else {
        towingPrice = parseFloat($('input[name="Invoice[' + inv_key + '][inventory][' + key + '][towing_price]"]').val());
    }
    
    var inventory_prices = parseFloat($('input[name="Invoice[' + inv_key + '][inventory][' + key + '][storage_price]"]').val()) +
            towingPrice +
            parseFloat($('input[name="Invoice[' + inv_key + '][inventory][' + key + '][title_price]"]').val()) +
            parseFloat($('input[name="Invoice[' + inv_key + '][inventory][' + key + '][another_payment]"]').val());
    
    total_value = inventory_prices + fees_prices;

    return total_value;
}
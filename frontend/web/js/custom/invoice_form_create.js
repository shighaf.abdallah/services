$(function () {
    $(document).on('change', '.invoice_items', function (e) {
        e.preventDefault();
        var chosen_invoice = $(this).val();
        show_input_field(chosen_invoice);
    });
});

function show_input_field(class_name) {
    $('.invoice_input').each(function () {
        if (!$(this).hasClass('hidden')) {
            $(this).addClass('hidden');
        }
    });
    //reset values
    $(".invoice_input_field").val('').trigger('change')
    $('.' + class_name).removeClass('hidden');
}
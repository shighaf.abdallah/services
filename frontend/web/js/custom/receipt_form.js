$(document).on('change', '.user_input', function (e) {
    var user_id = $(this).val();
    getUserCreateReceiptData(user_id);
});

function getUserCreateReceiptData(user_id) {
    $('#tbody_user_invoices').html('');
    $('.user_inventory_summery').html('');
    $('#customer_balance').html('');
    $.get(window.url + '/en/receipt/get-receipt-create-data', {id: user_id})
        .done(function (data) {
            var data = $.parseJSON(data);
            $('#receipt_title').html(' (' + data.invoices_count + ' Invoice)');
            $('#tbody_user_invoices').append(data.invoices_view);
            $('#customer_balance').append(data.user_balance);
            $('.user_inventory_summery').append(data.inventory_summery);
        });
}

$(document).on('click', '#receipt_form_submit', function (e) {
    e.preventDefault();
    var validate = receipt_form_validate(true, true);

    if (validate) {
        $('#receipt_form').submit();
    }
});

$(document).on('click', '#complete_receipt_form_submit', function (e) {
    e.preventDefault();
    var validate = receipt_form_validate(false, false);

    if (validate) {
        $('#receipt_form').submit();
    }
});

$(document).on('click', '.invoiceTotal', function (e) {
    var totalChosenInvoicesValue = 0;
    var totalCount = 0;
    $('.invoiceTotal:checkbox:checked').each(function () {
        totalCount = totalCount + 1;
        totalChosenInvoicesValue = totalChosenInvoicesValue + parseFloat($('#invoiceTotal_' + $(this).val()).val());
    });
    if (totalCount) {
        notif({
            msg: totalCount + " Record, Amount: " + totalChosenInvoicesValue,
            type: 'info'
        });
    }
});



function receipt_form_validate(allow_negative, include_user_balance) {
    var res = false;
    var receiptValue = parseFloat($('#userreceipt-value').val());
    var userBalance = 0;
    if (include_user_balance) {
        var userBalance = parseFloat($('.user_balance').html());
    }

    var totalChosenInvoicesValue = 0;
    $('.invoiceTotal:checkbox:checked').each(function () {
        res = true;
        totalChosenInvoicesValue = totalChosenInvoicesValue + parseFloat($('#invoiceTotal_' + $(this).val()).val());
    });
    if ((receiptValue + userBalance) < totalChosenInvoicesValue) {
        if (!allow_negative) {
            res = false;
            notif({
                msg: 'Total value is less than value + Customer balance.',
                type: 'error'
            });
        }
    }
    if (totalChosenInvoicesValue == 0) {
        notif({
            msg: "You didn't choose any record!",
            type: 'error'
        });
    }

    //check invoices payed value if larger than value
    var totalPayedValue = 0;
    $('.invoice_payed_value').each(function () {
        totalPayedValue = totalPayedValue + parseFloat($(this).val());
    });
    console.log(totalPayedValue);
    console.log(receiptValue + userBalance);
    if(totalPayedValue > (receiptValue + userBalance)){
        res = false;
        notif({
            msg: 'Total value is less than the Sum of invoices payed value.',
            type: 'error'
        });
    }
    //
    return res;
}

function selectAll() {
    if ($('#select_all').is(':checked')) {
        $('.new_invoice').each(function () {
            $(this).prop('checked', true);
        });
    } else {
        $('.new_invoice').each(function () {
            $(this).prop('checked', false);
        });
    }
    var totalChosenInvoicesValue = 0;
    var totalCount = 0;
    $('.invoiceTotal:checkbox:checked').each(function () {
        totalCount = totalCount + 1;
        totalChosenInvoicesValue = totalChosenInvoicesValue + parseFloat($('#invoiceTotal_' + $(this).val()).val());
    });
    notif({
        msg: totalCount + " Record, Amount: " + totalChosenInvoicesValue,
        type: 'info'
    });

}

$(document).on('click', '.receipt_form_submit_options', function (e) {
    e.preventDefault();
    var validate = receipt_form_validate(true, true);

    if (validate) {
        $('#receipt_form').submit();
//         $('#myModal').find('#modalContent').empty();
//         var viewModal = $('#myModal').modal('show')
//             .find('#modalContent');
//         viewModal.html('<div class="modal-content">\n\
// <div class="modal-header">\n\
// <button type="button" class="close" data-dismiss="modal">&times;</button>\n\
// <h4 class="modal-title">\n\
// </h4></div>\n\
// <div class="modal-body">\n\
// <p>Loading...</p>\n\
// </div>\n\
// </div>');
//
//         viewModal.load($(this).attr('value'));
    }
});

$(document).on('change', '#userreceipt-value', function (e) {
    calculateInvoicesPayed();
});

$(document).on('change', "input[name='UserReceipt[customer_balance]']", function (e) {
    calculateInvoicesPayed();
});

function calculateInvoicesPayed() {
    var receipt_value = parseFloat($('#userreceipt-value').val());
    var use_user_balance = false;
    if ($("input[name='UserReceipt[customer_balance]']").is(':checked')){
        use_user_balance = true;
    }
    if(use_user_balance){
        receipt_value = receipt_value + parseFloat($('.user_balance').html());
    }
    var invoice_num = '';
    var invoice_value = '';
    $('.invoice_payed_value').each(function () {
        invoice_num = $(this).data('id');
        invoice_value = parseFloat($('#invoiceTotal_'+invoice_num).val());
        if(receipt_value){
            if(receipt_value >= invoice_value){
                receipt_value = receipt_value - invoice_value;
                $(this).val(invoice_value);
            } else {
                $(this).val(receipt_value);
                receipt_value = 0;
            }
        } else {
            $(this).val(receipt_value);
        }
    });
}
$(document).on('click', '.form_btn', function (e) {
    e.preventDefault();
    $(this).parent().attr('action', $(this).val());
    $(this).parent().submit();
});

$(document).ready(function () {
    $('#select_all').on('click', function () {
        if (this.checked) {
            $('.checkbox_c').each(function () {
                this.checked = true;
            });
        } else {
            $('.checkbox_c').each(function () {
                this.checked = false;
            });
        }
    });

    $('.checkbox_c').on('click', function () {
        if ($('.checkbox_c:checked').length == $('.checkbox_c').length) {
            $('#select_all').prop('checked', true);
        } else {
            $('#select_all').prop('checked', false);
        }
    });
});
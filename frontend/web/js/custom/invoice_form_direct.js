$(document).on('change', 'input[name="memo_item"]', function (e) {
    var value = $(this).val();
    $('#invoice_payments').html('');
    var inventory_value = $('.inventory_input').val();
    var container_value = $('.container_input').val();
    var user_value = $('.customer_input').val();
    switch (value) {
        case 'inventory':
            hide_field('#invoice_container');
            show_field('#invoice_inventory');
            hide_field('#invoice_other');
            if(inventory_value){
                invoice_payment_fields(inventory_value, '', '');
            }
            break;

        case 'container':
            show_field('#invoice_container');
            hide_field('#invoice_inventory');
            hide_field('#invoice_other');
            if(container_value){
                invoice_payment_fields('', container_value, '', user_value);
            }
            break;

        case 'other':
            hide_field('#invoice_container');
            hide_field('#invoice_inventory');
            show_field('#invoice_other');
            invoice_payment_fields('', '', 1);
            break;

        default:
            break;
    }
});

$(document).on('change', '.inventory_input', function (e) {
    var value = $(this).val();
    invoice_payment_fields(value, '', '');
});

$(document).on('change', '.container_input', function (e) {
    var value = $(this).val();
    var user_value = $('.customer_input').val();
    invoice_payment_fields('', value, '', user_value);
});

$(document).on('change', '.customer_input', function (e) {
    var value = $(this).val();
    $('.inventory_input').val(null).trigger('select2:change');
    $('.inventory_input').empty().trigger('select2:change');

    $('.container_input').val(null).trigger('select2:change');
    $('.container_input').empty().trigger('select2:change');

    $.get(window.url  + '/' + window.lang + '/site/get-user-inventories-containers', {id: value}, function(data){
        var data = $.parseJSON(data);
        data.inventories.forEach(function (item) {
            var newOption = new Option(item.text, item.id, false, false);
            $('.inventory_input').append(newOption);
        });
        data.containers.forEach(function (item) {
            var newOption = new Option(item.text, item.id, false, false);
            $('.container_input').append(newOption);
        });

        $('#bill_to_input').val(data.bill_to);
        $('.inventory_input').trigger('select2:change');
        $('.container_input').trigger('select2:change');
    });
});

function invoice_payment_fields(inventory_id='', container_id='', other='', user='') {
    $.get(window.url + '/'+window.lang+'/invoice/get-invoice-payments-fields', {inv_id: inventory_id, cid: container_id, o: other, uid: user})
        .done(function (data) {
            $('#invoice_payments').html(data);
        });
}

function hide_field(item) {
    if (!$(item).hasClass('hidden')) {
        $(item).addClass('hidden');
    }
}

function show_field(item) {
    if ($(item).hasClass('hidden')) {
        $(item).removeClass('hidden');
    }
}

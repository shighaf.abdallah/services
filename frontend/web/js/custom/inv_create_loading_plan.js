$(function () {
    $(document).on('click', '.create_loading_plan', function (e) {
        e.preventDefault();
        $('#form_loading_plan').attr('action', url + '/en/container-inventory/create-loading-plan').submit();
    });

    $(document).on('click', '.prepare_loading_plan', function (e) {
        e.preventDefault();
        $('#form_loading_plan').attr('action', url + '/en/container/prepare-loading-plan').submit();
    });
});
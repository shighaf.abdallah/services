$(function () {
    $(document).on('click', '.add_another_invoice', function (e) {
        e.preventDefault();
        var keyNum = $('.inventory_invoice').last().attr('data-id');
        $.get(window.url + '/en/invoice/add-direct-invoice', {id: keyNum})
                .done(function (data) {
                    $('.invoices_form').append(data).show(2000);
                    var new_div_top = parseInt($('.add_another_invoice').offset().top);
                    $('html, body').animate({
                        scrollTop: new_div_top - 100
                    }, 1000);

                });
    });

    $(document).on('change', '.invoice_inventory_field', function (e) {
        var inventory_id = $(this).val();
        var key = $(this).attr('key');
        $.get(window.url + '/en/invoice/get-bill-to-from-inventory', {id: inventory_id})
                .done(function (data) {
                    $('#billofloading-bill_to_'+key).val(data);
                });
    });

    $(document).on('change', '.invoice_container_field', function (e) {
        var container_id = $(this).val();
        var key = $(this).attr('key');
        $.get(window.url + '/en/invoice/get-bill-to-from-container', {id: container_id})
                .done(function (data) {
                    $('#billofloading-bill_to_'+key).val(data);
                });
    });
    
    $(document).on('change', '.invoice_user_field', function (e) {
        var user_id = $(this).val();
        var key = $(this).attr('key');
        $.get(window.url + '/en/invoice/get-bill-to-from-user', {id: user_id})
                .done(function (data) {
                    $('#billofloading-bill_to_'+key).val(data);
                });
    });

    $(document).on('change', '.memo_items', function (e) {
        e.preventDefault();
        var chosen_memo = $(this).val();
        var key = $(this).attr('key');
        show_input_field(chosen_memo, key);
    });
});

function show_input_field(class_name, key) {
    $('.memo_input_' + key).each(function () {
        if (!$(this).hasClass('hidden')) {
            $(this).addClass('hidden');
        }
    });
    //reset values
    $('.' + class_name).each(function () {
        $(this).removeClass('hidden');
    });
}
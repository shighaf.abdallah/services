$(document).on('click', '.submit_add_booking', function (e) {
    e.preventDefault();
    $('#confrim').val(1);
    $( "#add_booking_form" ).submit();
});

$(document).on('click', '.submit_confirm_add_booking', function (e) {
    e.preventDefault();
    $('#confrim').val(2);
    $( "#add_booking_form" ).first().submit();
});

$(document).on('click', '.submit_delete_booking', function (e) {
    e.preventDefault();
    $('#confrim').val(3);
    $( "#add_booking_form" ).first().submit();
});

$(document).on('click', '.submit_confirm_booking', function (e) {
    e.preventDefault();
    $('#confrim').val(4);
    $( "#add_booking_form" ).first().submit();
});

$(document).on('click', '.submit_unconfirm_booking', function (e) {
    e.preventDefault();
    $('#confrim').val(5);
    $( "#add_booking_form" ).first().submit();
});

$(document).on('click', '.submit_deactivate_booking', function (e) {
    e.preventDefault();
    $('#confrim').val(6);
    $( "#add_booking_form" ).first().submit();
});

$(document).on('click', '.submit_deactivate_add_booking', function (e) {
    e.preventDefault();
    $('#confrim').val(7);
    $( "#add_booking_form" ).first().submit();
});
$(document).on('change', '#start_date', function (e) {
    $.fn.datepicker.defaults.format = 'yyyy-mm-dd'
    var date2 = $(this).datepicker('getDate', '+30d');
    date2.setDate(date2.getDate()+30);

    $('#end_date').datepicker('setDate', date2);

});
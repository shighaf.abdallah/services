function submit_form(e) {
    e.preventDefault();
    var validate = form_validate();
    if (validate) {
        $('form').each(function () {
            $(this).submit();
        });
    }
}

function form_validate() {
    var booking_value = $('#container-booking_id').val();
    if(booking_value){
        return true;
    } else {
        notif({
            msg: 'Please select a booking',
            type: 'error'
        });
        return false;
    }
    if ($(".invs").length != 0) {
        return true;
    } else {
        notif({
            msg: 'You need to add inventories to loading plan',
            type: 'error'
        });
        return false;
    }
}



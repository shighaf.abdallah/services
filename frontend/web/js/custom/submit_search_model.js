$(document).on('click', '.submit_search_model', function (e) {
    e.preventDefault();
    var url = $(this).val();
    var id = $(this).data('id');
    $('form').attr('action', url);
    $('#company_id').val(id);
    $('form').submit();
});
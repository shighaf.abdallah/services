function submit_form(e) {
    e.preventDefault();
    var validate = form_validate();
    if (validate) {
        $('form').each(function () {
            $(this).submit();
        });
    }
}

function form_validate() {
    if ($(".invs").length != 0) {
        return true;
    } else {
        notif({
            msg: 'You need to add inventories to loading plan',
            type: 'error'
        });
        return false;
    }
}

function add_inv() {
    var inv_id = $('#container-inventories').select2('val');
    if (inv_id) {
        $.get(url + '/en/container/get-inventory', {id: inv_id}, function (data) {
            if (data != null)
            {
                var data = $.parseJSON(data);
                if ($("#inv_" + data.id).length == 0) {
                    $("#inv_table").append('<tr id="inv_' + data.id + '" class="invs" data-key="' + data.id + '">\n\
                                            <td>\n\
                                                <input style="display: none" name="inv[' + data.id + '][inv_id]" value="' + data.id + '">\n\
                                                ' + data.vin + '</td>\n\
                                            <td>' + data.make + '</td>\n\
                                            <td>' + data.model + '</td>\n\
                                            <td>' + data.year + '</td>\n\
                                            <td>' + data.color + '</td>\n\
                                            <td>' + data.title + '</td>\n\
                                            <td>' + data.customer + '</td>\n\
                                            <td>' + data.shippers + '</td>\n\
                                            <td>' + data.consignees + '</td>\n\
                                            <td style="text-align: center">' + '<a class="btn" style="padding: 0px 4px; background-color: #dbdbdb!important; border-color: #dbdbdb!important;" onclick="inv_remove(event, ' + data.id + ')"><i class="fa fa-times" style="color: red;"></i></a>' + '</td>\n\
                                        </tr>');
                    $('.init_select').select2();
                    notif({
                        msg: 'Added to loading plan.',
                        type: 'info'
                    });
                } else {
                    notif({
                        msg: 'Already exist in loading plan.',
                        type: 'warning'
                    });
                }
            }
        });
    } else {
        notif({
            msg: 'Please choose an inventory and a shipper first!',
            type: 'warning'
        });
    }
}

function inv_remove(e, inv_id) {
    e.preventDefault();
    $("#inv_" + inv_id).remove();
}

function shipper_data() {
    var shipper_id = $('#container-client_shipper').select2('val');
    var old_shippers = '';
    $('.inv_shipper_val').each(function(i, obj) {
        console.log($(this).val());
        old_shippers = old_shippers + ',' + $(this).val();
    });
    $.get(url + '/en/container-inventory/get-shipper-consignees', {id: shipper_id, sh: old_shippers}, function (data) {
        if (data != null)
        {
            var data = $.parseJSON(data);
            var $select = $('#container-consignee');
// save current config. options
            var options = $select.data('select2').options.options;
// delete all items of the native select element
            $select.html('');
// build new items
            var items = [];
            for (var i = 0; i < data.length; i++) {
                // logik to create new items
                items.push({
                    "id": data[i].id,
                    "text": data[i].name
                });
                $select.append("<option value=\"" + data[i].id + "\">" + data[i].name + "</option>");
            }

// add new items
            options.data = items;
            $select.select2(options);
        }
    });




}

function consignee_data() {
    var consignee_id = $('#container-consignee').select2('val');
    $.get(url + '/en/container-inventory/get-consignee-data', {id: consignee_id}, function (data) {
        if (data != null)
        {
            var data = $.parseJSON(data);
            $('#container-consignee_address').val(data.address);
        }
    });
}

$(document).on('change', '.port_of_loading', function (e) {
    // debugger
    var port_of_loading = $('#container-port_of_loading_id').val();
    if(port_of_loading) {
        $.get(url + '/en/container/get-customer-port-of-loading-inventories', {
            id: port_of_loading,
        }, function (data) {
            if (data != null) {
                var data = $.parseJSON(data);
                //get shippers
                var $select = $('#container-inventories');
                var options = $select.data('select2').options.options;
                $select.html('');
                var items = [];
                for (var i = 0; i < data.invs.length; i++) {
                    items.push({
                        "id": data.invs[i].id,
                        "text": data.invs[i].name
                    });
                    $select.append("<option value=\"" + data.invs[i].id + "\">" + data.invs[i].name + "</option>");
                }
                for (var i = 0; i < data.no_title_invs.length; i++) {
                    items.push({
                        "id": data.no_title_invs[i].id,
                        "text": data.no_title_invs[i].name
                    });
                    $select.append("<option disabled='disabled' value=\"" + data.no_title_invs[i].id + "\">" + data.no_title_invs[i].name + "</option>");
                }
                options.data = items;
                $select.select2(options);
            }
        });
    }
});


function inv_shippers() {
    var inv_id = $('#container-inventories').select2('val');
    var old_shippers = '';
    $('.inv_shipper_val').each(function(i, obj) {
        console.log($(this).val());
        old_shippers = old_shippers + ',' + $(this).val();
    });
    if (inv_id) {
        $.get(url + '/en/container-inventory/get-inventory', {id: inv_id, sh: old_shippers}, function (data) {
            if (data != null)
            {
                var data = $.parseJSON(data);

                //get shippers
                var $select = $('#container-client_shipper');
                var options = $select.data('select2').options.options;
                $select.html('');
                var items = [];
                for (var i = 0; i < data.shippers.length; i++) {
                    items.push({
                        "id": data.shippers[i].id,
                        "text": data.shippers[i].name
                    });
                    $select.append("<option value=\"" + data.shippers[i].id + "\">" + data.shippers[i].name + "</option>");
                }
                options.data = items;
                $select.select2(options);

                //get consignees
                var $select = $('#container-consignee');
                var options = $select.data('select2').options.options;
                $select.html('');
                var items = [];
                for (var i = 0; i < data.consignees.length; i++) {
                    items.push({
                        "id": data.consignees[i].id,
                        "text": data.consignees[i].name
                    });
                    $select.append("<option value=\"" + data.consignees[i].id + "\">" + data.consignees[i].name + "</option>");
                }
                options.data = items;
                $select.select2(options);

                //assign order values
                $select.val(data.order_consignee).trigger('change');
                $("#container-destination_id").val(data.order_destination).trigger('change');
                $("#container-booking_num").val(data.order_booking_number);

            }
        });
    }
}

function check_dashs() {
    var containerNum = $("#container-container_num").val();
    var validate = 'pass';
    if (~containerNum.indexOf("-"))
        validate = 'dash';
    if (validate == 'dash') {
        $('#containerNum_error').html('Container Number shouldn\'t container a dash (-).');
    } else {
        $('#containerNum_error').html('');
    }

//        alert(validated ? "pass" : "fail");
}

function changeCoverSheetAgent() {
    var value = $('#container-booking_provider').val();
    $('#coversheet-agent_forwarder').val(value);
}
function changeCoverSheetAgentContact() {
    var value = $('#container-booking_provider_contact').val();
    $('#coversheet-agent_contact').val(value);
}


    

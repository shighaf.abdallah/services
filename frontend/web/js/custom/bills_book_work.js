function add_bills_book_work(bill_id) {
    $.get(window.url + '/en/accounting/add-to-book-work', {id: bill_id})
            .done(function (data) {
                data = $.parseJSON(data);
                notif({
                    msg: data.msg,
                    type: data.type
                });
            });
}

function remove_bills_book_work(bill_id) {
    $.get(window.url + '/en/accounting/remove-to-book-work', {id: bill_id})
            .done(function (data) {
                data = $.parseJSON(data);
                $('#book_work_bill_'+bill_id).fadeOut(500);
                notif({
                    msg: data.msg,
                    type: data.type
                });
            });
}

$(document).on('click', '.workbook_remove', function (e) {
   e.preventDefault();
   var id = $(this).data('id');
   remove_bills_book_work(id);
});

//$(document).on('click', '#receipt_form_submit', function (e) {
//    e.preventDefault();
//    var validate = receipt_form_validate();
//    if (validate) {
//        $('#receipt_form').submit();
//    }
//});
//
//function receipt_form_validate() {
//    var res = false;
//    var receiptValue = parseFloat($('#userreceipt-value').val());
//    var userBalance = parseFloat($('.user_balance').html());
//    var totalChosenInvoicesValue = 0;
//    $('.invoiceTotal:checkbox:checked').each(function () {
//        res = true;
//        totalChosenInvoicesValue = totalChosenInvoicesValue + parseFloat($('#invoiceTotal_' + $(this).val()).val());
//    });
//    if ((receiptValue + userBalance) < totalChosenInvoicesValue) {
//        res = false;
//        notif({
//            msg: 'Total invoices value is less than value + Customer balance.',
//            type: 'error'
//        });
//    }
//    if (totalChosenInvoicesValue == 0) {
//        notif({
//            msg: "You didn't choose any invoice!",
//            type: 'error'
//        });
//    }
//    return res;
//}
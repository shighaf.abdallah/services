<?php

namespace common\enums;

class Constents extends PhpEnum {

    const full_date_format = 'Y-m-d h:i:s';
    const date_format_view = 'd/m/Y';
    const date_format_view_2 = 'Y-m-d';
    const date_format_view_3 = 'm/d/Y';
    const date_format_view_4 = 'd-M-y';

}

<?php

return [
    'adminEmail' => 'admin@honeybee.app',
     'user.passwordResetTokenExpire' => 3600,
    'title' => 'Services',
];

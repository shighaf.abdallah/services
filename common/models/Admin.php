<?php


namespace common\models;

use Yii;
use common\enums\Constents;
use webvimark\modules\UserManagement\models\ZAdmin;

class Admin extends ZAdmin {

    public function rules() {
        $p = parent::rules();
        $p[] = [['auth_expiry', 'auth_key'], 'safe'];
        return $p;
    }

    // public function getAuthKey()
    // {
    //     if(!$this->auth_expiry || $this->auth_expiry < date(Constents::full_date_format)){
    //         throw new \yii\web\HttpException(504, "Auth Token Expired!", 504);
    //     }
    //     return $this->auth_key;
    // }

    public function getSetAuthKey()
    {
        if(!$this->auth_expiry || $this->auth_expiry < date(Constents::full_date_format)){
            $this->auth_expiry = date(Constents::full_date_format, strtotime(date(Constents::full_date_format) . ' +1 month'));
            $this->auth_key = Yii::$app->security->generateRandomString()
                . Yii::$app->security->generateRandomString()
                . Yii::$app->security->generateRandomString()
                . '_' . time();
            if(!$this->save()){
                stopv($this->errors);
            }
        }
        return $this->auth_key;
    }

    public function getSetNewAuthKey()
    {
        $this->auth_expiry = date(Constents::full_date_format, strtotime(date(Constents::full_date_format) . ' +1 month'));
        $this->auth_key = Yii::$app->security->generateRandomString()
            . Yii::$app->security->generateRandomString()
            . Yii::$app->security->generateRandomString()
            . '_' . time();
        $this->save();
        return $this->auth_key;
    }

    public function getFullName() {
        if ($this->fname) {
            return $this->fname . ' ' . $this->lname;
        } else {
            return $this->username;
        }
    }

}
<?php

namespace common\models;

use Yii;


class Category extends generated\Category {

    const categoryImageDir = 'category';

    /**
     *@var \yii\web\UploadedFile
     */
    public $imageFile;

    public function saveFile()
    {
        $filePath = Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . self::categoryImageDir . DIRECTORY_SEPARATOR;
        $this->image = uniqid().'.'.$this->imageFile->extension;
        $this->image_name = $this->imageFile->name;
        $this->imageFile->saveAs($filePath .  $this->image);
        return true;
    }

    public function getFileUrl()
    {
        return str_replace('/en/', '/', Yii::$app->urlManager->createAbsoluteUrl('public/' .self::categoryImageDir. '/'.$this->image));
    }

    public function getFilePath()
    {
        return Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . self::categoryImageDir . DIRECTORY_SEPARATOR.$this->image;
    }

}

<?php

namespace common\models;

use Yii;


class SubCategory extends generated\SubCategory {

    const categoryImageDir = 'sub_category';

    /**
     *@var \yii\web\UploadedFile
     */
    public $imageFile;


    public function attributeLabels()
    {
        $p =  parent::attributeLabels();
        $p['cat_id'] = 'Category';
        return $p;
    }

    public function saveFile()
    {
        $filePath = Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . self::categoryImageDir . DIRECTORY_SEPARATOR;
        $this->image = uniqid().'.'.$this->imageFile->extension;
        $this->image_name = $this->imageFile->name;
        $this->imageFile->saveAs($filePath .  $this->image);
        return true;
    }

    public function getFileUrl()
    {
        return str_replace('/en/', '/', Yii::$app->urlManager->createAbsoluteUrl('public/' .self::categoryImageDir. '/'.$this->image));
    }

    public function getFilePath()
    {
        return Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . self::categoryImageDir . DIRECTORY_SEPARATOR.$this->image;
    }

}

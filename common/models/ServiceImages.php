<?php


namespace common\models;
use Yii;

class ServiceImages extends \common\models\generated\ServiceImages
{
    const ImageDir = 'service';
    const SecondaryImageDir = 'Secondary';

    /**
     *@var \yii\web\UploadedFile
     */
    public $imageFile;

    public function rules(){
        $p = parent::rules();
        $p[] = [['imageFile'], 'file','maxFiles' => 10];
        return $p;
    }

    public function attributeLabels()
    {
        $p =  parent::attributeLabels();
        $p['imageFile'] = 'Secondary Images';
        return $p;
    }

    public function saveFile($one)
    {
        $filePath = Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . self::ImageDir . DIRECTORY_SEPARATOR. self::SecondaryImageDir . DIRECTORY_SEPARATOR;
        $this->image = uniqid().'.'.$one->extension;
        $one->saveAs($filePath .  $this->image);
        return true;
    }

    public function getFileUrl()
    {
        return str_replace('/en/', '/', Yii::$app->urlManager->createAbsoluteUrl('public/' .self::ImageDir.'/'.self::SecondaryImageDir. '/'.$this->image));
    }

    public function getFilePath()
    {
        return Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . self::ImageDir . DIRECTORY_SEPARATOR . self::SecondaryImageDir . DIRECTORY_SEPARATOR.$this->image;
    }


}
<?php


namespace common\models;

use Yii;
use common\models\ServiceImages;


class Service extends \common\models\generated\Service
{
    const ImageDir = 'service';

    /**
     *@var \yii\web\UploadedFile
     */
    public $imageFile;
    public $cat_id;

    public function rules()
    {
        $r = parent::rules();
        $r[] =[['cat_id','sub_cat_id'], 'safe'];
        $r[] = [['imageFile'], 'file'];
        return $r;
    }

    public function attributeLabels()
    {
        $p =  parent::attributeLabels();
        $p['sub_cat_id'] = 'Sub Category';
        $p['cat_id'] = 'Category';
        $p['imageFile'] = 'Main Image';
        $p['image'] = 'Main Image';
        $p['name_ge'] = 'Name (German)';
        return $p;
    }

    public function saveFile()
    {
        $filePath = Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . self::ImageDir . DIRECTORY_SEPARATOR;
        $this->image = uniqid().'.'.$this->imageFile->extension;
        $this->image_name = $this->imageFile->name;
        $this->imageFile->saveAs($filePath .  $this->image);
        return true;
    }

    public function getFileUrl()
    {
        return str_replace('/en/', '/', Yii::$app->urlManager->createAbsoluteUrl('public/' .self::ImageDir. '/'.$this->image));
    }

    public function getFilePath()
    {
        return Yii::$app->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . self::ImageDir . DIRECTORY_SEPARATOR.$this->image;
    }

    public function saveImages($files)
    {
        $oldImagesIds = ServiceImages::find()
            ->andWhere(['service_id' => $this->id])
            ->select(['name'])
            ->column();
        foreach ($files as $one) {
            if (!in_array($one->name, $oldImagesIds)) {
                $secondaryImage = new ServiceImages();
                $secondaryImage->saveFile($one);
                $secondaryImage->service_id = $this->id;
                $secondaryImage->name = $one->name;
                if (!$secondaryImage->save()) {
                    stopv($secondaryImage->errors);
                }
            }
        }
    }

    public function saveDetails($desc)
    {
        $oldDescriptionIds = ServiceDetails::find()->andWhere(['service_id' => $this->id])->select('id')->column();
        $foundOld=[];
        if(is_array($desc)){
            foreach($desc as $x => $value){
                $descriptionModel = new ServiceDetails();
                $descriptionModel->service_id = $this->id;
                if(in_array($x, $oldDescriptionIds)){
                    $foundOld[] = $x;
                    $descriptionModel = ServiceDetails::findOne($x);
                }
                $descriptionModel->title_en = $value['title_en'];
                $descriptionModel->title_ge = $value['title_ge'];
                $descriptionModel->description_en = $value['description_en'];
                $descriptionModel->description_ge = $value['description_ge'];
                $descriptionModel->save();
            }
        }
        if(is_array($oldDescriptionIds)){
            foreach ($oldDescriptionIds as $one) {
                if(!in_array($one, $foundOld)){
                    $descriptionModel = ServiceDetails::findOne($one);
                    $descriptionModel->delete();
                }
            }
        }

        return true;
    }

    /**
     * Gets query for [[ServiceImages]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServiceImages()
    {
        return $this->hasMany(ServiceImages::className(), ['service_id' => 'id']);
    }
}
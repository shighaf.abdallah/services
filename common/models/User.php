<?php


namespace common\models;

use webvimark\modules\UserManagement\models\ZUser;
use Yii;
use common\enums\Constents;
use webvimark\modules\UserManagement\models\ZAdmin;

class User extends ZUser {

    public function rules() {
        $p = parent::rules();
        $p[] = [['due_date_days', 'due_date_before_or_after', 'due_date_date_type'], 'safe'];
        return $p;
    }

    public function getFullName() {
        if ($this->fname) {
//            return $this->fname . ' ' . $this->lname;
            return $this->username;
        } else {
            return $this->username;
        }
    }

}
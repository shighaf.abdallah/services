<?php

namespace common\models\generated;

use Yii;

/**
 * This is the model class for table "sub_category".
 *
 * @property int $id
 * @property int $cat_id
 * @property string $name_en
 * @property string $name_ge
 * @property string|null $description_en
 * @property string|null $description_ge
 * @property string|null $image
 * @property string|null $image_name
 * @property string $created_at
 * @property int $status
 *
 * @property Category $cat
 */
class SubCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sub_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cat_id', 'name_en', 'name_ge', 'created_at', 'status'], 'required'],
            [['cat_id', 'status'], 'integer'],
            [['description_en', 'description_ge'], 'string'],
            [['created_at'], 'safe'],
            [['name_en', 'name_ge'], 'string', 'max' => 20],
            [['image', 'image_name'], 'string', 'max' => 255],
            [['cat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['cat_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cat_id' => 'Cat ID',
            'name_en' => 'Name En',
            'name_ge' => 'Name Ge',
            'description_en' => 'Description En',
            'description_ge' => 'Description Ge',
            'image' => 'Image',
            'image_name' => 'Image Name',
            'created_at' => 'Created At',
            'status' => 'Status',
        ];
    }

    /**
     * Gets query for [[Cat]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCat()
    {
        return $this->hasOne(Category::className(), ['id' => 'cat_id']);
    }
}

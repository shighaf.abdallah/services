<?php

namespace common\models\generated;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $name_en
 * @property string $name_ge
 * @property string|null $description_en
 * @property string|null $description_ge
 * @property string|null $image
 * @property string|null $image_name
 * @property string $created_at
 * @property int $status
 *
 * @property Service[] $services
 * @property SubCategory[] $subCategories
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_en', 'name_ge', 'created_at', 'status'], 'required'],
            [['description_en', 'description_ge'], 'string'],
            [['created_at'], 'safe'],
            [['status'], 'integer'],
            [['name_en', 'name_ge'], 'string', 'max' => 20],
            [['image', 'image_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_en' => 'Name En',
            'name_ge' => 'Name Ge',
            'description_en' => 'Description En',
            'description_ge' => 'Description Ge',
            'image' => 'Image',
            'image_name' => 'Image Name',
            'created_at' => 'Created At',
            'status' => 'Status',
        ];
    }

    /**
     * Gets query for [[Services]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServices()
    {
        return $this->hasMany(Service::className(), ['cat_id' => 'id']);
    }

    /**
     * Gets query for [[SubCategories]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubCategories()
    {
        return $this->hasMany(SubCategory::className(), ['cat_id' => 'id']);
    }
}

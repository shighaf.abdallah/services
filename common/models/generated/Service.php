<?php

namespace common\models\generated;

use Yii;

/**
 * This is the model class for table "service".
 *
 * @property int $id
 * @property string $name_en
 * @property string $name_ge
 * @property int $sub_cat_id
 * @property string|null $description_en
 * @property string|null $description_ge
 * @property string|null $image
 * @property string|null $image_name
 * @property string $created_at
 * @property int $status
 *
 * @property ServiceDetails[] $serviceDetails
 * @property ServiceImages[] $serviceImages
 * @property SubCategory $subCat
 */
class Service extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name_en', 'name_ge', 'sub_cat_id', 'created_at', 'status'], 'required'],
            [['sub_cat_id', 'status'], 'integer'],
            [['description_en', 'description_ge'], 'string'],
            [['created_at'], 'safe'],
            [['name_en', 'name_ge'], 'string', 'max' => 20],
            [['image'], 'string', 'max' => 250],
            [['image_name'], 'string', 'max' => 255],
            [['sub_cat_id'], 'exist', 'skipOnError' => true, 'targetClass' => SubCategory::className(), 'targetAttribute' => ['sub_cat_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_en' => 'Name En',
            'name_ge' => 'Name Ge',
            'sub_cat_id' => 'Sub Cat ID',
            'description_en' => 'Description En',
            'description_ge' => 'Description Ge',
            'image' => 'Image',
            'image_name' => 'Image Name',
            'created_at' => 'Created At',
            'status' => 'Status',
        ];
    }

    /**
     * Gets query for [[ServiceDetails]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServiceDetails()
    {
        return $this->hasMany(ServiceDetails::className(), ['service_id' => 'id']);
    }



    /**
     * Gets query for [[SubCat]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubCat()
    {
        return $this->hasOne(SubCategory::className(), ['id' => 'sub_cat_id']);
    }
}

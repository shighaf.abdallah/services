<?php

namespace common\models\generated;

use Yii;

/**
 * This is the model class for table "service_images".
 *
 * @property int $id
 * @property int $service_id
 * @property string $name
 * @property string $image
 * @property string|null $unique_id
 *
 * @property Service $service
 */
class ServiceImages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_id', 'name', 'image'], 'required'],
            [['service_id'], 'integer'],
            [['name', 'image', 'unique_id'], 'string', 'max' => 255],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Service::className(), 'targetAttribute' => ['service_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'service_id' => 'Service ID',
            'name' => 'Name',
            'image' => 'Image',
            'unique_id' => 'Unique ID',
        ];
    }

    /**
     * Gets query for [[Service]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }
}

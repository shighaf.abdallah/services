<?php

namespace common\models\generated;

use Yii;

/**
 * This is the model class for table "service_details".
 *
 * @property int $id
 * @property int $service_id
 * @property string $title_en
 * @property string $title_ge
 * @property string|null $description_en
 * @property string|null $description_ge
 *
 * @property Service $service
 */
class ServiceDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['service_id', 'title_en', 'title_ge'], 'required'],
            [['service_id'], 'integer'],
            [['description_en', 'description_ge'], 'string'],
            [['title_en', 'title_ge'], 'string', 'max' => 20],
            [['service_id'], 'exist', 'skipOnError' => true, 'targetClass' => Service::className(), 'targetAttribute' => ['service_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'service_id' => 'Service ID',
            'title_en' => 'Title En',
            'title_ge' => 'Title Ge',
            'description_en' => 'Description En',
            'description_ge' => 'Description Ge',
        ];
    }

    /**
     * Gets query for [[Service]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Service::className(), ['id' => 'service_id']);
    }
}

<?php

namespace common\services;

use api\models\auth\LoginResult;
use api\models\auth\LoginResultApi;
use api\models\other\ApiMessage;
use backend\controllers\InvoiceController;
use common\enums\ContainerInventoryStatus;
use common\enums\EmailRecordStatus;
use common\enums\EmailRecordType;
use common\enums\ErrorCode;
use common\enums\LoginApiEnum;
use common\models\Admin;
use common\models\BillOfLoading;
use common\models\ContainerInventory;
use common\models\CoverSheet;
use common\models\InventoryLog;
use common\models\Invoice;
use common\models\OrderInventory;
use kartik\mpdf\Pdf;
use webvimark\modules\UserManagement\libs\LibUser;
use webvimark\modules\UserManagement\models\User;
use webvimark\modules\UserManagement\models\ZAdmin;
use webvimark\modules\UserManagement\models\ZUser;
use Yii;
use common\models\Inventory;
use common\enums\InventoryStatus;
use common\models\InlandFeesChack;
use common\enums\CheckStatus;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\web\UploadedFile;
use common\models\InventoryImage;
use common\models\InventoryWeight;
use common\enums\InventoryTitle;
use common\models\search\InventorySearch;
use common\models\InventoryVideo;

class UserService extends LibUser {

    public static function loginPhoneEmailValidation($phone_email, $password) {
        $is_ok = true;
        if(!$phone_email || !$password){
            throw new \yii\web\HttpException(200, t('Missing data!'), 200);
        }

        if (!filter_var($phone_email, FILTER_VALIDATE_EMAIL)) {
            throw new \yii\web\HttpException(200, t('E-mail is not a valid email address.'), 200);
        }

        $login_model = new LoginResult();
        $login_model->token = '';

        $user = Admin::find()
            ->where(['email' => $phone_email])
//            ->orWhere(['email' => $phone_email])
            ->one();
        if (!($user)) {
            $login_model->resultCode = LoginApiEnum::not_found;
            $login_model->resultText = LoginApiEnum::LabelOf($login_model->resultCode);
        } elseif (!Yii::$app->getSecurity()->validatePassword($password, $user->password_hash)) {
            $login_model->resultCode = LoginApiEnum::invalid_password;
            $login_model->resultText = LoginApiEnum::LabelOf($login_model->resultCode, false, api_lang());
        } elseif ($user->status == User::STATUS_BANNED) {
            $login_model->resultCode = LoginApiEnum::banned;
            $login_model->resultText = LoginApiEnum::LabelOf($login_model->resultCode);
        }
        elseif (!$user->email_confirmed) {
            $login_model->resultCode = LoginApiEnum::not_confirmed;
            $login_model->resultText = LoginApiEnum::LabelOf($login_model->resultCode);
        } elseif ($user->status == User::STATUS_INACTIVE) {
            $login_model->resultCode = LoginApiEnum::not_active;
            $login_model->resultText = LoginApiEnum::LabelOf($login_model->resultCode);
        } else {
            $user->setAuthKey;
            $login_model->resultCode = LoginApiEnum::accepted;
            $login_model->resultText = LoginApiEnum::LabelOf($login_model->resultCode);
            $login_model->token = UserService::profile($user->id)->auth_key;
            $login_model->profile = FillApiModelService::FillProfileResultModel($user);
        }

        $res = new LoginResultApi([
            'result' => $login_model,
            'isOk' => $is_ok,
            'message' => new ApiMessage([
                'type' => 'Success',
                'code' => ErrorCode::success,
                'content' => '',
            ])
        ]);
        return $res;
    }

    public static function apiLogOut($user) {
        //change auth token
        $user->setNewAuthKey;
        Yii::$app->user->logout();
        $data = api_success_msg('Logout success');
        return $data;
    }
}
<?php
/**
 * This model allows for storing of javascript logging entries linked to a specific audit entry
 */

namespace bedezign\yii2\audit\models;

use bedezign\yii2\audit\components\db\ActiveRecord;
use Ramsey\Uuid\Uuid;
use Yii;

/**
 * AuditJavascript
 *
 * @package bedezign\yii2\audit\models
 * @property int    $guid
 * @property int    $entry_id
 * @property string $created
 * @property string $type
 * @property string $message
 * @property string $origin
 * @property string $data
 *
 * @property AuditEntry    $entry
 */
class AuditJavascript extends ActiveRecord
{
    public static function getDb() {
        return Yii::$app->db_audit;
    }

    public function beforeSave($insert)
    {
        if(!$this->id || $this->id == 'temp'){
            $this->id = Uuid::uuid4()->toString();
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%audit_javascript}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntry()
    {
        return $this->hasOne(AuditEntry::className(), ['id' => 'entry_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => Yii::t('audit', 'ID'),
            'entry_id'  => Yii::t('audit', 'Entry ID'),
            'created'   => Yii::t('audit', 'Created'),
            'type'      => Yii::t('audit', 'Type'),
            'message'   => Yii::t('audit', 'Message'),
            'origin'    => Yii::t('audit', 'Origin'),
            'data'      => Yii::t('audit', 'Data'),
        ];
    }
}
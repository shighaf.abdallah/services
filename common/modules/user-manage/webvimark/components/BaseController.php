<?php

namespace webvimark\components;
use webvimark\modules\UserManagement\components\GhostAccessControl;
use webvimark\modules\UserManagement\models\ZAdmin;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

class BaseController extends \common\utils\Controller
{
	/**
	 * @return array
	 */
	public function behaviors()
	{
		return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'toggle-attribute', 'add-attachment',
                            'bulk-activate', 'bulk-deactivate', 'bulk-delete', 'grid-sort', 'grid-page-size',
                            'set-child-permissions', 'set', 'set-roles', 'change-password', 'get-customers'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function($rule, $action) {
                            $permissionName = Yii::$app->controller->id . '_' . Yii::$app->controller->action->id;
                            return ZAdmin::hasPermission($permissionName);
                            //return \Yii::$app->user->can(Yii::$app->controller->id.'-'.$action->id);
//                    return \webvimark\modules\UserManagement\models\ZUser::hasRole([Yii::$app->controller->id . '-' . $action->id]);
                        }
                    ],
                    [
                        'actions' => ['registration', 'login', 'logout', 'confirm-registration-email', 'password-recovery', 'password-recovery-receive'],
                        'allow' => true,
                    ],
                ],
            ],
//			'ghost-access'=> [
//				'class' => GhostAccessControl::className(),
//			],
		];
	}

	/**
	 * Render ajax or usual depends on request
	 *
	 * @param string $view
	 * @param array $params
	 *
	 * @return string|\yii\web\Response
	 */
	protected function renderIsAjax($view, $params = [])
	{
		if ( Yii::$app->request->isAjax )
		{
			return $this->renderAjax($view, $params);
		}
		else
		{
			return $this->render($view, $params);
		}
	}
}